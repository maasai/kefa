<?php
namespace App\Models;

class Invoice extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'invoices';

    /**
     * Main table primary key
     * @var string
     */
    protected $primaryKey = 'uuid';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['client_id','order_id', 'currency', 'invoice_number', 'made_date', 'due_date', 'status', 'notes', 'terms'];

}

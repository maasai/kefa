<?php
namespace App\Models;

class InvoiceSetting extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'invoice_settings';

    /**
     * Main table primary key
     * @var string
     */
    protected $primaryKey = 'uuid';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['starting_number', 'terms', 'logo'];

}

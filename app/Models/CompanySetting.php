<?php

namespace App\Models;

class CompanySetting extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'company_settings';

    /**
     * Main table primary key
     * @var string
     */
    protected $primaryKey = 'uuid';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['company_name', 'contact_person', 'email', 'phone', 'address', 'city', 'state',
        'postal_code', 'country', 'website', 'vat_number', 'logo', 'paypal_email'];

}

<?php

namespace App\Models;

class Level extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'academic_levels';

    /**
     * Main table primary key
     * @var string
     */
    protected $primaryKey = 'uuid';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['academic_level', 'level_value'];


    public function urgencies()
    {
        return $this->belongsToMany('App\Models\Urgency', 'prices')->withPivot('factor');
    }

}

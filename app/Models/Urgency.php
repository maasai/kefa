<?php

namespace App\Models;

class Urgency extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'paper_urgencies';

    /**
     * Main table primary key
     * @var string
     */
    protected $primaryKey = 'uuid';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['urgency', 'duration'];


    public function levels()
    {
        return $this->belongsToMany('App\Models\Level', 'prices')->withPivot('factor');
    }

}

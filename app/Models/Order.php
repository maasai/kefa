<?php
namespace App\Models;

class Order extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * Main table primary key
     * @var string
     */
    protected $primaryKey = 'uuid';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['order_number','client_id','title', 'subject_area_id','academic_level_id', 'paid_date',
        'pages','total_cost','amount_paid','paper_type_id','deadline_date','sources', 'writing_style_id', 'paper_details', 'client_note',
        'status_id', 'code', 'spacing_id', 'urgency_id'
    ];


    //Relationships
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client(){
        return $this->belongsTo('App\Models\Client');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status(){
        return $this->belongsTo('App\Models\Status');
    }


    public function subject(){
        return $this->belongsTo('App\Models\Subject');
    }

    public function level(){
        return $this->belongsTo('App\Models\Level');
    }




}

<?php  namespace App\Providers;


use Illuminate\Support\ServiceProvider;

class WriterServiceProvider extends ServiceProvider {

    public function boot()
    {
        $this->composers();
    }

    protected $repositories = [
        'Assignment', 'CompanySetting', 'Client', 'ClientProfileSetting', 'Currency', 'Invoice', 'InvoiceSetting', 'Level', 'Order', 'OrderSetting', 'Payment', 'PaymentMethod', 'Price', 'ProfileSetting', 'Spacing', 'Style', 'Status', 'Subject', 'Urgency', 'User'
    ];

    public function register()
    {
        //Loops through all repositories and binds them with their Eloquent implementation
        array_walk($this->repositories, function($repository) {
            $this->app->bind(
                'App\Writer\Repositories\Contracts\\'. $repository . 'Interface',
                'App\Writer\Repositories\Eloquent\\' . $repository . 'Repository'
            );
        });

    }

    public function composers()
    {
        $this->app->make('view')->composer('admin.common.navigation', 'App\Http\Composers\NavigationComposer');
        $this->app->make('view')->composer('frontend.form.order', 'App\Http\Composers\OrderFormComposer');

    }



} 
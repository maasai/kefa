<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(array('middleware'=>'web'),function() {

Route::get('/', function () {
    return view('frontend.home');
});

Route::get('page', function () {
    return view('frontend.page');
});
Route::get('about', function () {
    return view('frontend.about');
});
Route::get('contact', function () {
    return view('frontend.contact');
});
Route::get('pricing', function () {
    return view('frontend.pricing');
});

Route::get('portfolio', function () {
    return view('frontend.portfolio');
});
});
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
/**
 * Registration and login Routes. Both client and admin.
 */
Route::group(['middleware' => ['web']], function () {

    Route::post('/form', ['uses' => 'FrontEnd\OrderFormController@store', 'as' => 'form']);

    //Prices
    Route::post('/amount', ['uses' => 'FrontEnd\PricesController@store', 'as' => 'amount']);


    //Login Routes...Admin
    Route::get('/admin/login','Admin\Auth\AuthController@showLoginForm');
    Route::post('/admin/login','Admin\Auth\AuthController@postLogin');
    Route::get('/admin/logout','Admin\Auth\AuthController@getLogout');

    //Login Routes...Client
    Route::get('/client/login','Client\Auth\AuthController@showLoginForm');
    Route::post('/client/login','Client\Auth\AuthController@postLogin');
    Route::get('/client/logout','Client\Auth\AuthController@getLogout');

    // Registration Routes...Admin
    Route::get('admin/register', 'Admin\Auth\AuthController@showRegistrationForm');
    Route::post('admin/register', 'Admin\Auth\AuthController@register');

    // Registration Routes...Client
    Route::get('client/register', 'Client\Auth\AuthController@getRegister');
    Route::post('client/register', 'Client\Auth\AuthController@postRegister');

    Route::get('/admin', 'Admin\DashboardController@index');
    Route::get('/client', 'Client\DashboardController@index');



});

/**
 * Admin Routes
 */
Route::group(array('prefix'=>'admin', 'middleware'=>'web'),function() {

    Route::get('dashboard', 'Admin\DashboardController@index');
    Route::resource('users', 'Admin\UsersController');
    Route::resource('clients', 'Admin\ClientsController');
    Route::resource('payments', 'Admin\PaymentsController');
    Route::resource('orders', 'Admin\OrdersController');
    Route::resource('invoices', 'Admin\InvoicesController');
    /**
     * Settings Routes
     */
    Route::group(array('prefix'=>'settings'),function(){
        Route::resource('company', 'Admin\CompanySettingsController');
        Route::resource('invoice', 'Admin\InvoiceSettingsController');
        Route::resource('profile', 'Admin\ProfileSettingsController');
        Route::resource('order', 'Admin\OrderSettingsController');

    });
    /**
     * System Routes
     */
    Route::group(array('prefix'=>'system'),function(){
        Route::resource('payment_methods', 'Admin\PaymentMethodsController');
        Route::resource('currencies', 'Admin\CurrenciesController');
        Route::resource('assignments', 'Admin\AssignmentsController');
        Route::resource('levels', 'Admin\LevelsController');
        Route::resource('spacings', 'Admin\SpacingsController');
        Route::resource('styles', 'Admin\StylesController');
        Route::resource('subjects', 'Admin\SubjectsController');
        Route::resource('urgencies', 'Admin\UrgenciesController');
        Route::resource('prices', 'Admin\PricesController',
            ['only' => ['index', 'store']]);

        Route::resource('statuses', 'Admin\StatusController');
    });
});

/**
 * Client Routes
 */
Route::group(array('prefix'=>'client', 'middleware'=>'web'),function() {

    Route::get('dashboard', 'Client\DashboardController@index');
    Route::resource('payments', 'Client\PaymentsController');
    Route::resource('orders', 'Client\OrdersController');
    Route::resource('invoices', 'Client\InvoicesController');
    /**
     * Settings Routes
     */
    Route::group(array('prefix'=>'settings'),function(){
        Route::resource('profile', 'Client\ProfileSettingsController');
    });

});
<?php

namespace App\Http\Requests;
use Illuminate\Http\JsonResponse;

class AssignmentRequest extends Request
{
    /**
     * Overrides response from the FormRequest
     * to not redirect for our API development
     * @param array $errors
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        $message = array(
            'message' => "There were validation errors",
            'errors' => $errors
        );

        return new JsonResponse($message, 400);
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =
            [
                'assignment_type'   => 'required|unique:assignment_types,assignment_type',
            ];

        if($id = $this->assignments)
        {
            $rules['assignment_type'] .= ','.$id.',uuid';
        }
        return $rules;
    }
}

<?php

namespace App\Http\Requests;
use Illuminate\Http\JsonResponse;

class SubjectRequest extends Request
{
    /**
     * Overrides response from the FormRequest
     * to not redirect for our API development
     * @param array $errors
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        $message = array(
            'message' => "There were validation errors",
            'errors' => $errors
        );

        return new JsonResponse($message, 400);
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =
            [
                'subject_area'  => 'required|unique:subject_areas,subject_area',
            ];

        if($id = $this->subjects)
        {
            $rules['subject_area'] .= ','.$id.',uuid';
        }
        return $rules;
    }
}

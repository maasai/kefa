<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Http\JsonResponse;

class ClientRequest extends Request
{

    /**
     * Overrides response from the FormRequest
     * to not redirect for our API development
     * @param array $errors
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        $message = array(
            'message' => "There were validation errors",
            'errors' => $errors
        );

        return new JsonResponse($message, 400);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =
            [
                'client_name'           => 'required',
                'email'                 => 'required|email|unique:clients,email',
                'password'              => 'confirmed|min:5',
            ];

        if($id = $this->clients)
        {
            $rules['client_name'] .= ','.$id.',uuid';
            $rules['email'] .= ','.$id.',uuid';
        }else{
            $rules['password'] .= '|required';
        }
        return $rules;
    }
}

<?php

namespace App\Http\Requests;
use Illuminate\Http\JsonResponse;

class OrderRequest extends Request
{
    /**
     * Overrides response from the FormRequest
     * to not redirect for our API development
     * @param array $errors
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        $message = array(
            'message' => "There were validation errors",
            'errors' => $errors
        );

        return new JsonResponse($message, 400);
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =
            [
                'client_id'         => 'required',
                'order_number'      => 'unique:orders,order_number',
                'title'             => 'required|min:5',
                'subject_area_id'   => 'required',
                'academic_level_id' => 'required',
                'paper_type_id'     => 'required',
                'writing_style_id'  => 'required',
                'pages'             => 'numeric|min:1|max:12',
                'sources'           => 'numeric|min:1|max:12'
            ];

        if($id = $this->orders)
        {
            $rules['order_number'] .= ','.$id.',uuid';
        }
        return $rules;
    }
}

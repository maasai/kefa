<?php
namespace App\Http\Requests;

use Illuminate\Http\JsonResponse;

class CompanySettingRequest extends Request
{
    /**
     * Overrides response from the FormRequest
     * to not redirect for our API development
     * @param array $errors
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        $message = array(
            'message' => "There were validation errors",
            'errors' => $errors
        );

        return new JsonResponse($message, 400);
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =
            [
                'company_name'   => 'required|unique:company_settings,company_name',
                'contact_person'   => 'required',
                'email'   => 'required|unique:company_settings,email',
                'paypal_email'   => 'email',
            ];

        if($id = $this->company_settings)
        {
            $rules['company_name'] .= ','.$id.',uuid';
        }
        return $rules;
    }
}

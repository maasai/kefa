<?php
namespace App\Http\Composers;

use App\Writer\Repositories\Contracts\ClientInterface;
use App\Writer\Repositories\Contracts\OrderInterface;
use App\Writer\Repositories\Contracts\StatusInterface;
use App\Writer\Repositories\Contracts\UserInterface;

class NavigationComposer
{
    protected $orders, $clients, $users, $statuses;

    public function __construct(OrderInterface $orders, ClientInterface $clients, UserInterface $users, StatusInterface $statuses)
    {
        $this->orders = $orders;
        $this->clients = $clients;
        $this->users = $users;
        $this->statuses = $statuses;
    }
    public function compose($view)
    {
        $view->with('totalClients', $this->clients->count())
            ->with('totalUsers', $this->users->count())
            ->with('orderStatuses', $this->statuses->getAll('orders'))
            ->with('orders', $this->orders->getAll());
    }

}
<?php
namespace App\Http\Composers;

use App\Writer\Repositories\Contracts\OrderInterface;


class OrderFormComposer
{
    protected $orderRepository;

    public function __construct(OrderInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;

    }
    public function compose($view)
    {
        $view->with('types', $this->orderRepository->types_select())
            ->with('levels', $this->orderRepository->levels_select())
            ->with('styles', $this->orderRepository->styles_select())
            ->with('urgencies', $this->orderRepository->urgencies_select())
            ->with('spacings', $this->orderRepository->spacings_select())
            ->with('currencies', $this->orderRepository->currencies_select())
            ->with('subjects', $this->orderRepository->subjects_select());
    }

}
<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\WriterController;
use App\Http\Requests\UserRequest;
use App\Writer\Repositories\Contracts\UserInterface;

class UsersController extends WriterController
{
    protected $userRepository;

    public function __construct(UserInterface $userRepository)
    {
        $this->middleware('admin');
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *     * @return Response
     */
    public function index()
    {
        $users = $this->userRepository->getAll();
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store new data
     * @param UserRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(UserRequest $request)
    {
        $data = array(
            'first_name'    => $request->first_name,
            'last_name'     => $request->last_name,
            'email'         => $request->email,
            'phone_1'       => $request->phone_1,
            'phone_2'       => $request->phone_2,
            'country'       => $request->country,
            'profile_pic'   => $request->profile_pic,
            'password'      => bcrypt($request->password)
        );

       if($this->userRepository->create($data)){
            session()->flash('success', 'Success !! User has been added.');
            return response()->json(array('success' => true, 'msg' => 'User created'), 201);
        }
        return redirect('admin/users');
    }

    /**
     * Show Edit Form
     * @param $uuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($uuid)
    {
        $user = $this->userRepository->getById($uuid);
        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update data
     * @param UserRequest $request
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(UserRequest $request, $uuid)
    {
        $data = array(
            'first_name'    => $request->first_name,
            'last_name'     => $request->last_name,
            'email'         => $request->email,
            'phone_1'       => $request->phone_1,
            'phone_2'       => $request->phone_2,
            'country'       => $request->country,
            'profile_pic'   => $request->profile_pic,
            'status'       => $request->status,
            'password'      => bcrypt($request->password)
        );
        if($this->userRepository->update($data, $uuid) ){
            session()->flash('success', 'Success !! User has been Updated.');
            return response()->json(array('success' => true, 'msg' => 'User Updated'), 201);
        }
        return redirect('admin/users');

    }

    /**
     * Delete
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($uuid)
    {
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $fuel){
                $this->userRepository->delete($fuel);
            }
            session()->flash('success', 'Success !! Users have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Users deleted successfully'], 200);
        }
        else{
            if($this->userRepository->delete($uuid)){
                session()->flash('success', 'Success !! User has been deleted.');
            }else{
                session()->flash('error', 'Error !! Error deleting User. Try again.');
            }
            return redirect('admin/users');
        }
    }

}


<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\WriterController;
use App\Http\Requests\CurrencyRequest;
use App\Writer\Repositories\Contracts\CurrencyInterface;

class CurrenciesController extends WriterController
{
    protected $currencyRepository;

    public function __construct(CurrencyInterface $currencyRepository)
    {
        $this->middleware('admin');
        $this->currencyRepository = $currencyRepository;
    }

    /**
     * Display a listing of the resource.
     *     * @return Response
     */
    public function index()
    {
        $currencies = $this->currencyRepository->getAll();
        return view('admin.currencies.index', compact('currencies'));
    }

    /**
     * Show form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.currencies.create');
    }

    /**
     * Store new data
     * @param CurrencyRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CurrencyRequest $request)
    {
        if($this->currencyRepository->create($request->all())){
            session()->flash('success', 'Success !! Currency has been added.');
            return response()->json(array('success' => true, 'msg' => 'Currency created'), 201);
        }
        return redirect('admin/system/currencies');
    }

    /**
     * Show Edit Form
     * @param $uuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($uuid)
    {
        $currency = $this->currencyRepository->getById($uuid);
        return view('admin.currencies.edit', compact('currency'));
    }

    /**
     * Update data
     * @param CurrencyRequest $request
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(CurrencyRequest $request, $uuid)
    {
        if($this->currencyRepository->update($request->all(), $uuid) ){
            session()->flash('success', 'Success !! Currency has been Updated.');
            return response()->json(array('success' => true, 'msg' => 'Currency Updated'), 201);
        }
        return redirect('admin/system/currencies');

    }

    /**
     * Delete
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($uuid)
    {
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $fuel){
                $this->currencyRepository->delete($fuel);
            }
            session()->flash('success', 'Success !! Currencies have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Currencies deleted successfully'], 200);
        }
        else{
            if($this->currencyRepository->delete($uuid)){
                session()->flash('success', 'Success !! Currency has been deleted.');
            }else{
                session()->flash('error', 'Error !! Error deleting Currency. Try again.');
            }
            return redirect('admin/system/currencies');
        }
    }

}


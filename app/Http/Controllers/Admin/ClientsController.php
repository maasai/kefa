<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\WriterController;
use App\Http\Requests\ClientRequest;
use App\Writer\Repositories\Contracts\ClientInterface;

class ClientsController extends WriterController
{
    protected $clientRepository;

    public function __construct(ClientInterface $clientRepository)
    {
        $this->middleware('admin');
        $this->clientRepository = $clientRepository;
    }

    /**
     * Display a listing of the resource.
     *     * @return Response
     */
    public function index()
    {
        $clients = $this->clientRepository->getAll();
        return view('admin.clients.index', compact('clients'));
    }

    /**
     * Show form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.clients.create');
    }

    /**
     * Store new data
     * @param ClientRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(ClientRequest $request)
    {
        $data = array(
            'client_name'   => $request->client_name,
            'country'       => $request->country,
            'phone'         => $request->phone,
            'email'         => $request->email,
            'address'       => $request->address,
            'city'          => $request->city,
            'state'         => $request->state,
            'postal_code'   => $request->postal_code,
            'website'       => $request->website,
            'notes'         => $request->notes,
            'password'      => bcrypt($request->password)
        );

        if($this->clientRepository->create($data)){
            session()->flash('success', 'Success !! Client has been added.');
            return response()->json(array('success' => true, 'msg' => 'Client created'), 201);
        }
        return redirect('admin/clients');
    }

    /**
     * Show Edit Form
     * @param $uuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($uuid)
    {
        $client = $this->clientRepository->getById($uuid);
        return view('admin.clients.edit', compact('client'));
    }

    /**
     * Update data
     * @param ClientRequest $request
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(ClientRequest $request, $uuid)
    {
        $data = array(
            'client_name'   => $request->client_name,
            'country'       => $request->country,
            'phone'         => $request->phone,
            'email'         => $request->email,
            'address'       => $request->address,
            'city'          => $request->city,
            'state'         => $request->state,
            'postal_code'   => $request->postal_code,
            'website'       => $request->website,
            'notes'         => $request->notes,
            'status'        => $request->status,
            'password'      => bcrypt($request->password)
        );

        if($this->clientRepository->update($data, $uuid) ){
            session()->flash('success', 'Success !! Client has been Updated.');
            return response()->json(array('success' => true, 'msg' => 'Client Updated'), 201);
        }
        return redirect('admin/clients');

    }

    /**
     * Delete
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($uuid)
    {
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $fuel){
                $this->clientRepository->delete($fuel);
            }
            session()->flash('success', 'Success !! Clients have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Clients deleted successfully'], 200);
        }
        else{
            if($this->clientRepository->delete($uuid)){
                session()->flash('success', 'Success !! Client has been deleted.');
            }else{
                session()->flash('error', 'Error !! Error deleting Client. Try again.');
            }
            return redirect('admin/clients');
        }
    }

}


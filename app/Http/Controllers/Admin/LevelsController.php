<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\WriterController;
use App\Http\Requests\LevelRequest;
use App\Writer\Repositories\Contracts\LevelInterface;

class LevelsController extends WriterController
{
    protected $levelRepository;

    public function __construct(LevelInterface $levelRepository)
    {
        $this->middleware('admin');
        $this->levelRepository = $levelRepository;
    }

    /**
     * Display a listing of the resource.
     *     * @return Response
     */
    public function index()
    {
        $levels = $this->levelRepository->getAll();
        return view('admin.levels.index', compact('levels'));
    }

    /**
     * Show form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.levels.create');
    }

    /**
     * Store new data
     * @param LevelRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(LevelRequest $request)
    {
        if($this->levelRepository->create($request->all())){
            session()->flash('success', 'Success !! Level has been added.');
            return response()->json(array('success' => true, 'msg' => 'Level created'), 201);
        }
        return redirect('admin/system/levels');
    }

    /**
     * Show Edit Form
     * @param $uuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($uuid)
    {
        $level = $this->levelRepository->getById($uuid);
        return view('admin.levels.edit', compact('level'));
    }

    /**
     * Update data
     * @param LevelRequest $request
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(LevelRequest $request, $uuid)
    {
        if($this->levelRepository->update($request->all(), $uuid) ){
            session()->flash('success', 'Success !! Level has been Updated.');
            return response()->json(array('success' => true, 'msg' => 'Level Updated'), 201);
        }
        return redirect('admin/system/levels');

    }

    /**
     * Delete
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($uuid)
    {
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $fuel){
                $this->levelRepository->delete($fuel);
            }
            session()->flash('success', 'Success !! Levels have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Levels deleted successfully'], 200);
        }
        else{
            if($this->levelRepository->delete($uuid)){
                session()->flash('success', 'Success !! Level has been deleted.');
            }else{
                session()->flash('error', 'Error !! Error deleting Level. Try again.');
            }
            return redirect('admin/system/levels');
        }
    }

}


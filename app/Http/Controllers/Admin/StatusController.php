<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\WriterController;
use App\Http\Requests\StatusRequest;
use App\Writer\Repositories\Contracts\StatusInterface;

class StatusController extends WriterController
{
    protected $statusRepository;

    public function __construct(StatusInterface $statusRepository)
    {
        $this->middleware('admin');
        $this->statusRepository = $statusRepository;
    }

    /**
     * Display a listing of the resource.
     *     * @return Response
     */
    public function index()
    {
        $statuses = $this->statusRepository->getAll();
        return view('admin.statuses.index', compact('statuses'));
    }

    /**
     * Show form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.statuses.create');
    }

    /**
     * Store new data
     * @param StatusRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StatusRequest $request)
    {
        if($this->statusRepository->create($request->all())){
            session()->flash('success', 'Success !! Status has been added.');
            return response()->json(array('success' => true, 'msg' => 'Status created'), 201);
        }
        return redirect('admin/system/statuses');
    }

    /**
     * Show Edit Form
     * @param $uuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($uuid)
    {
        $status = $this->statusRepository->getById($uuid);
        return view('admin.statuses.edit', compact('status'));
    }

    /**
     * Update data
     * @param StatusRequest $request
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(StatusRequest $request, $uuid)
    {
        if($this->statusRepository->update($request->all(), $uuid) ){
            session()->flash('success', 'Success !! Status has been Updated.');
            return response()->json(array('success' => true, 'msg' => 'Status Updated'), 201);
        }
        return redirect('admin/system/statuses');

    }

    /**
     * Delete
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($uuid)
    {
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $fuel){
                $this->statusRepository->delete($fuel);
            }
            session()->flash('success', 'Success !! Statuses have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Statuses deleted successfully'], 200);
        }
        else{
            if($this->statusRepository->delete($uuid)){
                session()->flash('success', 'Success !! Status has been deleted.');
            }else{
                session()->flash('error', 'Error !! Error deleting Status. Try again.');
            }
            return redirect('admin/system/statuses');
        }
    }

}


<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\WriterController;
use App\Http\Requests\PaymentRequest;
use App\Writer\Repositories\Contracts\PaymentInterface;

class PaymentsController extends WriterController
{
    protected $paymentRepository;

    public function __construct(PaymentInterface $paymentRepository)
    {
        $this->middleware('admin');
        $this->paymentRepository = $paymentRepository;
    }

    /**
     * Display a listing of the resource.
     *     * @return Response
     */
    public function index()
    {
        $payments = $this->paymentRepository->getAll();
        $invoices= $this->paymentRepository->invoicesSelect();
        $payment_methods = $this->paymentRepository->paymentMethodsSelect();

        return view('admin.payments.index', compact('payments', 'invoices', 'payment_methods'));
    }

    /**
     * Show form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $payments = $this->paymentRepository->getAll();
        $invoices= $this->paymentRepository->invoicesSelect();
        $payment_methods = $this->paymentRepository->paymentMethodsSelect();

        return view('admin.payments.create', compact('invoices', 'payment_methods'));
    }

    /**
     * Store new data
     * @param PaymentRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(PaymentRequest $request)
    {
        if($this->paymentRepository->create($request->all())){
            session()->flash('success', 'Success !! Payment has been added.');
            return response()->json(array('success' => true, 'msg' => 'Payment created'), 201);
        }
        return redirect('admin/payments');
    }

    /**
     * Show Edit Form
     * @param $uuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($uuid)
    {
        $payment = $this->paymentRepository->getById($uuid);
        return view('admin.payments.edit', compact('payment'));
    }

    /**
     * Update data
     * @param PaymentRequest $request
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(PaymentRequest $request, $uuid)
    {
        if($this->paymentRepository->update($request->all(), $uuid) ){
            session()->flash('success', 'Success !! Payment has been Updated.');
            return response()->json(array('success' => true, 'msg' => 'Payment Updated'), 201);
        }
        return redirect('admin/payments');

    }

    /**
     * Delete
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($uuid)
    {
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $fuel){
                $this->paymentRepository->delete($fuel);
            }
            session()->flash('success', 'Success !! Payments have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Payments deleted successfully'], 200);
        }
        else{
            if($this->paymentRepository->delete($uuid)){
                session()->flash('success', 'Success !! Payment has been deleted.');
            }else{
                session()->flash('error', 'Error !! Error deleting Payment. Try again.');
            }
            return redirect('admin/payments');
        }
    }

}


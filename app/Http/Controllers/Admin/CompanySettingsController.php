<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\WriterController;
use App\Http\Requests\CompanySettingRequest;
use App\Writer\Repositories\Contracts\CompanySettingInterface;

class CompanySettingsController extends WriterController
{
    protected $companySettingRepository;

    public function __construct(CompanySettingInterface $companySettingRepository)
    {
        $this->middleware('admin');
        $this->companySettingRepository = $companySettingRepository;
    }

    /**
     * Display a listing of the resource.
     *     * @return Response
     */
    public function index()
    {
        $settings = $this->companySettingRepository->first();
        return view('admin.settings.company', compact('settings'));
    }

    /**
     * Show form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.companySettings.create');
    }

    /**
     * Store new data
     * @param CompanySettingRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CompanySettingRequest $request)
    {
        if($this->companySettingRepository->create($request->all())){
            session()->flash('success', 'Success !! CompanySetting has been added.');
            return response()->json(array('success' => true, 'msg' => 'Company Settings created'), 201);
        }
        return redirect('admin/system/company_settings');
    }

    /**
     * Show Edit Form
     * @param $uuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($uuid)
    {
        $companySetting = $this->companySettingRepository->getById($uuid);
        return view('admin.companySettings.edit', compact('companySetting'));
    }

    /**
     * Update data
     * @param CompanySettingRequest $request
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(CompanySettingRequest $request, $uuid)
    {
        if($this->companySettingRepository->update($request->all(), $uuid) ){
            session()->flash('success', 'Success !! Company Settings has been Updated.');
            return response()->json(array('success' => true, 'msg' => 'Company Settings Updated'), 201);
        }
        return redirect('admin/system/company_settings');

    }

    /**
     * Delete
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($uuid)
    {
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $fuel){
                $this->companySettingRepository->delete($fuel);
            }
            session()->flash('success', 'Success !! Company Settings have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Company Settings deleted successfully'], 200);
        }
        else{
            if($this->companySettingRepository->delete($uuid)){
                session()->flash('success', 'Success !! Company Settings has been deleted.');
            }else{
                session()->flash('error', 'Error !! Error deleting Company Settings. Try again.');
            }
            return redirect('admin/system/company_settings');
        }
    }

}


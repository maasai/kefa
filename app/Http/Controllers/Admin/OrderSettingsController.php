<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\WriterController;
use App\Http\Requests\OrderSettingRequest;
use App\Writer\Repositories\Contracts\OrderSettingInterface;

class OrderSettingsController extends WriterController
{
    protected $orderSettingRepository;

    public function __construct(OrderSettingInterface $orderSettingRepository)
    {
        $this->middleware('admin');
        $this->orderSettingRepository = $orderSettingRepository;
    }

    /**
     * Display a listing of the resource.
     *     * @return Response
     */
    public function index()
    {
        $settings = $this->orderSettingRepository->first();
        return view('admin.settings.order', compact('settings'));
    }


    /**
     * Store new data
     * @param OrderSettingRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(OrderSettingRequest $request)
    {
        if($this->orderSettingRepository->create($request->all())){
            session()->flash('success', 'Success !! Order Settings has been added.');
            return response()->json(array('success' => true, 'msg' => 'Order Settings created'), 201);
        }
        return redirect('admin/system/order_settings');
    }

    /**
     * Show Edit Form
     * @param $uuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($uuid)
    {
        $orderSetting = $this->orderSettingRepository->getById($uuid);
        return view('admin.orderSettings.edit', compact('orderSetting'));
    }

    /**
     * Update data
     * @param OrderSettingRequest $request
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(OrderSettingRequest $request, $uuid)
    {
        if($this->orderSettingRepository->update($request->all(), $uuid) ){
            session()->flash('success', 'Success !! Order Settings has been Updated.');
            return response()->json(array('success' => true, 'msg' => 'Order Settings Updated'), 201);
        }
        return redirect('admin/system/order_settings');

    }

    /**
     * Delete
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($uuid)
    {
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $fuel){
                $this->orderSettingRepository->delete($fuel);
            }
            session()->flash('success', 'Success !! Order Settings have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Order Settings deleted successfully'], 200);
        }
        else{
            if($this->orderSettingRepository->delete($uuid)){
                session()->flash('success', 'Success !! Order Settings has been deleted.');
            }else{
                session()->flash('error', 'Error !! Error deleting Order Settings. Try again.');
            }
            return redirect('admin/system/order_settings');
        }
    }

}


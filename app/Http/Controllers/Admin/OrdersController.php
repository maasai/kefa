<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\WriterController;
use App\Http\Requests\OrderRequest;
use App\Writer\Repositories\Contracts\OrderInterface;
use App\Writer\Repositories\Contracts\OrderSettingInterface;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;

class OrdersController extends WriterController
{
    protected $orderRepository;

    public function __construct(OrderInterface $orderRepository)
    {
        $this->middleware('admin');
        $this->orderRepository = $orderRepository;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $title = false;

        if(!empty(trim(Input::get('status')))){
            $orders = $this->orderRepository->where('status_id', Input::get('status'))->get();
            $title = true;
        }else{
            $orders = $this->orderRepository->getAll();
        }
        return view('admin.orders.index', compact('orders', 'title'));
    }

    /**
     * Show form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $clients = $this->orderRepository->clients_select();
        $subjects = $this->orderRepository->subjects_select();
        $levels = $this->orderRepository->levels_select();
        $types = $this->orderRepository->types_select();
        $spacings = $this->orderRepository->spacings_select();
        $styles = $this->orderRepository->styles_select();
        $statuses = $this->orderRepository->statuses_select();
        $urgencies = $this->orderRepository->urgencies_select();


        return view('admin.orders.create', compact('clients', 'subjects', 'levels', 'types', 'spacings', 'styles', 'statuses', 'urgencies'));
    }

    /**
     * Store new data
     * @param OrderRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(OrderRequest $request)
    {
        if($this->orderRepository->create($request->all())){
            session()->flash('success', 'Success !! Order has been added.');
            return response()->json(array('success' => true, 'msg' => 'Order created'), 201);
        }
        return redirect('admin/orders');
    }

    /**
     * Show Edit Form
     * @param $uuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($uuid)
    {
        $order = $this->orderRepository->getById($uuid);

        $clients = $this->orderRepository->clients_select();
        $subjects = $this->orderRepository->subjects_select();
        $levels = $this->orderRepository->levels_select();
        $types = $this->orderRepository->types_select();
        $spacings = $this->orderRepository->spacings_select();
        $styles = $this->orderRepository->styles_select();
        $statuses = $this->orderRepository->statuses_select();
        $urgencies = $this->orderRepository->urgencies_select();



        return view('admin.orders.edit', compact('order', 'clients', 'subjects', 'levels', 'types', 'spacings', 'styles', 'statuses', 'urgencies'));
    }

    /**
     * Update data
     * @param OrderRequest $request
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(OrderRequest $request, $uuid)
    {
        if($this->orderRepository->update($request->all(), $uuid) ){
            session()->flash('success', 'Success !! Order has been Updated.');
            return response()->json(array('success' => true, 'msg' => 'Order Updated'), 201);
        }
        return redirect('admin/orders');

    }

    public function show($uuid)
    {
        $order = $this->orderRepository->getById($uuid);


        $clients = $this->orderRepository->clients_select();
        $subjects = $this->orderRepository->subjects_select();
        $levels = $this->orderRepository->levels_select();
        $types = $this->orderRepository->types_select();
        $spacings = $this->orderRepository->spacings_select();
        $styles = $this->orderRepository->styles_select();
        $statuses = $this->orderRepository->statuses_select();

        return view('admin.orders.show', compact('order', 'clients', 'subjects', 'levels', 'types', 'spacings', 'styles', 'statuses'));

    }

    /**
     * Delete
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($uuid)
    {
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $fuel){
                $this->orderRepository->delete($fuel);
            }
            session()->flash('success', 'Success !! Orders have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Orders deleted successfully'], 200);
        }
        else{
            if($this->orderRepository->delete($uuid)){
                session()->flash('success', 'Success !! Order has been deleted.');
            }else{
                session()->flash('error', 'Error !! Error deleting Order. Try again.');
            }
            return redirect('admin/orders');
        }
    }

}


<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\WriterController;
use App\Http\Requests\InvoiceRequest;
use App\Writer\Repositories\Contracts\InvoiceInterface;
use App\Writer\Repositories\Contracts\OrderInterface;

class InvoicesController extends WriterController
{
    protected $invoiceRepository, $orderRepository;

    public function __construct(InvoiceInterface $invoiceRepository, OrderInterface $orderRepository)
    {
        $this->middleware('admin');
        $this->invoiceRepository = $invoiceRepository;
        $this->orderRepository = $orderRepository;

    }

    /**
     * Display a listing of the resource.
     *     * @return Response
     */
    public function index()
    {
        $invoices = $this->invoiceRepository->getAll();
        return view('admin.invoices.index', compact('invoices'));
    }

    /**
     * Show form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {

        $clients = [];
        $taxes   = [];
        $invoiceSetting =[];
        $invoice_number = '';
        $orders = $this->orderRepository->orders_select();
        $ordersAll = $this->orderRepository->getAll();

        return view('admin.invoices.create',compact('clients', 'taxes', 'invoiceSetting', 'invoice_number', 'orders', 'ordersAll'));

    }

    /**
     * Store new data
     * @param InvoiceRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(InvoiceRequest $request)
    {
        if($this->invoiceRepository->create($request->all())){
            session()->flash('success', 'Success !! Invoice has been added.');
            return response()->json(array('success' => true, 'msg' => 'Invoice created'), 201);
        }
        return redirect('admin/system/invoices');
    }

    /**
     * Show Edit Form
     * @param $uuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($uuid)
    {
        $invoice = $this->invoiceRepository->getById($uuid);
        return view('admin.invoices.edit', compact('invoice'));
    }

    /**
     * Update data
     * @param InvoiceRequest $request
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(InvoiceRequest $request, $uuid)
    {
        if($this->invoiceRepository->update($request->all(), $uuid) ){
            session()->flash('success', 'Success !! Invoice has been Updated.');
            return response()->json(array('success' => true, 'msg' => 'Invoice Updated'), 201);
        }
        return redirect('admin/system/invoices');

    }

    /**
     * Delete
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($uuid)
    {
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $fuel){
                $this->invoiceRepository->delete($fuel);
            }
            session()->flash('success', 'Success !! Invoices have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Invoices deleted successfully'], 200);
        }
        else{
            if($this->invoiceRepository->delete($uuid)){
                session()->flash('success', 'Success !! Invoice has been deleted.');
            }else{
                session()->flash('error', 'Error !! Error deleting Invoice. Try again.');
            }
            return redirect('admin/system/invoices');
        }
    }

}


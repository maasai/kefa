<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\WriterController;
use App\Http\Requests\InvoiceSettingRequest;
use App\Writer\Repositories\Contracts\InvoiceSettingInterface;

class InvoiceSettingsController extends WriterController
{
    protected $invoiceSettingRepository;

    public function __construct(InvoiceSettingInterface $invoiceSettingRepository)
    {
        $this->middleware('admin');
        $this->invoiceSettingRepository = $invoiceSettingRepository;
    }

    /**
     * Display a listing of the resource.
     *     * @return Response
     */
    public function index()
    {
        $settings = $this->invoiceSettingRepository->first();
        return view('admin.settings.invoice', compact('settings'));
    }

    /**
     * Show form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.invoiceSettings.create');
    }

    /**
     * Store new data
     * @param InvoiceSettingRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(InvoiceSettingRequest $request)
    {
        if($this->invoiceSettingRepository->create($request->all())){
            session()->flash('success', 'Success !! Invoice Settings has been added.');
            return response()->json(array('success' => true, 'msg' => 'Invoice Settings created'), 201);
        }
        return redirect('admin/system/invoice_settings');
    }

    /**
     * Show Edit Form
     * @param $uuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($uuid)
    {
        $invoiceSetting = $this->invoiceSettingRepository->getById($uuid);
        return view('admin.invoiceSettings.edit', compact('invoiceSetting'));
    }

    /**
     * Update data
     * @param InvoiceSettingRequest $request
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(InvoiceSettingRequest $request, $uuid)
    {
        if($this->invoiceSettingRepository->update($request->all(), $uuid) ){
            session()->flash('success', 'Success !! Invoice Settings has been Updated.');
            return response()->json(array('success' => true, 'msg' => 'Invoice Settings Updated'), 201);
        }
        return redirect('admin/system/invoice_settings');

    }

    /**
     * Delete
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($uuid)
    {
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $fuel){
                $this->invoiceSettingRepository->delete($fuel);
            }
            session()->flash('success', 'Success !! Invoice Settings have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Invoice Settings deleted successfully'], 200);
        }
        else{
            if($this->invoiceSettingRepository->delete($uuid)){
                session()->flash('success', 'Success !! Invoice Settings has been deleted.');
            }else{
                session()->flash('error', 'Error !! Error deleting Invoice Settings. Try again.');
            }
            return redirect('admin/system/invoice_settings');
        }
    }

}


<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\WriterController;
use App\Http\Requests\UrgencyRequest;
use App\Models\Level;
use App\Models\Urgency;
use App\Writer\Repositories\Contracts\UrgencyInterface;

class UrgenciesController extends WriterController
{
    protected $urgencyRepository;

    public function __construct(UrgencyInterface $urgencyRepository)
    {
        $this->middleware('admin');
        $this->urgencyRepository = $urgencyRepository;
    }

    /**
     * Display a listing of the resource.
     *     * @return Response
     */
    public function index()
    {
       // $urgencies = $this->urgencyRepository->getAll();
       $urgencies = Urgency::all();
       // $urgency = Urgency::first();
       // $levels = Urgency::first()->levels;
        $academic_levels = Level::with('urgencies')->get();
        return view('admin.urgencies.index', compact( 'urgencies',  'academic_levels'));
    }

    /**
     * Show form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.urgencies.create');
    }

    /**
     * Store new data
     * @param UrgencyRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(UrgencyRequest $request)
    {
        if($this->urgencyRepository->create($request->all())){
            session()->flash('success', 'Success !! Urgency has been added.');
            return response()->json(array('success' => true, 'msg' => 'Urgency created'), 201);
        }
        return redirect('admin/system/urgencies');
    }

    /**
     * Show Edit Form
     * @param $uuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($uuid)
    {
        $urgency = $this->urgencyRepository->getById($uuid);
        return view('admin.urgencies.edit', compact('urgency'));
    }

    /**
     * Update data
     * @param UrgencyRequest $request
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(UrgencyRequest $request, $uuid)
    {
        if($this->urgencyRepository->update($request->all(), $uuid) ){
            session()->flash('success', 'Success !! Urgency has been Updated.');
            return response()->json(array('success' => true, 'msg' => 'Urgency Updated'), 201);
        }
        return redirect('admin/system/urgencies');

    }

    /**
     * Delete
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($uuid)
    {
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $fuel){
                $this->urgencyRepository->delete($fuel);
            }
            session()->flash('success', 'Success !! Writing Urgencies have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Writing Urgencies deleted successfully'], 200);
        }
        else{
            if($this->urgencyRepository->delete($uuid)){
                session()->flash('success', 'Success !! Urgency has been deleted.');
            }else{
                session()->flash('error', 'Error !! Error deleting Urgency. Try again.');
            }
            return redirect('admin/system/urgencies');
        }
    }

}


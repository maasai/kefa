<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\WriterController;
use App\Http\Requests\SubjectRequest;
use App\Writer\Repositories\Contracts\SubjectInterface;

class SubjectsController extends WriterController
{
    protected $subjectRepository;

    public function __construct(SubjectInterface $subjectRepository)
    {
        $this->middleware('admin');
        $this->subjectRepository = $subjectRepository;
    }

    /**
     * Display a listing of the resource.
     *     * @return Response
     */
    public function index()
    {
        $subjects = $this->subjectRepository->getAll();
        return view('admin.subjects.index', compact('subjects'));
    }

    /**
     * Show form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.subjects.create');
    }

    /**
     * Store new data
     * @param SubjectRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(SubjectRequest $request)
    {
        if($this->subjectRepository->create($request->all())){
            session()->flash('success', 'Success !! Subject has been added.');
            return response()->json(array('success' => true, 'msg' => 'Subject created'), 201);
        }
        return redirect('admin/system/subjects');
    }

    /**
     * Show Edit Form
     * @param $uuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($uuid)
    {
        $subject = $this->subjectRepository->getById($uuid);
        return view('admin.subjects.edit', compact('subject'));
    }

    /**
     * Update data
     * @param SubjectRequest $request
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(SubjectRequest $request, $uuid)
    {
        if($this->subjectRepository->update($request->all(), $uuid) ){
            session()->flash('success', 'Success !! Subject has been Updated.');
            return response()->json(array('success' => true, 'msg' => 'Subject Updated'), 201);
        }
        return redirect('admin/system/subjects');

    }

    /**
     * Delete
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($uuid)
    {
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $fuel){
                $this->subjectRepository->delete($fuel);
            }
            session()->flash('success', 'Success !! Subjects have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Subjects deleted successfully'], 200);
        }
        else{
            if($this->subjectRepository->delete($uuid)){
                session()->flash('success', 'Success !! Subject has been deleted.');
            }else{
                session()->flash('error', 'Error !! Error deleting Subject. Try again.');
            }
            return redirect('admin/system/subjects');
        }
    }

}


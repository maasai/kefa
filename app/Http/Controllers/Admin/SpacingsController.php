<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\WriterController;
use App\Http\Requests\SpacingRequest;
use App\Writer\Repositories\Contracts\SpacingInterface;

class SpacingsController extends WriterController
{
    protected $spacingRepository;

    public function __construct(SpacingInterface $spacingRepository)
    {
        $this->middleware('admin');
        $this->spacingRepository = $spacingRepository;
    }

    /**
     * Display a listing of the resource.
     *     * @return Response
     */
    public function index()
    {
        $spacings = $this->spacingRepository->getAll();
        return view('admin.spacings.index', compact('spacings'));
    }

    /**
     * Show form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.spacings.create');
    }

    /**
     * Store new data
     * @param SpacingRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(SpacingRequest $request)
    {
        if($this->spacingRepository->create($request->all())){
            session()->flash('success', 'Success !! Spacing has been added.');
            return response()->json(array('success' => true, 'msg' => 'Spacing created'), 201);
        }
        return redirect('admin/system/spacings');
    }

    /**
     * Show Edit Form
     * @param $uuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($uuid)
    {
        $spacing = $this->spacingRepository->getById($uuid);
        return view('admin.spacings.edit', compact('spacing'));
    }

    /**
     * Update data
     * @param SpacingRequest $request
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(SpacingRequest $request, $uuid)
    {
        if($this->spacingRepository->update($request->all(), $uuid) ){
            session()->flash('success', 'Success !! Spacing has been Updated.');
            return response()->json(array('success' => true, 'msg' => 'Spacing Updated'), 201);
        }
        return redirect('admin/system/spacings');

    }

    /**
     * Delete
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($uuid)
    {
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $fuel){
                $this->spacingRepository->delete($fuel);
            }
            session()->flash('success', 'Success !! Spacings have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Spacings deleted successfully'], 200);
        }
        else{
            if($this->spacingRepository->delete($uuid)){
                session()->flash('success', 'Success !! Spacing has been deleted.');
            }else{
                session()->flash('error', 'Error !! Error deleting Spacing. Try again.');
            }
            return redirect('admin/system/spacings');
        }
    }

}


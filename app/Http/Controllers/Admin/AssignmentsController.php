<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\WriterController;
use App\Http\Requests\AssignmentRequest;
use App\Writer\Repositories\Contracts\AssignmentInterface;

class AssignmentsController extends WriterController
{
    protected $assignmentRepository;

    public function __construct(AssignmentInterface $assignmentRepository)
    {
        $this->middleware('admin');
        $this->assignmentRepository = $assignmentRepository;
    }

    /**
     * Display a listing of the resource.
     *     * @return Response
     */
    public function index()
    {
        $assignments = $this->assignmentRepository->getAll();
        return view('admin.assignments.index', compact('assignments'));
    }

    /**
     * Show form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.assignments.create');
    }

    /**
     * Store new data
     * @param AssignmentRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(AssignmentRequest $request)
    {
        if($this->assignmentRepository->create($request->all())){
            session()->flash('success', 'Success !! Assignment Type has been added.');
            return response()->json(array('success' => true, 'msg' => 'Assignment Type created'), 201);
        }
        return redirect('admin/system/assignments');
    }

    /**
     * Show Edit Form
     * @param $uuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($uuid)
    {
        $assignment = $this->assignmentRepository->getById($uuid);
        return view('admin.assignments.edit', compact('assignment'));
    }

    /**
     * Update data
     * @param AssignmentRequest $request
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(AssignmentRequest $request, $uuid)
    {
        if($this->assignmentRepository->update($request->all(), $uuid) ){
            session()->flash('success', 'Success !! Assignment Type has been Updated.');
            return response()->json(array('success' => true, 'msg' => 'Assignment Type Updated'), 201);
        }
        return redirect('admin/system/assignments');

    }

    /**
     * Delete
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($uuid)
    {
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $fuel){
                $this->assignmentRepository->delete($fuel);
            }
            session()->flash('success', 'Success !! Assignment Types have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Assignment Types deleted successfully'], 200);
        }
        else{
            if($this->assignmentRepository->delete($uuid)){
                session()->flash('success', 'Success !! Assignment Type has been deleted.');
            }else{
                session()->flash('error', 'Error !! Error deleting Assignment Type. Try again.');
            }
            return redirect('admin/system/assignments');
        }
    }

}


<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\WriterController;
use App\Http\Requests\UserRequest;
use App\Writer\Repositories\Contracts\ProfileSettingInterface;
use Illuminate\Support\Facades\Auth;

class ProfileSettingsController extends WriterController
{
    protected $profileSettingRepository;

    public function __construct(ProfileSettingInterface $profileSettingRepository)
    {
        $this->middleware('admin');
        $this->profileSettingRepository = $profileSettingRepository;
    }

    /**
     * Display a listing of the resource.
     *     * @return Response
     */
    public function index()
    {
        $settings =  Auth::user();
        return view('admin.settings.profile', compact('settings'));
    }

    /**
     * Show form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.profileSettings.create');
    }

    /**
     * Store new data
     * @param UserRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(UserRequest $request)
    {
        if($this->profileSettingRepository->create($request->all())){
            session()->flash('success', 'Success !! Profile Settings has been added.');
            return response()->json(array('success' => true, 'msg' => 'Profile Settings created'), 201);
        }
        return redirect('admin/system/profile_settings');
    }

    /**
     * Show Edit Form
     * @param $uuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($uuid)
    {
        $profileSetting = $this->profileSettingRepository->getById($uuid);
        return view('admin.profileSettings.edit', compact('profileSetting'));
    }

    /**
     * Update data
     * @param UserRequest $request
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(UserRequest $request, $uuid)
    {
        if($this->profileSettingRepository->update($request->all(), $uuid) ){
            session()->flash('success', 'Success !! Profile Settings has been Updated.');
            return response()->json(array('success' => true, 'msg' => 'Profile Settings Updated'), 201);
        }
        return redirect('admin/system/profile_settings');

    }

    /**
     * Delete
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($uuid)
    {
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $fuel){
                $this->profileSettingRepository->delete($fuel);
            }
            session()->flash('success', 'Success !! Profile Settings have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Profile Settings deleted successfully'], 200);
        }
        else{
            if($this->profileSettingRepository->delete($uuid)){
                session()->flash('success', 'Success !! Profile Settings has been deleted.');
            }else{
                session()->flash('error', 'Error !! Error deleting Profile Settings. Try again.');
            }
            return redirect('admin/system/profile_settings');
        }
    }

}




<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\WriterController;
use App\Http\Requests\PriceRequest;
use App\Models\Level;
use App\Models\Price;
use App\Models\Urgency;
use App\Writer\Repositories\Contracts\LevelInterface;
use App\Writer\Repositories\Contracts\OrderSettingInterface;
use App\Writer\Repositories\Contracts\PriceInterface;
use App\Writer\Repositories\Contracts\UrgencyInterface;

class PricesController extends WriterController
{
    protected $priceRepository, $levelRepository, $urgencyRepository, $orderSettingRepository;

    public function __construct(PriceInterface $priceRepository, LevelInterface $levelRepository,
                                UrgencyInterface $urgencyRepository, OrderSettingInterface $orderSettingRepository)
    {
        $this->middleware('admin');
        $this->priceRepository = $priceRepository;
        $this->levelRepository = $levelRepository;
        $this->urgencyRepository = $urgencyRepository;
        $this->orderSettingRepository = $orderSettingRepository;

    }

    /**
     * Display a listing of the resource.
     *     * @return Response
     */
    public function index()
    {
        //\DB::connection()->enableQueryLog();
        $prices = $this->priceRepository->getAll();
        $urgencies = $this->urgencyRepository->getAll();
        $academic_levels = $this->levelRepository->getAll();
        $settings = $this->orderSettingRepository->first();
        $urgencies_select = $this->priceRepository->urgencies_select();
        $levels_select = $this->priceRepository->levels_select();

        return view('admin.prices.index', compact('urgencies',  'academic_levels', 'prices', 'settings', 'urgencies_select', 'levels_select'));
    }

    /**
     * Show form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.prices.create');
    }

    /**
     * Store new data
     * @param PriceRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(PriceRequest $request)
    {
        $factors = $this->priceRepository->getAll();

        $data = [
            'urgency_id' => $request->get('urgency_id'),
            'level_id' => $request->get('level_id'),
            'factor' => $request->get('factor')
        ];

        $level_urgency_id = $request->get('urgency_id').$request->get('level_id');

        $existing_factors = array();

        foreach($factors as $factor){

            $existing_factors[$factor->uuid] = $factor->urgency_id.$factor->level_id;
        }

        if(in_array($level_urgency_id, $existing_factors)){
            //get uuid and update
            $key = array_search ($level_urgency_id, $existing_factors);
            $this->priceRepository->update($data, $key);
            session()->flash('success', 'Success !! Price has been Updated.');

        }else{
            //save new
            $this->priceRepository->create($data);
            session()->flash('success', 'Success !! Price has been Created.');
        }

    }

    /**
     * Show Edit Form
     * @param $uuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($uuid)
    {
     /*   $price = $this->priceRepository->getById($uuid);
        return view('admin.prices.edit', compact('price'));*/
    }

    /**
     * Update data
     * @param PriceRequest $request
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(PriceRequest $request, $uuid)
    {
        /*if($this->priceRepository->update($request->all(), $uuid) ){
            session()->flash('success', 'Success !! Price has been Updated.');
            return response()->json(array('success' => true, 'msg' => 'Price Updated'), 201);
        }
        return redirect('admin/system/prices');*/

    }

    /**
     * Delete
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($uuid)
    {
       /* if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $fuel){
                $this->priceRepository->delete($fuel);
            }
            session()->flash('success', 'Success !! Prices have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Prices deleted successfully'], 200);
        }
        else{
            if($this->priceRepository->delete($uuid)){
                session()->flash('success', 'Success !! Price has been deleted.');
            }else{
                session()->flash('error', 'Error !! Error deleting Price. Try again.');
            }
            return redirect('admin/system/prices');
        }*/
    }

}


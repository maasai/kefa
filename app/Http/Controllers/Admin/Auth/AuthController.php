<?php


namespace app\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Validator;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    protected $guard = 'admin';

    /**
     * Create a new authentication controller instance.
     * AuthController constructor.
     */
    public function __construct()
    {
       $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Display user login form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        if (view()->exists('auth.authenticate')) {
            return view('auth.authenticate');
        }

        return view('admin.auth.login');
    }
    public function showRegistrationForm()
    {
        return view('admin.auth.register');
    }


    /**
     * Log in the user
     * @param Request $request
     * @return mixed
     */
    public function postLogin(Request $request) {

        //Validate the user input on server side
        if($this->validator($request->all())->fails()){
            return Redirect::to('admin/login')
                ->withInput()
                ->withErrors($this->validator($request->all())->messages());
        }

        //Make an attempt to login the user
        if(Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])) {
            // Authentication passed...
            return redirect()->intended('admin');
        } else {
            //invalid credentials
            return Redirect::to('/admin/login')
                ->withErrors([
                    'error' => 'Provided info did not match any record.',
                ])
                ->withInput();
        }
    }

    /**
     * Log out the user
     * @return mixed
     */
    public function getLogout() {
        Auth::guard('admin')->logout();
        Auth::logout();
        \Session::flush();

        return Redirect::to('admin');
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'email' => 'required|max:255',
            'password' => 'required|min:2'
        ]);

        return $validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
<?php


namespace app\Http\Controllers\Admin;


use App\Http\Controllers\WriterController;
use Illuminate\Routing\Controller;

class DashboardController extends WriterController
{

    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        return view('admin.dashboard.index');
    }

}
<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\WriterController;
use App\Http\Requests\PaymentMethodRequest;
use App\Writer\Repositories\Contracts\PaymentMethodInterface;

class PaymentMethodsController extends WriterController
{
    protected $paymentMethodRepository;

    public function __construct(PaymentMethodInterface $paymentMethodRepository)
    {
        $this->middleware('admin');
        $this->paymentMethodRepository = $paymentMethodRepository;
    }

    /**
     * Display a listing of the resource.
     *     * @return Response
     */
    public function index()
    {
        $paymentMethods = $this->paymentMethodRepository->getAll();
        return view('admin.paymentmethods.index', compact('paymentMethods'));
    }

    /**
     * Show form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.paymentmethods.create');
    }

    /**
     * Store new data
     * @param PaymentMethodRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(PaymentMethodRequest $request)
    {
        if($this->paymentMethodRepository->create($request->all())){
            session()->flash('success', 'Success !! Payment Method has been added.');
            return response()->json(array('success' => true, 'msg' => 'Payment Method created'), 201);
        }
        return redirect('admin/system/payment_methods');
    }

    /**
     * Show Edit Form
     * @param $uuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($uuid)
    {
        $paymentMethod = $this->paymentMethodRepository->getById($uuid);
        return view('admin.paymentmethods.edit', compact('paymentMethod'));
    }

    /**
     * Update data
     * @param PaymentMethodRequest $request
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(PaymentMethodRequest $request, $uuid)
    {
        if($this->paymentMethodRepository->update($request->all(), $uuid) ){
            session()->flash('success', 'Success !! Payment Method has been Updated.');
            return response()->json(array('success' => true, 'msg' => 'Payment Method Updated'), 201);
        }
        return redirect('admin/system/payment_methods');

    }

    /**
     * Delete
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($uuid)
    {
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $fuel){
                $this->paymentMethodRepository->delete($fuel);
            }
            session()->flash('success', 'Success !! Payment Methods have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Payment Methods deleted successfully'], 200);
        }
        else{
            if($this->paymentMethodRepository->delete($uuid)){
                session()->flash('success', 'Success !! Payment Method has been deleted.');
            }else{
                session()->flash('error', 'Error !! Error deleting Payment Method. Try again.');
            }
            return redirect('admin/system/payment_methods');
        }
    }

}


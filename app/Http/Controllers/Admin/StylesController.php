<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\WriterController;
use App\Http\Requests\StyleRequest;
use App\Writer\Repositories\Contracts\StyleInterface;

class StylesController extends WriterController
{
    protected $styleRepository;

    public function __construct(StyleInterface $styleRepository)
    {
        $this->middleware('admin');
        $this->styleRepository = $styleRepository;
    }

    /**
     * Display a listing of the resource.
     *     * @return Response
     */
    public function index()
    {
        $styles = $this->styleRepository->getAll();
        return view('admin.styles.index', compact('styles'));
    }

    /**
     * Show form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.styles.create');
    }

    /**
     * Store new data
     * @param StyleRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StyleRequest $request)
    {
        if($this->styleRepository->create($request->all())){
            session()->flash('success', 'Success !! Style has been added.');
            return response()->json(array('success' => true, 'msg' => 'Style created'), 201);
        }
        return redirect('admin/system/styles');
    }

    /**
     * Show Edit Form
     * @param $uuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($uuid)
    {
        $style = $this->styleRepository->getById($uuid);
        return view('admin.styles.edit', compact('style'));
    }

    /**
     * Update data
     * @param StyleRequest $request
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(StyleRequest $request, $uuid)
    {
        if($this->styleRepository->update($request->all(), $uuid) ){
            session()->flash('success', 'Success !! Style has been Updated.');
            return response()->json(array('success' => true, 'msg' => 'Style Updated'), 201);
        }
        return redirect('admin/system/styles');

    }

    /**
     * Delete
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($uuid)
    {
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $fuel){
                $this->styleRepository->delete($fuel);
            }
            session()->flash('success', 'Success !! Styles have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Styles deleted successfully'], 200);
        }
        else{
            if($this->styleRepository->delete($uuid)){
                session()->flash('success', 'Success !! Style has been deleted.');
            }else{
                session()->flash('error', 'Error !! Error deleting Style. Try again.');
            }
            return redirect('admin/system/styles');
        }
    }

}


<?php
namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\WriterController;
use App\Writer\Repositories\Contracts\OrderInterface;
use App\Writer\Repositories\Contracts\PriceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class PricesController extends  WriterController
{
    protected $priceRepository, $orderRepository;

    public function __construct(PriceInterface $priceRepository, OrderInterface $orderRepository)
    {
        $this->priceRepository = $priceRepository;
        $this->orderRepository = $orderRepository;

    }

    public function store(Request $request)
    {
        //Fetch data from prices table where urgency_id and level_id are as provided

        $level_id = Input::get('level_id');
        $urgency_id = Input::get('urgency_id');
        $pages = Input::get('pages');

        $amount = $this->orderRepository->calculateAmount($level_id, $urgency_id, $pages);

        if($request->ajax()){
            $response = array(
                'success' => true,
                'amount' => number_format($amount,2)
            );

            return json_encode($response);
        }else{
            return number_format($amount,2);
        }



     /*   if(null!=$level_id && null!=$urgency_id){
            $factor = $this->priceRepository->where('level_id', $level_id)->where('urgency_id', $urgency_id)->first();

            $amount = $pages*$factor->factor;

            $response = array(
                'success' => true,
                'amount' => number_format($amount,2)
            );
        }else{
            $amount = 0;
            $response = array(
                'success' => false,
                'amount' => 0
            );
        }

        if($request->ajax()){
            echo json_encode($response);
        }else{
            return number_format($amount,2);

        }*/

    }
}
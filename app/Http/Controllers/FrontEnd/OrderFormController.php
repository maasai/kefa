<?php
namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\WriterController;
use App\Http\Requests\OrderFormRequest;
use App\Writer\Repositories\Contracts\ClientInterface;
use App\Writer\Repositories\Contracts\OrderInterface;
use Illuminate\Support\Facades\Session;

class OrderFormController extends  WriterController
{
    protected $orderRepository, $orderSetting, $clientRepository;

    public function __construct(OrderInterface $orderRepository, ClientInterface $clientRepository)
    {
//        $this->middleware('admin');
        $this->orderRepository = $orderRepository;
        $this->clientRepository = $clientRepository;
    }

    public function store(OrderFormRequest $request)
    {
        //Get client Id and associate the order
        $client = $this->clientRepository->where('email', $request['email'])->get()->first();

        if(isset($client)){
            //We have a return client. Redirect to login
            $request['client_id'] = $client->uuid;
            //We have a client, redirect to login with all the data. It will be saved after login.

            //TODO send the session data as session->put() so as to be available on page reload.
            return redirect('client/login')
                ->with('email',$request['email'])
                ->with('temp_order', $request->all());
        }else{

            //show registration form. use session put to avoid losing data on register page reload.
            Session::put('email', $request['email']);
            Session::put('temp_order', $request->all());
            return redirect('client/register');

        }

        return redirect('admin/orders');
    }
}
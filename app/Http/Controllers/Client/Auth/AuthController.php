<?php


namespace App\Http\Controllers\Client\Auth;

use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Writer\Repositories\Contracts\OrderInterface;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Validator;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/client';

    protected $guard = 'client';

    /**
     * Create a new authentication controller instance.
     * AuthController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Display user login form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        if (view()->exists('auth.authenticate')) {
            return view('auth.authenticate');
        }

        return view('client.auth.login');
    }
    public function getRegister()
    {
        return view('client.auth.register');
    }

    /**
     * Log in the user
     * @param Request $request
     * @param OrderInterface $orderRepository
     * @return mixed
     */
    public function postLogin(Request $request, OrderInterface $orderRepository)
    {

       //Validate the user input on server side
        if($this->validator($request->all())->fails()){
            return Redirect::to('client/login')
                ->withInput()
                ->withErrors($this->validator($request->all())->messages());
        }
        /*//Make an attempt to login the user
        if(Auth::guard('client')->attempt(['email' => $request->email, 'password' => $request->password])) {
            //Create Order
            if(isset($request->temp_order) && $request->temp_order != null ){
              $data = unserialize($request->temp_order);
                if(is_array($data)){
                    $newOrder = $orderRepository->create($data);
                   //redirect to edit the just created order
                    $redirectRoute = "client/orders/".$newOrder['uuid']."/edit";
                    return Redirect::to($redirectRoute)
                        ->with('success', "Thanks. We just received your order. Please review and update any missing info.");
                }

            }
            // Authentication passed...
            return redirect()->intended('client');
        } else {
            //invalid credentials
            return Redirect::to('/client/login')
                ->withErrors([
                    'error' => 'Provided info did not match any record.',
                ])
                ->withInput();
        }*/


        //Make an attempt to login the user
        //Then save the order from session
        if(Auth::guard('client')->attempt(['email' => $request->email, 'password' => $request->password])) {
            $temp_order = session('temp_order');

            //Create Order
            if(isset($temp_order) && $temp_order != null ){
               // $data = unserialize($request->temp_order);
                if(is_array($temp_order)){
                    $newOrder = $orderRepository->create($temp_order);
                    //redirect to edit the just created order
                    $redirectRoute = "client/orders/".$newOrder['uuid']."/edit";
                    return Redirect::to($redirectRoute)
                        ->with('success', "Thanks. We just received your order. Please review and update any missing info.");
                }

            }
            // Authentication passed...
            return redirect()->intended('client');
        } else {
            //invalid credentials
            return Redirect::to('/client/login')
                ->withErrors([
                    'error' => 'Provided info did not match any record.',
                ])
                ->withInput();
        }
    }

    public function postRegister(Request $request, OrderInterface $orderRepository)
    {
        //create account. Create the order and assign to that new account. Then login the new client to complete their order.

        //Validate the user input on server side
        if($this->registerValidator($request->all())->fails()){
            return Redirect::to('client/register')
                ->withInput()
                ->withErrors($this->registerValidator($request->all())->messages());
        }else{

            $client = $this->create($request->all());
            if($client){

                if(Auth::guard('client')->attempt(['email' => $request->email, 'password' => $request->password])) {
                    $temp_order = session('temp_order');
                    //Create Order
                    if(isset($temp_order) && $temp_order != null ){
                       // dd($temp_order);
                       // $data = unserialize($request->temp_order);
                        if(is_array($temp_order)){
                            $temp_order['client_id'] = $client['uuid'];
                            $newOrder = $orderRepository->create($temp_order);
                            //redirect to edit the just created order
                            $redirectRoute = "client/orders/".$newOrder['uuid']."/edit";
                            return Redirect::to($redirectRoute)
                                ->with('success', "Thanks. We just received your order. Please review and update any missing info.");
                        }

                    }
                    return redirect()->intended('client');
                }

            }

        }
        return Redirect::to('client/register')
            ->withInput()
            ->with('error', "Some ERROR just happened");
    }

    /**
     * Log out the user
     * @return mixed
     */
    public function getLogout() {
        Auth::guard('client')->logout();
        Auth::logout();
        \Session::flush();

        return Redirect::to('client');
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'email' => 'required|max:255',
            'password' => 'required|min:2'
        ]);

        return $validator;
    }

    protected function registerValidator(array $data)
    {
        $validator = Validator::make($data, [
            'email'         => 'required|unique:clients,email',
            'client_name'   => 'required|max:255',
            'password'      => 'confirmed|min:5',
        ]);

        return $validator;
    }

    /**
     * Create a new client instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return Client::create([
            'client_name'   => $data['client_name'],
            'email'         => $data['email'],
            'password'      => bcrypt($data['password']),
        ]);
    }
}
<?php
namespace App\Http\Controllers\Client;


use App\Http\Controllers\WriterController;
use App\Http\Requests\OrderClientRequest;
use App\Http\Requests\OrderRequest;
use App\Writer\Repositories\Contracts\OrderInterface;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;


class OrdersController extends WriterController
{
    protected $orderRepository;

    public function __construct(OrderInterface $orderRepository)
    {
        $this->middleware('client');
        $this->orderRepository = $orderRepository;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $title = false;

        $client =  Auth::guard('client')->user()  ;

        if($client){
            $orders = $this->orderRepository->where('client_id', $client['uuid'])->get();

        }else
            $orders = [];

            return view('client.orders.index', compact('orders', 'title'));
    }

    /**
     * Show form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $subjects = $this->orderRepository->subjects_select();
        $levels = $this->orderRepository->levels_select();
        $types = $this->orderRepository->types_select();
        $spacings = $this->orderRepository->spacings_select();
        $styles = $this->orderRepository->styles_select();
        $urgencies = $this->orderRepository->urgencies_select();

        return view('client.orders.create', compact('subjects', 'levels', 'types', 'spacings', 'styles', 'urgencies'));
    }

    /**
     * Store new data
     * @param OrderClientRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(OrderClientRequest $request)
    {
        $client =  Auth::guard('client')->user();
        $request['client_id'] = $client['uuid'];

        if($this->orderRepository->create($request->all())){
            session()->flash('success', 'Success !! Order has been added.');
            return response()->json(array('success' => true, 'msg' => 'Order created'), 201);
        }
        return redirect('client/orders');
    }

    /**
     * Show Edit Form
     * @param $uuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($uuid)
    {
        $order = $this->orderRepository->getById($uuid);


        $subjects = $this->orderRepository->subjects_select();
        $levels = $this->orderRepository->levels_select();
        $types = $this->orderRepository->types_select();
        $spacings = $this->orderRepository->spacings_select();
        $styles = $this->orderRepository->styles_select();
        $urgencies = $this->orderRepository->urgencies_select();

        return view('client.orders.edit', compact('order', 'subjects', 'levels', 'types', 'spacings', 'styles', 'urgencies'));
    }

    /**
     * Update data
     * @param OrderRequest $request
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(OrderRequest $request, $uuid)
    {
        if($this->orderRepository->update($request->all(), $uuid) ){
            session()->flash('success', 'Success !! Order has been Updated.');
            return response()->json(array('success' => true, 'msg' => 'Order Updated'), 201);
        }
        return redirect('client/orders');

    }

    public function show($uuid)
    {
        $order = $this->orderRepository->getById($uuid);


        $clients = $this->orderRepository->clients_select();
        $subjects = $this->orderRepository->subjects_select();
        $levels = $this->orderRepository->levels_select();
        $types = $this->orderRepository->types_select();
        $spacings = $this->orderRepository->spacings_select();
        $styles = $this->orderRepository->styles_select();
        $statuses = $this->orderRepository->statuses_select();

        return view('client.orders.show', compact('order', 'clients', 'subjects', 'levels', 'types', 'spacings', 'styles', 'statuses'));

    }

    /**
     * Delete
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($uuid)
    {
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $fuel){
                $this->orderRepository->delete($fuel);
            }
            session()->flash('success', 'Success !! Orders have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Orders deleted successfully'], 200);
        }
        else{
            if($this->orderRepository->delete($uuid)){
                session()->flash('success', 'Success !! Order has been deleted.');
            }else{
                session()->flash('error', 'Error !! Error deleting Order. Try again.');
            }
            return redirect('client/orders');
        }
    }

}


<?php


namespace app\Http\Middleware;

use Illuminate\Support\Facades\Auth;

class RedirectIfNotClient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, \Closure $next, $guard = 'client')
    {
        if (!Auth::guard($guard)->check()) {
            //return redirect('/client/login'); Line below helps redirect to intended page
            return redirect()->guest('/client/login');
        }

        return $next($request);
    }

}
<?php
namespace App\Writer\Repositories\Eloquent;

use App\Models\Urgency;
use App\Writer\Repositories\Contracts\UrgencyInterface;

class UrgencyRepository extends BaseRepository implements UrgencyInterface {

    protected $model, $orderBy  = array('duration', 'asc');

    /**
     * @param Urgency $model
     */
    function __construct(Urgency $model)
    {
        $this->model = $model;
    }


}
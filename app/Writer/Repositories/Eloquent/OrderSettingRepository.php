<?php
namespace App\Writer\Repositories\Eloquent;

use App\Models\OrderSetting;
use App\Writer\Repositories\Contracts\OrderSettingInterface;

class OrderSettingRepository extends BaseRepository implements OrderSettingInterface {

    protected $model;

    /**
     * @param OrderSetting $model
     */
    function __construct(OrderSetting $model)
    {
        $this->model = $model;
    }


}
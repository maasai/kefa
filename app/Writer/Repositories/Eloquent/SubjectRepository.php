<?php
namespace App\Writer\Repositories\Eloquent;

use App\Models\Subject;
use App\Writer\Repositories\Contracts\SubjectInterface;

class SubjectRepository extends BaseRepository implements SubjectInterface {

    protected $model;

    /**
     * @param Subject $model
     */
    function __construct(Subject $model)
    {
        $this->model = $model;
    }


}
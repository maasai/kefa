<?php

namespace App\Writer\Repositories\Eloquent;

use App\Models\PaymentMethod;
use App\Writer\Repositories\Contracts\PaymentMethodInterface;

class PaymentMethodRepository extends BaseRepository implements PaymentMethodInterface {

    protected $model;

    /**
     * @param PaymentMethod $model
     */
    function __construct(PaymentMethod $model)
    {
        $this->model = $model;
    }


}
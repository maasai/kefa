<?php

namespace App\Writer\Repositories\Eloquent;

use App\Models\Payment;
use App\Writer\Repositories\Contracts\PaymentInterface;

class PaymentRepository extends BaseRepository implements PaymentInterface {

    protected $model, $invoices, $paymentMethods;

    /**
     * PaymentRepository constructor.
     * @param Payment $model
     * @param InvoiceRepository $invoices
     * @param PaymentMethodRepository $paymentMethods
     */
    function __construct(Payment $model, InvoiceRepository $invoices, PaymentMethodRepository $paymentMethods)
    {
        $this->model            = $model;
        $this->invoices         = $invoices;
        $this->paymentMethods   = $paymentMethods;
    }

    /**
     * @return array
     */
    public function invoicesSelect()
    {
        $invoices = $this->invoices->sortBy('invoice_number', 'asc')->getAllNoPaginate();
        $options = array();
        $options[''] = 'Select Invoice';
        foreach($invoices as $invoice){
            $options[$invoice->uuid] =  $invoice->invoice_number;
        }
        return $options;
    }


    /**
     * @return array
     */
    public function paymentMethodsSelect()
    {
        $paymentMethods = $this->paymentMethods->sortBy('code', 'asc')->getAllNoPaginate();
        $options = array();
        $options[''] = 'Select Payment Method';
        foreach($paymentMethods as $paymentMethod){
            $options[$paymentMethod->uuid] =  $paymentMethod->code;
        }
        return $options;
    }



}
<?php

namespace App\Writer\Repositories\Eloquent;

use App\Models\CompanySetting;
use App\Writer\Repositories\Contracts\CompanySettingInterface;

class CompanySettingRepository extends BaseRepository implements CompanySettingInterface {

    protected $model;

    /**
     * @param CompanySetting $model
     */
    function __construct(CompanySetting $model)
    {
        $this->model = $model;
    }


}
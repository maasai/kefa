<?php  namespace App\Writer\Repositories\Eloquent;

use App\Models\User;
use App\Writer\Repositories\Contracts\UserInterface;

class UserRepository extends BaseRepository implements UserInterface {

    protected $model;

    /**
     * @param User $model
     */
    function __construct(User $model)
    {
        $this->model = $model;
    }


} 
<?php

namespace App\Writer\Repositories\Eloquent;

use App\Models\Currency;
use App\Writer\Repositories\Contracts\CurrencyInterface;

class CurrencyRepository extends BaseRepository implements CurrencyInterface {

    protected $model;

    /**
     * @param Currency $model
     */
    function __construct(Currency $model)
    {
        $this->model = $model;
    }


}
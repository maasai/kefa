<?php

namespace App\Writer\Repositories\Eloquent;

use App\Models\Style;
use App\Writer\Repositories\Contracts\StyleInterface;

class StyleRepository extends BaseRepository implements StyleInterface {

    protected $model;

    /**
     * @param Style $model
     */
    function __construct(Style $model)
    {
        $this->model = $model;
    }


}
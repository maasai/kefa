<?php
namespace App\Writer\Repositories\Eloquent;

use App\Models\Client;
use App\Writer\Repositories\Contracts\ClientProfileSettingInterface;

class ClientProfileSettingRepository extends BaseRepository implements ClientProfileSettingInterface {

    protected $model;

    /**
     * @param Client $model
     */
    function __construct(Client $model)
    {
        $this->model = $model;
    }


}
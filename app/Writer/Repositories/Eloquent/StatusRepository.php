<?php
namespace App\Writer\Repositories\Eloquent;

use App\Models\Status;
use App\Writer\Repositories\Contracts\StatusInterface;

class StatusRepository extends BaseRepository implements StatusInterface {

    protected $model;

    protected $orderBy  = array('status', 'desc');


    /**
     * @param Status $model
     */
    function __construct(Status $model)
    {
        $this->model = $model;
    }


}
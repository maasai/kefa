<?php

namespace App\Writer\Repositories\Eloquent;

use App\Models\Level;
use App\Writer\Repositories\Contracts\LevelInterface;

class LevelRepository extends BaseRepository implements LevelInterface {

    protected $model;

    protected $orderBy  = array('level_value', 'asc');


    /**
     * @param Level $model
     */
    function __construct(Level $model)
    {
        $this->model = $model;
    }


}
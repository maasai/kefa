<?php  namespace App\Writer\Repositories\Eloquent;

use App\Models\Client;
use App\Writer\Repositories\Contracts\ClientInterface;
use Illuminate\Support\Facades\Lang;

class ClientRepository extends BaseRepository implements ClientInterface {

    protected $model;

    /**
     * @param Client $model
     */
    function __construct(Client $model)
    {
        $this->model = $model;
    }

    public function clientSelect(){
        $clients = $this->getAll();
        $clientList = array('' => Lang::get('app.select_record'));
        foreach($clients as $client)
        {
            $clientList[$client->uuid] = $client->client_name;
        }
        return $clientList;
    }
} 
<?php

namespace App\Writer\Repositories\Eloquent;

use App\Models\Price;
use App\Writer\Repositories\Contracts\LevelInterface;
use App\Writer\Repositories\Contracts\PriceInterface;
use App\Writer\Repositories\Contracts\UrgencyInterface;

class PriceRepository extends BaseRepository implements PriceInterface {

    protected $model, $urgencyRepository, $levelRepository;

    /**
     * PriceRepository constructor.
     * @param Price $model
     * @param UrgencyInterface $urgencyRepository
     * @param LevelInterface $levelRepository
     */
    function __construct(Price $model, UrgencyInterface $urgencyRepository, LevelInterface $levelRepository)
    {
        $this->model = $model;
        $this->urgencyRepository = $urgencyRepository;
        $this->levelRepository = $levelRepository;
    }


    /**
     * @return array
     */
    public function urgencies_select()
    {
        $urgencies = $this->urgencyRepository->sortBy('duration', 'asc')->getAllNoPaginate();
        $options = array();
        $options[''] = 'Select Urgency';
        foreach($urgencies as $urgency){
            $options[$urgency->uuid] =  $urgency->urgency;
        }
        return $options;
    }

    /**
     * @return array
     */
    public function levels_select()
    {
        $levels = $this->levelRepository->sortBy('level_value', 'asc')->getAllNoPaginate();
        $options = array();
        $options[''] = 'Select Academic Level';
        foreach($levels as $level){
            $options[$level->uuid] =  $level->academic_level;
        }
        return $options;
    }


}
<?php
namespace App\Writer\Repositories\Eloquent;

use App\Models\Order;
use App\Writer\Repositories\Contracts\OrderInterface;
use App\Writer\Repositories\Contracts\OrderSettingInterface;
use App\Writer\Repositories\Contracts\PriceInterface;

class OrderRepository extends BaseRepository implements OrderInterface {

    protected $model, $clients, $subjects, $levels, $types, $spacings, $styles, $statuses, $urgencies, $currencies, $orderSettingRepository, $priceRepository;


    function __construct(Order $model, ClientRepository $clients, SubjectRepository $subjects,
                         LevelRepository $levels, AssignmentRepository $types,
                         SpacingRepository $spacings, StyleRepository $styles,
                         StatusRepository $statuses, UrgencyRepository $urgencies,
                         CurrencyRepository $currencies, OrderSettingInterface $orderSettingRepository, PriceInterface $priceRepository)
    {
        $this->model = $model;
        $this->clients = $clients;
        $this->subjects = $subjects;
        $this->levels = $levels;
        $this->types = $types;
        $this->spacings = $spacings;
        $this->styles = $styles;
        $this->statuses = $statuses;
        $this->urgencies = $urgencies;
        $this->currencies = $currencies;
        $this->orderSettingRepository = $orderSettingRepository;
        $this->priceRepository = $priceRepository;

    }


    public function calculateAmount($level_id, $urgency_id, $pages)
    {

        if(null!=$level_id && null!=$urgency_id){
            $factor = $this->priceRepository->where('level_id', $level_id)->where('urgency_id', $urgency_id)->first();

            $amount = $pages*$factor->factor;


            /*$response = array(
                'success' => true,
                'amount' => number_format($amount,2)
            );*/
        }else{
            $amount = 0;
           /* $response = array(
                'success' => false,
                'amount' => 0
            );*/
        }

        /*if($request->ajax()){
            echo json_encode($response);
        }else{
            return number_format($amount,2);

        }*/

        return $amount;
    }

    /**
     * Modify parent method to generate order number
     * @param array $data
     * @return bool
     */
    public function create(array $data)
    {
        $orderSettings = $this->orderSettingRepository->first();
        $data['order_number'] = $orderSettings['next_order_number'];

        $level_id   = $data['academic_level_id'];
        $urgency_id = $data['urgency_id'];
        $pages      = $data['pages'];

        $data['total_cost'] = $this->calculateAmount($level_id, $urgency_id, $pages);

        //modify the data to generate order_number

        if($order = parent::create($data)){
            $this->orderSettingRepository->update(['next_order_number'=>$data['order_number']+1], $orderSettings['uuid']);
            return $order;
        }

        return false;
    }

    /**
     * Modify parent method to generate order number
     * @param array $data
     * @return bool
     */
    public function update(array $data, $uuid)
    {
       // $orderSettings = $this->orderSettingRepository->first();
       // $data['order_number'] = $orderSettings['next_order_number'];

        $level_id   = $data['academic_level_id'];
        $urgency_id = $data['urgency_id'];
        $pages      = $data['pages'];

        $data['total_cost'] = $this->calculateAmount($level_id, $urgency_id, $pages);

        //modify the data to generate order_number

       if($order = parent::update($data, $uuid)){
          // $this->orderSettingRepository->update(['next_order_number'=>$data['order_number']+1], $orderSettings['uuid']);
            return $order;
       }

        return false;
    }


    public function orders_select()
    {
        $orders = $this->sortBy('order_number', 'asc')->getAllNoPaginate();
        $options = array();
        $options[''] = 'Select Order';
        foreach($orders as $order){
            $options[$order->uuid] =  $order->order_number;
        }
        return $options;
    }

    /**
 * @return array
 */
    public function urgencies_select()
    {
        $urgencies = $this->urgencies->sortBy('duration', 'asc')->getAllNoPaginate();
        $options = array();
        $options[''] = 'Select Urgency';
        foreach($urgencies as $urgency){
            $options[$urgency->uuid] =  $urgency->urgency;
        }
        return $options;
    }

    /**
     * @return array
     */
    public function urgencies_select_form()
    {
        $urgencies = $this->urgencies->sortBy('duration', 'asc')->getAllNoPaginate();
        $options = array();
        foreach($urgencies as $urgency){
            $options[$urgency->uuid] =  $urgency->urgency;
        }
        return $options;
    }

    /**
     * @return array
     */
    public function currencies_select()
    {
        $currencies = $this->currencies->sortBy('currency_code', 'asc')->getAllNoPaginate();
        $options = array();
        $default =  "";
        $options[''] = 'Select Currency';
        foreach($currencies as $currency){
            if($currency['default'] == 'Yes'){
                $default =  $currency->uuid;
            }
            $options[$currency->uuid] =  $currency->currency_code;
        }
       // $options['default'] = $default;
       // return $options;

        $together = [$options, $default];

        return $together;
    }

    /**
     * @return array
     */
    public function clients_select()
    {
        $clients = $this->clients->sortBy('client_name', 'asc')->getAllNoPaginate();
        $options = array();
        $options[''] = 'Select Client';
        foreach($clients as $client){
            $options[$client->uuid] =  $client->client_name;
        }
        return $options;
    }

    /**
     * @return array
     */
    public function subjects_select()
    {
        $subjects = $this->subjects->sortBy('subject_area', 'asc')->getAllNoPaginate();
        $options = array();
        $options[''] = 'Select Subject';
        foreach($subjects as $subject){
            $options[$subject->uuid] =  $subject->subject_area;
        }
        return $options;
    }

    public function levels_select_form()
    {
        $levels = $this->levels->sortBy('level_value', 'asc')->getAllNoPaginate();
        $options = array();
        foreach($levels as $level){
            $options[$level->uuid] =  $level->academic_level;
        }
        return $options;
    }

    public function levels_select()
    {
        $levels = $this->levels->sortBy('level_value', 'asc')->getAllNoPaginate();
        $options = array();
        $options[''] = 'Select Academic Level';
        foreach($levels as $level){
            $options[$level->uuid] =  $level->academic_level;
        }
        return $options;
    }

    public function types_select()
    {
        $assignment_types = $this->types->sortBy('assignment_type', 'asc')->getAllNoPaginate();
        $options = array();
        $options[''] = 'Select Assignment Type';
        foreach($assignment_types as $assignment_type){
            $options[$assignment_type->uuid] =  $assignment_type->assignment_type;
        }
        return $options;
    }

    public function spacings_select()
    {
        $spacings = $this->spacings->sortBy('spacing', 'asc')->getAllNoPaginate();
        $options = array();
        $options[''] = 'Select Spacing';
        foreach($spacings as $spacing){
            $options[$spacing->uuid] =  $spacing->spacing;
        }
        return $options;
    }

    public function styles_select()
    {
        $writing_styles = $this->styles->sortBy('writing_style', 'asc')->getAllNoPaginate();
        $options = array();
        $options[''] = 'Select Writing Style';
        foreach($writing_styles as $writing_style){
            $options[$writing_style->uuid] =  $writing_style->writing_style;
        }
        return $options;
    }

    public function statuses_select()
    {
        $statuses = $this->statuses->sortBy('status', 'asc')->getAllNoPaginate();
        $options = array();
        $options[''] = 'Select Order Status';
        foreach($statuses as $status){
            $options[$status->uuid] =  $status->status;
        }
        return $options;
    }


}
<?php

namespace App\Writer\Repositories\Eloquent;

/**
 * Class BaseRepository
 * @package App\SignalInvoice\Repositories\Eloquent
 */
abstract class BaseRepository {
    /**
     * Fetch a collection
     * @param array $load
     * @return mixed
     */

    protected $orderBy  = array('created_at', 'desc');

    public function getAll($load = array())
    {
        $limit = \Request::input('limit') ?: 10;
        list($sortField, $sortDir) = $this->orderBy;
        $data = $this->model->with($load)->orderBy($sortField, $sortDir)->paginate($limit);

        $data = $this->model->with($load)->orderBy($sortField, $sortDir)->get();

        return $data;
    }

    /**
     * @param array $load
     * @return mixed
     */

    public function getAllNoPaginate($load = array()){
        list($sortField, $sortDir) = $this->orderBy;
        $data = $this->model->with($load)->orderBy($sortField, $sortDir)->get();
        return $data;
    }

    /**
     * Fetch a single item from db table.
     * Also load with relationships in load
     * @param $uuid
     * @param array $load
     * @return mixed
     */
    public function getById($uuid, $load = array())
    {
        if(!empty($load))
        {
            return $this->model->with($load)->find($uuid);
        }

        return $this->model->find($uuid);

    }

    /**
     * Fetch all deleted records since the provided time
     * @param $timestamp
     * @return mixed
     */
    public function getDeleted($timestamp){

        return $this->model->onlyTrashed()
            ->where('deleted_at', '>=', date_timestamp_set(date_create(), $timestamp))
            ->paginate();
    }

    /**
     * Fetches all records updated since last check as per provided timestamp
     * @param $timestamp
     * @return mixed
     */
    public function getUpdated($timestamp){
        return $this->model->where('updated_at', '>=', date_timestamp_set(date_create(), $timestamp))
            ->paginate();
    }

    public function where($column, $value){
        return $this->model->where($column, '=', $value);
    }

    /**
     * Create a new item
     * @param array $data
     * @return bool
     */

    public function create(array $data)
    {
        $record = $this->model->create($data);

        if(!$record){
            return false;
        }

        return $record;
    }

    /**
     * Edit an existing item.
     * @param array $data
     * @param $uuid
     * @return bool
     */
    public function update(array $data, $uuid)
    {
        $item = $this->model->find($uuid);

        $item->update($data);

        if($item){
            return $item;
        }
        return false;

    }

    /**
     * Remove a record from db
     * @param $uuid
     * @return bool
     */
    public function delete($uuid)
    {
        $record = $this->model->find($uuid);

        if(is_null($record)){
            return false;
        }

        elseif($record->destroy($uuid)){
            return true;
        }

        return false;
    }

    /**
     * @param array $load
     * @return mixed
     */

    public function first($load = array())
    {
        if(!empty($load))
        {
            return $this->model->with($load)->first();
        }

        return $this->model->first();
    }

    /**
     * Count the number of specified model records in the database
     *
     * @return int
     */
    public function count()
    {
        return $this->model->count();
    }

    /**
     * Sets how the results are sorted
     * @param string $field The field being sorted
     * @param string $direction The direction to sort (ASC or DESC)
     * @return EloquenFooRepository The current instance
     */
    public function sortBy($field, $direction = 'DESC')
    {
        $direction = (strtoupper($direction) == 'ASC') ? 'ASC' : 'DESC';
        $this->orderBy = array($field, $direction);

        return $this;
    }

} 
<?php

namespace App\Writer\Repositories\Eloquent;

use App\Models\User;
use App\Writer\Repositories\Contracts\ProfileSettingInterface;

class ProfileSettingRepository extends BaseRepository implements ProfileSettingInterface {

    protected $model;

    /**
     * @param User $model
     */
    function __construct(User $model)
    {
        $this->model = $model;
    }


}
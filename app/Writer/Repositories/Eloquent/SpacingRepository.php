<?php
namespace App\Writer\Repositories\Eloquent;

use App\Models\Spacing;
use App\Writer\Repositories\Contracts\SpacingInterface;

class SpacingRepository extends BaseRepository implements SpacingInterface {

    protected $model;

    /**
     * @param Spacing $model
     */
    function __construct(Spacing $model)
    {
        $this->model = $model;
    }


}
<?php

namespace App\Writer\Repositories\Eloquent;

use App\Models\InvoiceSetting;
use App\Writer\Repositories\Contracts\InvoiceSettingInterface;

class InvoiceSettingRepository extends BaseRepository implements InvoiceSettingInterface {

    protected $model;

    /**
     * @param InvoiceSetting $model
     */
    function __construct(InvoiceSetting $model)
    {
        $this->model = $model;
    }


}
<?php

namespace App\Writer\Repositories\Eloquent;

use App\Models\Invoice;
use App\Writer\Repositories\Contracts\InvoiceInterface;

class InvoiceRepository extends BaseRepository implements InvoiceInterface {

    protected $model;

    /**
     * @param Invoice $model
     */
    function __construct(Invoice $model)
    {
        $this->model = $model;
    }


}
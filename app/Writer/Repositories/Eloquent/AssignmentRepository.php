<?php

namespace App\Writer\Repositories\Eloquent;

use App\Models\Assignment;
use App\Writer\Repositories\Contracts\AssignmentInterface;

class AssignmentRepository extends BaseRepository implements AssignmentInterface {

    protected $model;

    /**
     * @param Assignment $model
     */
    function __construct(Assignment $model)
    {
        $this->model = $model;
    }


}
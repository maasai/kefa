<?php

namespace App\Writer\Transformers;


class ClientTransformer extends BaseTransformer {

    /**
     * @param $client
     * @return array|mixed
     */
    public function transform($client)
    {
        $allFields = [
            'client_id'         => $client['uuid'],
            'client_name'       => $client['client_name'],
            'contact_fname'     => $client['contact_first_name'],
            'contact_lname'     => $client['contact_last_name'],
            'client_email'      => $client['email'],
            'client_phone'      => $client['phone'],
            'client_address'    => $client['address'],
            'client_postal_code'=> $client['postal_code'],
            'client_city'       => $client['city'],
            'client_state'      => $client['state'],
            'client_country'    => $client['country'],
            'client_profile_pic'=> $client['profile_picture'],
            'client_other_details'=> $client['other_details'],
            'client_password'     => $client['password'],
            'client_remember_token' => $client['remember_token'],
            'deleted_at'        => $client['deleted_at'],
            'created_at'        => $client['created_at'],
            'updated_at'        => $client['updated_at'],
        ];


        return $allFields;
    }

} 
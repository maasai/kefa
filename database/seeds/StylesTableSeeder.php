<?php
namespace database\seeds;

use App\Models\Style;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StylesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('styles')->delete();

        $styles = [
            "APA",
            "MLA",
            "Turabian",
            "Chicago",
            "Harvard",
            "Oxford",
            "Vancouver",
            "CBE",
            "Other"
        ];

        foreach( $styles as $style)
        {
            Style::create([
                'writing_style'    => $style
            ]);
        }
    }

}
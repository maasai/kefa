<?php


namespace database\seeds;


use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{

    public function run()
    {

        \App\Models\User::create([
            'first_name'        => 'Kevin',
            'last_name'        => 'Mungai',
            'email'             => 'test@test.com',
            'phone_1'             => '0724475357',
            'password'          => bcrypt('test'),
            'remember_token'    => '',
        ]);

    }

}
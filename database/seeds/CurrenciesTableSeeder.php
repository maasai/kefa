<?php

namespace database\seeds;

use App\Models\Currency;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CurrenciesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('currencies')->delete();

        $currency_codes = [
            "USD",
            "GBP",
            "CAD",
            "AUD",
            "EUR"
        ];

        foreach( $currency_codes as $currency_code)
        {
            Currency::create([
                'currency_code'    => $currency_code
            ]);
        }
    }

}
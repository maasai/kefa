<?php
namespace database\seeds;

use App\Models\Level;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LevelsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('academic_levels')->delete();

        $academic_levels = [
            "High School",
            "College",
            "Undergraduate",
            "Masters",
            "Ph. D"
        ];

        foreach( $academic_levels as $academic_level)
        {
            Level::create([
                'academic_level'    => $academic_level
            ]);
        }
    }

}
<?php


namespace database\seeds;

use Illuminate\Database\Seeder;

class ClientTableSeeder extends Seeder
{

    public function run()
    {

        \App\Models\Client::create([
            'client_name'        => 'Client test',
            'email'             => 'client@client.com',
            'password'          => bcrypt('client'),

        ]);

    }

}
<?php
namespace database\seeds;

use App\Models\Spacing;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SpacingsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('paper_spacings')->delete();

        $spacings = [
            "Double Spaced",
            "Single Spaced",
            "Other"
        ];

        foreach( $spacings as $spacing)
        {
            Spacing::create([
                'spacing'    => $spacing
            ]);
        }
    }

}
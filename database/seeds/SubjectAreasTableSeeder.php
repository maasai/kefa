<?php
namespace database\seeds;

use App\Models\Subject;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubjectAreasTableSeeder extends Seeder {

    public function run()
    {
        DB::table('subject_areas')->delete();

        $subject_areas = [
                "Art",
                "Architecture",
                "Dance",
                "Design Analysis",
                "Drama",
                "Movies",
                "Music",
                "Paintings",
                "Theatre",
                "Biology",
                "Business",
                "Chemistry",
                "Communications and Media",
                "Advertising",
                "Communication Strategies",
                "Journalism",
                "Public Relations",
                "Creative writing",
                "Economics",
                "Accounting",
                "Case Study",
                "Company Analysis",
                "E-Commerce",
                "Finance",
                "Investment",
                "Logistics",
                "Education",
                "Application Essay",
                "Education Theories",
                "Pedagogy",
                "Teacher's Career",
                "Engineering",
                "English",
                "Ethics",
                "History",
                "African-American Studies",
                "American History",
                "Asian Studies",
                "Canadian Studies",
                "East European Studies",
                "Holocaust",
                "Latin-American Studies",
                "Native-American Studies",
                "West European Studies",
                "Law",
                "Criminology",
                "Legal Issues",
                "Linguistics",
                "Literature",
                "American Literature",
                "Antique Literature",
                "Asian Literature",
                "English Literature",
                "Shakespeare Studies",
                "Management",
                "Marketing",
                "Mathematics",
                "Medicine and Health",
                "Alternative Medicine",
                "Healthcare",
                "Nursing",
                "Nutrition",
                "Pharmacology",
                "Sport",
                "Nature",
                "Agricultural Studies",
                "Anthropology",
                "Astronomy",
                "Environmental Issues",
                "Geography",
                "Geology",
                "Philosophy",
                "Physics",
                "Political Science",
                "Psychology",
                "Religion and Theology",
                "Sociology",
                "Technology",
                "Aeronautics",
                "Aviation",
                "Computer Science",
                "Internet",
                "IT Management",
                "Web Design",
                "Tourism",
                "Other"
        ];

        foreach( $subject_areas as $subject_area)
        {
            Subject::create([
                'subject_area'    => $subject_area
            ]);
        }
    }

}
<?php

namespace database\seeds;

use App\Models\Assignment;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('assignment_types')->delete();

        $assignment_types = [
            "Admission/Application Essay",
            "Annotated Bibliography",
            "Article",
            "Assignment",
            "Book Report/Review",
            "Case Study",
            "Coursework",
            "Dissertation",
            "Dissertation Chapter - Abstract",
            "Dissertation Chapter - Introduction Chapter",
            "Dissertation Chapter - Literature Review",
            "Dissertation Chapter - Methodology",
            "Dissertation Chapter - Results",
            "Dissertation Chapter - Discussion",
            "Dissertation Chapter - Hypothesis",
            "Dissertation Chapter - Conclusion Chapter",
            "Editing",
            "Essay",
            "Formatting",
            "Lab Report",
            "Math Problem",
            "Movie Review",
            "Personal Statement",
            "PowerPoint Presentation plain",
            "PowerPoint Presentation with accompanying text",
            "Proofreading",
            "Paraphrasing",
            "Research Paper",
            "Research Proposal",
            "Scholarship Essay",
            "Speech/Presentation",
            "Statistics Project",
            "Term Paper",
            "Thesis",
            "Thesis Proposal"
        ];

        foreach( $assignment_types as $assignment_type)
        {
            Assignment::create([
                'assignment_type'    => $assignment_type
            ]);
        }
    }

}
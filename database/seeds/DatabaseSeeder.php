<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use database\seeds\UserTableSeeder;

class DatabaseSeeder extends Seeder
{
    protected $tables = [
        'users',
        'clients'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
    */
    public function run()
    {
        Eloquent::unguard();
        $this->cleanDatabase();

        $this->call(\database\seeds\UserTableSeeder::class);
        $this->command->info('Users table seeded!');

        $this->call(\database\seeds\ClientTableSeeder::class);
        $this->command->info('Clients table seeded!');

        $this->call(\database\seeds\CurrenciesTableSeeder::class);
        $this->command->info('Currencies table seeded!');

        $this->call(\database\seeds\LevelsTableSeeder::class);
        $this->command->info('Levels table seeded!');

        $this->call(\database\seeds\PaperUrgenciesTableSeeder::class);
        $this->command->info('Urgencies table seeded!');

        $this->call(\database\seeds\PaymentMethodsTableSeeder::class);
        $this->command->info('Payment Methods table seeded!');

        $this->call(\database\seeds\SpacingsTableSeeder::class);
        $this->command->info('Spacings table seeded!');

        $this->call(\database\seeds\StylesTableSeeder::class);
        $this->command->info('Styles table seeded!');

        $this->call(\database\seeds\SubjectAreasTableSeeder::class);
        $this->command->info('Subject Areas table seeded!');

        $this->call(\database\seeds\TypesTableSeeder::class);
        $this->command->info('Types table seeded!');

    }

    /**
     * Clean out the database for a new seed generation
     */
    private function cleanDatabase()
    {
        if (App::environment() == "testing") {
            DB::statement('PRAGMA foreign_keys = OFF;');
        } else {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
        }

        foreach ($this->tables as $table) {
            DB::table($table)->truncate();
        }

        if (APP::environment() == "testing") {
            DB::statement('PRAGMA foreign_keys = ON;');
        } else {
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        }
    }
}

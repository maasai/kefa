<?php

namespace database\seeds;

use App\Models\Urgency;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaperUrgenciesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('paper_urgencies')->delete();

        $urgencies = [
            "Less than 6 hours",
            "6 - 12 hours",
            "1 day",
            "4 to 7 days",
            "7 days  & Beyond"

        ];

        foreach( $urgencies as $urgency)
        {
            Urgency::create([
                'urgency'    => $urgency
            ]);
        }
    }

}
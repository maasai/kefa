<?php
namespace database\seeds;

use App\Models\Payment;
use Illuminate\Database\Seeder;

class PaymentMethodsTableSeeder extends Seeder {

    public function run()
    {

        Payment::create([
            'name'        => 'PAYPAL',
            'code'        => 'PAYPAL',
            'description'   => 'Paypal payment method',
        ]);

    }

}
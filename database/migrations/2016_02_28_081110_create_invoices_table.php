<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->string('uuid', 36)->primary()->unique();
            $table->string('client_id');
            $table->string('order_id');
            $table->string('currency');
            $table->string('invoice_number');
            $table->string('made_date');
            $table->string('due_date');
            $table->string('status')->nullable();
            $table->string('notes')->nullable();
            $table->string('terms')->nullable();
            $table->engine = 'InnoDB';
            $table->softDeletes();
            $table->timestamps();

            //Relationships
            $table->foreign('client_id')->references('uuid')->on('clients')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('order_id')->references('uuid')->on('orders')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoices');
    }
}

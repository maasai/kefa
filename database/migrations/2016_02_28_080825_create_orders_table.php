<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->string('uuid', 36)->primary()->unique();
            $table->string('order_number', 36);
            $table->string('client_id', 36);
            $table->string('title');
            $table->string('subject_area_id');
            $table->string('academic_level_id');
            $table->string('pages')->nullable();
            $table->string('total_cost')->nullable();
            $table->string('amount_paid')->nullable();
            $table->string('paper_type_id');
            $table->string('deadline_date');
            $table->string('sources')->nullable();
            $table->string('spacing_id');
            $table->string('urgency_id');
            $table->string('writing_style_id');
            $table->string('paper_details')->nullable();
            $table->string('client_note')->nullable();
            $table->string('status_id', 36)->nullable();

            $table->engine = 'InnoDB';
            $table->softDeletes();
            $table->timestamps();

            //Relationships
            $table->foreign('client_id')->references('uuid')->on('clients')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}

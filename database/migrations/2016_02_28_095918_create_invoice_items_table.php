<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_items', function (Blueprint $table) {
            $table->string('uuid', 36)->primary()->unique();
            $table->string('invoice_id');
            $table->string('product');
            $table->string('product_description');
            $table->string('quantity');
            $table->string('price');
            $table->string('amount');
            $table->string('note')->nullable();
            $table->engine = 'InnoDB';
            $table->softDeletes();
            $table->timestamps();

            //Relationships
            $table->foreign('invoice_id')->references('uuid')->on('invoices')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoice_items');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_files', function (Blueprint $table) {
            $table->string('uuid', 36)->primary()->unique();
            $table->string('order_id', 36);
            $table->string('file_name');
            $table->engine = 'InnoDB';
            $table->softDeletes();
            $table->timestamps();

            //Relationships
            $table->foreign('order_id')->references('uuid')->on('orders')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_files');
    }
}

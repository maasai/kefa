<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->string('uuid', 36)->primary()->unique();
            $table->string('invoice_id');
            $table->string('client_id')->nullable();
            $table->string('received_date');
            $table->string('amount');
            $table->string('payment_method_id');
            $table->string('payment_note')->nullable();
            $table->engine = 'InnoDB';
            $table->softDeletes();
            $table->timestamps();

            //Relationships
            $table->foreign('invoice_id')->references('uuid')->on('invoices')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('client_id')->references('uuid')->on('clients')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payments');
    }
}

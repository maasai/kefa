<!-- page content wrapper -->
<div class="page-content-wrap bg-img-2 light-elements">
    <!-- page content holder -->
    <div class="page-content-holder">

        <div class="block-heading block-heading-centralized this-animate" data-animate="fadeInDown">
            <h2>Professional Writing Service.</h2>
            <div class="block-heading-text">
                We give original and timely delivery for all your academic writing and editing needs.
            </div>
        </div>

        <div class="block push-up-20">
            <div class="row">
               <div class="col-md-4">

                    <div class="text-column this-animate" data-animate="fadeInLeft">

                        <h4  class="text-column-icon"><span class="fa fa-check-square-o"></span>&nbsp;&nbsp;&nbsp;&nbsp;100% Original</h4>
                        <div class="text-column-info">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas nisi sapien, imperdiet quis semper non, ornare eu justo. Vivamus dignissim eros vitae eros eleifend luctus.
                        </div>
                    </div>
                    <div class="text-column this-animate" data-animate="fadeInLeft">
                        <h4  class="text-column-icon"><span class="fa fa-pencil-square-o"></span>&nbsp;&nbsp;&nbsp;&nbsp;Unlimited Revisions</h4>
                        <div class="text-column-info">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eu blandit nisi. Duis sed vulputate lectus. Quisque dapibus finibus dui efficitur fermentum.                        </div>
                    </div>
                </div>
                <div class="col-md-8 text-center this-animate" data-animate="fadeInRight">
                    <!--   Big container   -->

                                <!--      Wizard container        -->
                                <div class="wizard-container">
                                    @if(null!=$errors->has())
                                        @foreach ($errors->all() as $error)
                                            <div class="alert alert-warning">{{ $error }}</div>
                                        @endforeach
                                    @endif
                                    <div class="card wizard-card ct-wizard-orange" id="wizardProfile">

                                            {!! Form::open(['route' => ['form'], 'class'=>'order_form']) !!}

                     <!--        You can switch "ct-wizard-orange"  with one of the next bright colors: "ct-wizard-blue", "ct-wizard-green", "ct-wizard-orange", "ct-wizard-red"             -->

                                            <ul>
                                                <li><a href="#about" data-toggle="tab">Order</a></li> <!-- About-->
                                                <li><a href="#account" data-toggle="tab">More Details</a></li> <!-- Account-->
                                                <li><a href="#address" data-toggle="tab">Client</a></li> <!-- Address-->
                                            </ul>

                                            <div class="tab-content">
                                                <div class="tab-pane" id="about">
                                                    <div class="row">
                                <div class="form-group col-xs-12">
                                     <div class="col-xs-12">
                                        <div class="form-group">
                                            {!! Form::label('title','Work Title', ['class'=>'control-label pull-left']) !!}
                                                {!! Form::text('title',null,['class' => 'form-control input-sm', 'required']) !!}
                                        </div>
                                    </div>
                                </div>
                                                    </div> <!-- End Title -->
                                                    <!-- 2nd row -->
                                                    <div class="row">
                                                    <div class="form-group col-xs-12">
                                                        <div class="col-xs-4">
                                                                <div class="form-group">
                                                                    {!! Form::label('paper_type_id','Type of Work', ['class'=>'control-label pull-left']) !!}
                                                                        {!! Form::select('paper_type_id',$types, null, ['class' => 'form-control select input-md',  'data-live-search'=>"true"]) !!}
                                                                </div>
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <div class="form-group">
                                                                {!! Form::label('academic_level_id','Academic Level', ['class'=>'control-label pull-left']) !!}
                                                                {!! Form::select('academic_level_id',$levels, null, ['class' => 'form-control select input-md', 'required',  'data-live-search'=>"true", 'id' => 'academic_level_id', 'onchange' => 'calculate_amount()']) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <div class="form-group">
                                                                {!! Form::label('writing_style_id','Writing Style', ['class'=>'control-label pull-left']) !!}
                                                                {!! Form::select('writing_style_id',$styles, null, ['class' => 'form-control select input-md',  'data-live-search'=>"true"]) !!}
                                                            </div>

                                                        </div>

                                                    </div>
                                                    </div><!-- End Row -->
                                                    <!-- 3rd row -->
                                                    <div class="row">
                                                        <div class="form-group col-xs-12">
                                                            <div class="col-xs-4">
                                                                <div class="form-group">
                                                                    {!! Form::label('subject_area_id','Subject Area', ['class'=>'control-label pull-left']) !!}
                                                                    {!! Form::select('subject_area_id',$subjects, null, ['class' => 'form-control select input-md',  'data-live-search'=>"true"]) !!}
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <div class="form-group">
                                                                    {!! Form::label('urgency_id','Urgency', ['class'=>'control-label pull-left']) !!}
                                                                    {!! Form::select('urgency_id',$urgencies, null, ['class' => 'form-control select input-md', 'required',  'data-live-search'=>"true", 'id' => 'urgency_id', 'onchange' => 'calculate_amount()']) !!}
                                                                </div>

                                                            </div>
                                                            <div class="col-xs-4">
                                                                <div class="form-group">
                                                                    {!! Form::label('spacing_id','Line Spacing', ['class'=>'control-label pull-left']) !!}
                                                                    {!! Form::select('spacing_id',$spacings, null, ['class' => 'form-control select input-md',  'data-live-search'=>"true"]) !!}
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div><!-- End Row -->
                                                </div>


                                                <div class="tab-pane" id="account">
                                                    <!-- 1st row -->
                                                    <div class="row">
                                                        <div class="form-group col-xs-12">
                                                            <div class="col-xs-4">
                                                                <div class="form-group">
                                                                    {!! Form::label('currency','Currency', ['class'=>'control-label pull-left']) !!}
                                                                    {!! Form::select('currency',$currencies['0'], $currencies['1'], ['class' => 'form-control select input-md',  'data-live-search'=>"true", 'id'=>"currency_id"]) !!}
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <div class="form-group">
                                                                    {!! Form::label('pages','Number Of Pages', ['class'=>'control-label pull-left']) !!}
                                                                    {!! Form::number('pages',1,['class' => 'form-control input-sm', 'min'=>"1", 'id'=>'pages', 'onchange' => 'calculate_amount()']) !!}
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <div class="form-group">
                                                                    {!! Form::label('sources','Reference Sources', ['class'=>'control-label pull-left']) !!}
                                                                    {!! Form::number('sources',null,['class' => 'form-control input-sm', 'min'=>"0"]) !!}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><!-- End Row -->
                                                    <!-- DeadLine-->
                                                    <div class="row">
                                                        <div class="form-group col-xs-12">
                                                            <div class="col-xs-12">
                                                                <div class="form-group">
                                                                    {!! Form::label('deadline_date','DeadLine Date', ['class'=>'control-label pull-left']) !!}
                                                                    {!! Form::text('deadline_date',null,['class' => 'form-control datepicker input-sm']) !!}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> <!-- End DeadLine -->
                                                </div>




                                                <div class="tab-pane" id="address">
                                                    <div class="row">
                                                        <!-- Email -->
                                                        <div class="row">
                                                            <div class="form-group col-xs-12">
                                                                <div class="col-xs-12">
                                                                    <div class="form-group">
                                                                        {!! Form::label('email','Email', ['class'=>'control-label pull-left']) !!}
                                                                        {!! Form::text('email',null,['class' => 'form-control input-sm', 'required', 'placeholder'=>'Client Email Address']) !!}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div> <!-- End Email -->
                                                        <!-- Comment -->
                                                        <div class="row">
                                                            <div class="form-group col-xs-12">
                                                                <div class="col-xs-12">
                                                                    You will be redirected to our secure Client Area. Where you Register/Login and complete this order.
                                                                </div>
                                                            </div>
                                                        </div> <!-- End Comment -->
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="wizard-footer">
                                                <span style="font-size:24px; color: #111111;">
                                                    <span id="currency_symbol">$</span>
                                                    <span id="total_amount">0</span>
                                                </span>
                                                        <!--<label  class="control-label info-text"><h3><strong>$125 </strong></h3><small>(Total Cost)</small></label>-->
                                                <div class="pull-right">
                                                    <a href="#" class="btn btn-next btn-fill btn-primary btn-wd btn-md"> Next <span class="glyphicon glyphicon-chevron-right"></span></a>
                                                    <!--<input type='button' class='btn btn-next btn-fill btn-primary btn-wd btn-md' name='next' value='Next &raquo;' />-->
                                                    <!--<input type='button' class='btn btn-finish btn-fill btn-success btn-wd btn-md' name='finish' value='Finish' />-->
                                                    <button class="btn btn-finish btn-fill btn-success btn-wd btn-md"><i class="fa fa-save"></i> {{ 'Finish' }}</button>
                                                </div>

                                                <div class="pull-left">
                                                    <!--<input type='button' class='btn btn-previous btn-fill btn-primary btn-wd btn-md' name='previous' value='Previous' />-->
                                                    <a href="#" class="btn btn-previous btn-fill btn-primary btn-wd btn-md"><div class="glyphicon glyphicon-chevron-left"> Previous </div></a>

                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div> <!-- wizard container -->



                </div>
            </div>
        </div>

    </div>
    <!-- ./page content holder -->
</div>
<!-- ./page content wrapper -->

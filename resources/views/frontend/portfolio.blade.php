@extends('frontend.layouts.main')

@section('content')
        <!-- page content -->
<div class="page-content">

    <!-- page content wrapper -->
    <div class="page-content-wrap bg-light">
        <!-- page content holder -->
        <div class="page-content-holder no-padding">
            <!-- page title -->
            <div class="page-title">
                <h1>Portfolio 2 Column</h1>
                <!-- breadcrumbs -->
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Portfolio</a></li>
                    <li class="active">Portfolio With Title</li>
                </ul>
                <!-- ./breadcrumbs -->
            </div>
            <!-- ./page title -->
        </div>
        <!-- ./page content holder -->
    </div>
    <!-- ./page content wrapper -->


    <!-- page content wrapper -->
    <div class="page-content-wrap">
        <!-- page content holder -->
        <div class="page-content-holder">

            <div class="block-heading this-animate" data-animate="fadeInDown">
                <h2>This is an example of portfolio with title</h2>
                <div class="block-heading-text">
                    Donec sit amet elementum libero. Curabitur ut lorem id tellus malesuada tincidunt et eget purus. Cras molestie, velit quis viverra ultrices, tortor erat suscipit arcu, a ullamcorper neque lorem et massa. Praesent facilisis tellus nec rutrum luctus. Curabitur non venenatis metus, vitae rhoncus risus.
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 gallery-filter">
                    <div class="button-panel">
                        <button data-filter="all" class="btn btn-primary filter">All</button>
                        <button data-filter=".cat_music" class="btn btn-primary filter">Music</button>
                        <button data-filter=".cat_nature" class="btn btn-primary filter">Nature</button>
                        <button data-filter=".cat_space" class="btn btn-primary filter">Space</button>
                    </div>
                </div>
            </div>

            <div class="row mix-grid thumbnails">
                <div class="col-md-4 col-xs-4 mix cat_nature cat_all">
                    <a class="thumbnail-item">
                        <img src="{{ asset('assets/assets/img/gallery/nature-1.jpg') }}"  alt="Nature 1"/>
                        <div class="thumbnail-info">
                            <p>Curabitur ut lorem id tellus malesuada tincidunt et eget purus</p>
                            <button class="btn btn-primary"><span class="fa fa-link"></span></button>
                            <button class="btn btn-primary"><span class="fa fa-eye"></span></button>
                        </div>
                    </a>
                    <div class="thumbnail-data">
                        <h5>Lorem ipsum dolor</h5>
                        <p>Curabitur ut lorem id telius</p>
                    </div>
                </div>
                <div class="col-md-4 col-xs-4 mix cat_space cat_all">
                    <a class="thumbnail-item">
                        <img src="{{ asset('assets/assets/img/gallery/space-1.jpg') }}" alt="Space 1"/>
                        <div class="thumbnail-info">
                            <p>Curabitur ut lorem id tellus malesuada tincidunt et eget purus</p>
                            <button class="btn btn-primary"><span class="fa fa-link"></span></button>
                            <button class="btn btn-primary"><span class="fa fa-eye"></span></button>
                        </div>
                    </a>
                    <div class="thumbnail-data">
                        <h5>Lorem ipsum dolor</h5>
                        <p>Curabitur ut lorem id telius</p>
                    </div>
                </div>
                <div class="col-md-4 col-xs-4 mix cat_nature cat_all">
                    <a class="thumbnail-item">
                        <img src="{{ asset('assets/assets/img/gallery/nature-2.jpg')}}" alt="Nature 2"/>
                        <div class="thumbnail-info">
                            <p>Curabitur ut lorem id tellus malesuada tincidunt et eget purus</p>
                            <button class="btn btn-primary"><span class="fa fa-link"></span></button>
                            <button class="btn btn-primary"><span class="fa fa-eye"></span></button>
                        </div>
                    </a>
                    <div class="thumbnail-data">
                        <h5>Lorem ipsum dolor</h5>
                        <p>Curabitur ut lorem id telius</p>
                    </div>
                </div>
                <div class="col-md-4 col-xs-4 mix cat_music cat_all">
                    <a class="thumbnail-item">
                        <img src="{{ asset('assets/assets/img/gallery/music-2.jpg')}}" alt="Music 2"/>
                        <div class="thumbnail-info">
                            <p>Curabitur ut lorem id tellus malesuada tincidunt et eget purus</p>
                            <button class="btn btn-primary"><span class="fa fa-link"></span></button>
                            <button class="btn btn-primary"><span class="fa fa-eye"></span></button>
                        </div>
                    </a>
                    <div class="thumbnail-data">
                        <h5>Lorem ipsum dolor</h5>
                        <p>Curabitur ut lorem id telius</p>
                    </div>
                </div>
                <div class="col-md-4 col-xs-4 mix cat_nature cat_all">
                    <a class="thumbnail-item">
                        <img src="{{ asset('assets/assets/img/gallery/nature-3.jpg')}}" alt="Nature 3"/>
                        <div class="thumbnail-info">
                            <p>Curabitur ut lorem id tellus malesuada tincidunt et eget purus</p>
                            <button class="btn btn-primary"><span class="fa fa-link"></span></button>
                            <button class="btn btn-primary"><span class="fa fa-eye"></span></button>
                        </div>
                    </a>
                    <div class="thumbnail-data">
                        <h5>Lorem ipsum dolor</h5>
                        <p>Curabitur ut lorem id telius</p>
                    </div>
                </div>
                <div class="col-md-4 col-xs-4 mix cat_nature cat_all">
                    <a class="thumbnail-item">
                        <img src="{{ asset('assets/assets/img/gallery/nature-4.jpg')}}" alt="Nature 4"/>
                        <div class="thumbnail-info">
                            <p>Curabitur ut lorem id tellus malesuada tincidunt et eget purus</p>
                            <button class="btn btn-primary"><span class="fa fa-link"></span></button>
                            <button class="btn btn-primary"><span class="fa fa-eye"></span></button>
                        </div>
                    </a>
                    <div class="thumbnail-data">
                        <h5>Lorem ipsum dolor</h5>
                        <p>Curabitur ut lorem id telius</p>
                    </div>
                </div>
                <div class="col-md-4 col-xs-4 mix cat_space cat_all">
                    <a class="thumbnail-item">
                        <img src="{{ asset('assets/assets/img/gallery/space-2.jpg')}}" alt="Space 2"/>
                        <div class="thumbnail-info">
                            <p>Curabitur ut lorem id tellus malesuada tincidunt et eget purus</p>
                            <button class="btn btn-primary"><span class="fa fa-link"></span></button>
                            <button class="btn btn-primary"><span class="fa fa-eye"></span></button>
                        </div>
                    </a>
                    <div class="thumbnail-data">
                        <h5>Lorem ipsum dolor</h5>
                        <p>Curabitur ut lorem id telius</p>
                    </div>
                </div>
                <div class="col-md-4 col-xs-4 mix cat_nature cat_all">
                    <a class="thumbnail-item">
                        <img src="{{ asset('assets/assets/img/gallery/nature-5.jpg')}}" alt="Nature 5"/>
                        <div class="thumbnail-info">
                            <p>Curabitur ut lorem id tellus malesuada tincidunt et eget purus</p>
                            <button class="btn btn-primary"><span class="fa fa-link"></span></button>
                            <button class="btn btn-primary"><span class="fa fa-eye"></span></button>
                        </div>
                    </a>
                    <div class="thumbnail-data">
                        <h5>Lorem ipsum dolor</h5>
                        <p>Curabitur ut lorem id telius</p>
                    </div>
                </div>
                <div class="col-md-4 col-xs-4 mix cat_music cat_all">
                    <a class="thumbnail-item">
                        <img src="{{ asset('assets/assets/img/gallery/music-1.jpg')}}" alt="Music 1"/>
                        <div class="thumbnail-info">
                            <p>Curabitur ut lorem id tellus malesuada tincidunt et eget purus</p>
                            <button class="btn btn-primary"><span class="fa fa-link"></span></button>
                            <button class="btn btn-primary"><span class="fa fa-eye"></span></button>
                        </div>
                    </a>
                    <div class="thumbnail-data">
                        <h5>Lorem ipsum dolor</h5>
                        <p>Curabitur ut lorem id telius</p>
                    </div>
                </div>
            </div>

            <ul class="pagination pagination-sm pull-right">
                <li class="disabled"><a href="#">«</a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">»</a></li>
            </ul>

        </div>
        <!-- ./page content holder -->
    </div>
    <!-- ./page content wrapper -->

</div>
<!-- ./page content -->
@endsection
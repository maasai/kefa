@extends('frontend.layouts.main')

@section('content')
        <!-- page content -->
<div class="page-content">

@include('frontend.form.order')



    <!-- page content wrapper -->
    <div class="page-content-wrap bg-img-1">

        <div class="divider"><div class="box"><span class="fa fa-angle-down"></span></div></div>

        <!-- page content holder -->
        <div class="page-content-holder">

            <div class="row">
                <div class="col-md-8 this-animate" data-animate="fadeInLeft">

                    <div class="block-heading block-heading-centralized">
                        <h2 class="heading-underline">High quality and professional support 24/7.</h2>
                        <div class="block-heading-text">
                            Proin luctus nulla fringilla massa euismod commodo. Donec sit amet elementum libero. Curabitur ut lorem id tellus malesuada tincidunt et eget purus. Cras molestie, velit quis viverra ultrices, tortor erat suscipit arcu, a ullamcorper neque lorem et massa. Praesent facilisis tellus nec rutrum luctus. Curabitur non venenatis metus, vitae rhoncus risus. Donec quis mattis est. Proin ut augue vel odio condimentum ornare.
                        </div>
                    </div>
                    <div class="block this-animate" data-animate="fadeInLeft">
                        <img src="{{ asset('assets/assets/atlant_technologies.png') }}" class="img-responsive"/>
                    </div>
                </div>
                <div class="col-md-4 this-animate text-center" data-animate="fadeInRight">
                    <img src="{{ asset('assets/assets/atlant_responsive.png') }}" class="img-responsive-mobile"/>
                </div>
            </div>



        </div>
        <!-- ./page content holder -->
    </div>
    <!-- ./page content wrapper -->

    <!-- page content wrapper -->
    <div class="page-content-wrap bg-texture-1 bg-dark light-elements">

        <div class="divider"><div class="box"><span class="fa fa-angle-down"></span></div></div>

        <!-- page content holder -->
        <div class="page-content-holder">

            <div class="row">

                <div class="col-md-4">
                    <div class="text-column text-column-centralized tex-column-icon-lg this-animate" data-animate="fadeInLeft">
                        <div class="text-column-icon">
                            <span class="fa fa-support"></span>
                        </div>
                        <h4>Free Updates & Support</h4>
                        <div class="text-column-info">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="text-column text-column-centralized tex-column-icon-lg this-animate" data-animate="fadeInUp">
                        <div class="text-column-icon">
                            <span class="fa fa-expand"></span>
                        </div>
                        <h4>Responsive & Retina Ready</h4>
                        <div class="text-column-info">
                            Website built using newest technologies, that gives you possibilities use it wherever you want.
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="text-column text-column-centralized tex-column-icon-lg this-animate" data-animate="fadeInRight">
                        <div class="text-column-icon">
                            <span class="fa fa-clock-o"></span>
                        </div>
                        <h4>Time Saver</h4>
                        <div class="text-column-info">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit <strong>yes bold</strong>.
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <!-- ./page content holder -->
    </div>
    <!-- ./page content wrapper -->

    <!-- page content wrapper -->
    <div class="page-content-wrap bg-light bg-texture-1">

        <div class="divider"><div class="box"><span class="fa fa-angle-down"></span></div></div>

        <!-- page content holder -->
        <div class="page-content-holder">

            <div class="quote this-animate" data-animate="fadeInDown">
                <div class="row">
                    <div class="col-md-9">
                        <h3><strong>ATLANT</strong> &mdash; Responsive Bootstrap Admin Template</h3>
                    </div>
                    <div class="col-md-3">
                        <button class="btn btn-primary btn-block btn-lg">Preview Admin</button>
                    </div>
                </div>
            </div>

        </div>
        <!-- ./page content holder -->
    </div>
    <!-- ./page content wrapper -->

    <!-- page content wrapper -->
    <div class="page-content-wrap bg-light">
        <!-- page content holder -->
        <div class="page-content-holder padding-v-20">

            <div class="text-center">
                <a href="../html/index.html" class="btn btn-primary btn-xl"><span class="fa fa-eye"></span> Preview</a>
                <a href="http://themeforest.net/item/atlant-responsive-bootstrap-admin-template/9217590" class="btn btn-success btn-xl"><span class="fa fa-shopping-cart"></span> Purchase</a>
            </div>

        </div>
        <!-- ./page content holder -->
    </div>
    <!-- ./page content wrapper -->
</div>
<!-- ./page content -->
@endsection
<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.title')


    <!-- Order Form -->
    <link type="text/css" href="{{ asset('assets/assets/form/css/bootstrap.min.css')}}" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/styles.css') }}" media="screen" />


    <link type="text/css" href="{{ asset('assets/assets/form/css/gsdk-base.css')}}" rel="stylesheet" />
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">

</head>
<body>
<!-- page container -->
<div class="page-container">

   @include('frontend.common.header')

    @yield('content')

    @include('frontend.common.footer')


</div>
<!-- ./page container -->

<!-- page scripts -->
<script type="text/javascript" src="{{ asset('assets/js/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/bootstrap/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/mixitup/jquery.mixitup.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/appear/jquery.appear.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/actions.js') }}"></script>
<!-- ./page scripts -->


<script type="text/javascript" src="{{ asset('assets/admin/js/plugins/bootstrap/bootstrap-datepicker.js') }}"></script>

<!-- ./Form scripts -->

<!--   plugins 	 -->
<script src="{{ asset('assets/assets/form/js/jquery.bootstrap.wizard.js')}}" type="text/javascript"></script>

<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
<script src="{{ asset('assets/assets/form/js/jquery.validate.min.js')}}"></script>

<!--  methods for manipulating the wizard and the validation -->
<script src="{{ asset('assets/assets/form/js/wizard.js')}}"></script>

<script type="text/javascript" src="{{ asset('assets/js/custom.js') }}"></script>

</body>

</html>







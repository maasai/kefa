<!-- page header -->
<div class="page-header">

    <!-- page header holder -->
    <div class="page-header-holder">

        <!-- page logo -->
        <div class="logo">
            <a href="#">Kefa - Ultimate Contract Writer</a>
        </div>
        <!-- ./page logo -->


        <!-- search
        <div class="search">
            <div class="search-button"><span class="fa fa-search"></span></div>
            <div class="search-container animated fadeInDown">
                <form action="#" method="post">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search..."/>
                        <div class="input-group-btn">
                            <button class="btn btn-primary"><span class="fa fa-search"></span></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- ./search -->

        <!-- nav mobile bars -->
        <div class="navigation-toggle">
            <div class="navigation-toggle-button"><span class="fa fa-bars"></span></div>
        </div>
        <!-- ./nav mobile bars -->

        <!-- navigation -->
        <ul class="navigation">
            <li>
                <a href="{{ Url('/') }}">Home</a>
            </li>
            <li>
                <a href="{{ Url('page') }}">Essay</a>
            </li>
            <li>
                <a href="{{ Url('page') }}">Proofreading</a>
            </li>
            <li>
                <a href="{{ Url('page') }}">Dissertation</a>
            </li>

            <li>
                <a href="#">Styles</a>
                <ul>
                    <li><a href="{{ Url('page') }}">APA Style</a></li>
                    <li><a href="{{ Url('page') }}">Harvard</a></li>
                    <li><a href="{{ Url('page') }}">Other</a></li>
                </ul>
            </li>
            <li>
                <a href="{{ Url('portfolio') }}">Samples</a>
            </li>
            <li>
                <a href="#">Information</a>
                <ul>
                    <li><a href="{{ Url('pricing') }}">Pricing</a></li>
                    <li><a href="{{ Url('pricing') }}">Revision Policy</a></li>
                    <li><a href="{{ Url('pricing') }}">Our Writers</a></li>
                    <li><a href="{{ Url('pricing') }}">Terms and Conditions</a></li>
                    <li><a href="{{ Url('about') }}">About Us</a></li>
                    <li><a href="{{ Url('contact') }}">Contact Us</a></li>

                </ul>
            </li>
            <li>

                <a href="#">
                    <div>
                        <button type="submit" class='btn orange btn-wd btn-md '><span class="fa fa-lock"></span> MY ACCOUNT</button>
                    </div>
                </a>
                <ul>
                    <li><a href="{{ Url('admin') }}">Admin</a></li>
                    <li><a href="{{ Url('client') }}">Client</a></li> <!--client-->

                </ul>

            </li>

        </ul>
        <!-- ./navigation -->


    </div>
    <!-- ./page header holder -->

</div>
<!-- ./page header -->
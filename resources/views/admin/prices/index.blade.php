@extends('admin.layouts.main')

@section('content')
        <!-- PAGE CONTENT -->
<div class="page-content">
    <?php \DB::listen(function($sql) {
        $query = \DB::getQueryLog();
       // $lastQuery = end($query);
      //  dd($query);
    }); ?>

    @include('admin.common.navigation_horizontal')
    @include('admin.common.system_data_breadcrumb')
            <!-- START CONTENT FRAME -->
    <div class="content-frame">

        <!-- START CONTENT FRAME TOP -->
        <div class="content-frame-top">

            <div class="pull-right">
                <button class="btn btn-default content-frame-left-toggle"><span class="fa fa-bars"></span></button>
            </div>
        </div>
        <!-- END CONTENT FRAME TOP -->

        <!-- START CONTENT FRAME LEFT -->
        <div class="content-frame-left">
            <div class="panel panel-default">

                @include('admin.common.system_navigation')

            </div>
        </div>
        <!-- END CONTENT FRAME LEFT -->

        <!-- START CONTENT FRAME BODY -->
        <div class="content-frame-body">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="page-title">
                        <h2><span class="fa fa-bars"></span> Price Per Page </h2>
                    </div>

                <!-- START DATATABLE EXPORT -->
                <div class="panel panel-default">

                    <div class="panel-body">
                        @if (session()->has('success'))
                            <div class="alert alert-success" id="success-alert">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                                {{ session('success') }}
                            </div>
                        @endif
                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif

                        <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>
                                        <div>Academic Level</div>
                                        <div>Urgency</div>
                                    </th>
                                    @foreach($academic_levels as $level)

                                        <th width="200"  style="text-align:center">{{$level->academic_level}}</th>

                                    @endforeach


                                </tr>
                                </thead>
                                <tbody>


                                @foreach($urgencies as $urgency)
                                    <tr>
                                        <td width="200">{{$urgency->urgency}}</td>
                                        @foreach($academic_levels as $all_levels)

                                            <?php
                                            $levelId = $all_levels->uuid;
                                            $urgencyId = $urgency->uuid;
                                                $dataFactors = false;
                                            $factor = "";
                                                ?>
                                                @foreach($prices as $price)
                                                    @if($levelId == $price->level_id && $urgencyId == $price->urgency_id)
                                                        <?php $dataFactors = true; $factor = $price->factor; ?>
                                                    @endif
                                                @endforeach

                                            @if($dataFactors)
                                                    <td width="200" align="center">{{$factor}}</td>
                                                @else
                                                    <td width="200" align="center"> - </td>

                                                @endif

                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>



                        <div class="panel-heading">
                            <h3>Edit Price</h3>
                            {!! Form::open(['route' => ['admin.system.prices.store'],'class'=>'ajax-submit']) !!}

                            <div class="row">
                                <div class="form-group col-xs-12">
                                    <div class="col-sm-4 invoice-col">
                                        <div class="form-group">
                                            {!! Form::label('urgency_id','Paper Urgency') !!}
                                            <div class="input-group">
                                                <div class="input-group-addon"><div class="fa fa-clock-o"></div></div>
                                                {!! Form::select('urgency_id',$urgencies_select, null, ['class' => 'form-control select input-md', 'required']) !!}
                                            </div>
                                        </div>
                                    </div><!-- /.col -->


                                    <div class="col-sm-4 invoice-col">
                                        <div class="form-group">
                                            {!! Form::label('level_id','Academic Level') !!}
                                            <div class="input-group">
                                                <div class="input-group-addon"><div class="fa fa-graduation-cap"></div></div>
                                                {!! Form::select('level_id',$levels_select, null, ['class' => 'form-control select input-md',  'required']) !!}
                                            </div>
                                        </div>
                                    </div><!-- /.col -->


                                    <div class="col-sm-4 invoice-col">
                                        <div class="form-group">
                                            {!! Form::label('factor','Price') !!}
                                            <div class="input-group">
                                                <div class="input-group-addon"><div class="fa fa-sort-numeric-asc"></div></div>
                                                {!! Form::number('factor',null,['class' => 'form-control input-sm', 'min'=>"0", "required"]) !!}
                                            </div>
                                        </div>
                                    </div><!-- /.col -->

                                    <div class="form-group">
                                        <button class="btn btn-finish btn-fill btn-success btn-wd btn-md pull-right"><i class="fa fa-save"></i> {{ 'Save' }}</button>
                                    </div>

                                </div>
                            </div>
                            {!! Form::close() !!}

                        </div>

                    </div>
                </div>
                <!-- END DATATABLE EXPORT -->
            </div>
        </div>


</div>

    </div>
    <!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTENT -->
@endsection
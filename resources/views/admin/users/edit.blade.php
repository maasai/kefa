@extends('layouts.modal')

@section('content')
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5>Edit User</h5>
            </div>
            {!! Form::model($user, ['route' => ['admin.users.update', $user->uuid],'method'=>'PATCH', 'class'=>'ajax-submit']) !!}

            <div class="modal-body">

                <div class="form-group">
                    {!! Form::label('first_name','First Name') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                        {!! Form::text('first_name',null,['class' => 'form-control input-sm','required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('last_name','Last Name') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                        {!! Form::text('last_name',null,['class' => 'form-control input-sm']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('email','Email') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-envelope-o"></div></div>
                        {!! Form::text('email',null,['class' => 'form-control input-sm','required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('phone_1','Phone#1') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-mobile-phone"></div></div>
                        {!! Form::text('phone_1',null,['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('phone_2','Phone#2') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-mobile-phone"></div></div>
                        {!! Form::text('phone_2',null,['class' => 'form-control input-sm']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('country','Country') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                        {!! Form::text('country',null,['class' => 'form-control input-sm']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('status','Client Status') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                        {!! Form::select('status', array('Active' => 'Active', 'Disabled' => 'Disabled'), null, ['class'=>'form-control required', 'required'])!!}

                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('password','Password') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-unlock-alt"></div></div>
                        {!! Form::password('password',['class' => 'form-control input-sm']) !!}

                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('password_confirmation','Confirm Password') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-unlock-alt"></div></div>
                        {!! Form::password('password_confirmation',['class' => 'form-control input-sm']) !!}

                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Update</button>
                <button type="button" data-dismiss="modal" class="btn btn-danger">Close</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
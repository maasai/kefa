@extends('layouts.modal')

@section('content')
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5>Add User</h5>
            </div>
            {!! Form::open(['route' => ['admin.users.store'], 'class' => 'ajax-submit']) !!}
            <div class="modal-body">

                <div class="form-group">
                    {!! Form::label('first_name','First Name') !!}
                        <div class="input-group">
                            <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                            {!! Form::text('first_name',null,['class' => 'form-control input-sm','required', 'placeholder'=>'Enter First Name']) !!}
                        </div>
                </div>

                <div class="form-group">
                    {!! Form::label('last_name','Last Name') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                        {!! Form::text('last_name',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter Last Name']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('email','Email') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-envelope-o"></div></div>
                        {!! Form::text('email',null,['class' => 'form-control input-sm','required', 'placeholder'=>'Enter Email']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('phone_1','Phone#1') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-mobile-phone"></div></div>
                        {!! Form::text('phone_1',null,['class' => 'form-control', 'placeholder'=>'Enter First Phone Number']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('phone_2','Phone#2') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-mobile-phone"></div></div>
                        {!! Form::text('phone_2',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter Second Phone Number']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('country','Country') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                        {!! Form::text('country',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter Country Name']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('password','Password') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-unlock-alt"></div></div>
                        {!! Form::password('password',['class' => 'form-control input-sm']) !!}

                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('password_confirmation','Confirm Password') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-unlock-alt"></div></div>
                        {!! Form::password('password_confirmation',['class' => 'form-control input-sm']) !!}

                    </div>
                </div>

                </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>Save</button>
                <button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-times"></i>Close</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
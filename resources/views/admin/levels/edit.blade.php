@extends('layouts.modal')

@section('content')
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5>Edit Academic Level Style</h5>
            </div>
            {!! Form::model($level, ['route' => ['admin.system.levels.update', $level->uuid],'method'=>'PATCH', 'class'=>'ajax-submit']) !!}

            <div class="modal-body">

                <div class="form-group">
                    {!! Form::label('academic_level','Academic Level') !!}
                    {!! Form::text('academic_level', null, ['class'=>'form-control required', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('level_value','Level Value') !!}
                    {!! Form::text('level_value', null, ['class'=>'form-control required']) !!}
                </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Update</button>
                <button type="button" data-dismiss="modal" class="btn btn-danger">Close</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
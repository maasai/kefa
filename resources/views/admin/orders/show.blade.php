@extends('admin.layouts.main')

@section('content')
        <!-- PAGE CONTENT -->
<div class="page-content">

    @include('admin.common.navigation_horizontal')

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-xs-12">


                <div class="box box-primary">

                    <div class="panel panel-body">
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead><tr><td colspan="3"> Summary</td></tr></thead>
                                <tbody>
                                <tr id="trow_1">
                                    <td class="text-left" width="250">Order Number : </td>
                                    <td><strong>{{$order->order_number}}</strong></td>
                                    <td class="text-right" width="250"> Amount : </td>
                                    <td><strong>$500</strong></td>
                                </tr>
                                <tr id="trow_1">
                                    <td class="text-left"> Title  : </td>
                                    <td><strong>{{$order->title}}</strong></td>
                                </tr>
                                <tr id="trow_1">
                                    <td class="text-left" width="100"> Client : </td>
                                    <td><strong>{{$order->client->client_name}}</strong></td>
                                </tr>

                                </tbody>
                                </table>
                        </div>

                    </div>

                            <div class="col-md-12">
                                <!-- START JUSTIFIED TABS -->
                                <div class="panel panel-default tabs">
                                    <ul class="nav nav-tabs nav-justified">
                                        <li class="active"><a href="#tab1" data-toggle="tab"><strong> DETAILS</strong></a></li>
                                        <li><a href="#tab2" data-toggle="tab"><strong>FILES (Attachments)</strong></a></li>
                                        <li><a href="#tab3" data-toggle="tab"><strong>EXTRA NOTES</strong></a></li>
                                    </ul>
                                    <div class="panel-body tab-content">

                                        <div class="tab-pane active" id="tab1">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped table-actions borderless">
                                                    <thead>

                                                    <tr>
                                                        <th width="200"  class="text-right"></th>
                                                        <th></th>
                                                        <th width="100"></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr id="trow_1">
                                                        <td class="text-right">Order Number</td>
                                                        <td><strong>{{$order->order_number}}</strong></td>
                                                    </tr>

                                                    <tr id="trow_1">
                                                        <td class="text-right"> Title</td>
                                                        <td><strong>{{$order->title}}</strong></td>
                                                    </tr>

                                                    <tr id="trow_1">
                                                        <td class="text-right">Client</td>
                                                        <td><strong>{{$order->client->client_name}}</strong></td>
                                                    </tr>

                                                    <tr id="trow_1">
                                                        <td class="text-right">Subject Area</td>
                                                        <td><strong>{{$order->subject_area_id}}</strong></td>
                                                    </tr>

                                                    <tr id="trow_1">
                                                        <td class="text-right">Academic Level</td>
                                                        <td><strong>{{$order->academic_level_id}}</strong></td>
                                                    </tr>

                                                    <tr id="trow_1">
                                                        <td class="text-right"># Of Pages</td>
                                                        <td><strong>{{$order->pages}}</strong></td>
                                                    </tr>

                                                    <tr id="trow_1">
                                                        <td class="text-right">Amount Paid</td>
                                                        <td><strong>{{$order->amount_paid}}</strong></td>
                                                    </tr>


                                                    <tr id="trow_1">
                                                        <td class="text-right">Paper Type</td>
                                                        <td><strong>{{$order->paper_type_id}}</strong></td>
                                                    </tr>

                                                    <tr id="trow_1">
                                                        <td class="text-right">Sources</td>
                                                        <td><strong>{{$order->sources}}</strong></td>
                                                    </tr>


                                                    <tr id="trow_1">
                                                        <td class="text-right">Deadline</td>
                                                        <td><strong>{{$order->deadline_date}}</strong></td>
                                                    </tr>

                                                    <tr id="trow_1">
                                                        <td class="text-right">Spacing</td>
                                                        <td><strong>{{$order->spacing_id}}</strong></td>
                                                    </tr>

                                                    <tr id="trow_1">
                                                        <td class="text-right">Writing Style</td>
                                                        <td><strong>{{$order->writing_style_id}}</strong></td>
                                                    </tr>

                                                    <tr id="trow_1">
                                                        <td class="text-right">Status</td>
                                                        <td><strong>{{$order->status->status}}</strong></td>
                                                    </tr>



                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>






                                        <div class="tab-pane" id="tab2">
                                            <p>Donec tristique eu sem et aliquam. Proin sodales elementum urna et euismod. Quisque nisl nisl, venenatis eget dignissim et, adipiscing eu tellus. Sed nulla massa, luctus id orci sed, elementum consequat est. Proin dictum odio quis diam gravida facilisis. Sed pharetra dolor a tempor tristique. Sed semper sed urna ac dignissim. Aenean fermentum leo at posuere mattis. Etiam vitae quam in magna viverra dictum. Curabitur feugiat ligula in dui luctus, sed aliquet neque posuere.</p>
                                        </div>





                                        <div class="tab-pane" id="tab3">

                                            <p>
                                                Any edits on this page will NOT be saved.
                                            </p>
                                            <div style="padding-bottom: 35px; padding-top: 20px">
                                                <h4>Paper Details</h4>
                                                <textarea rows="4" cols="50"  class="summernote">{{html_entity_decode($order->paper_details)}}</textarea>
                                            </div>
                                            <div>
                                                <h4>Client Note</h4>
                                                <textarea rows="4" cols="50" class="summernote">{{html_entity_decode($order->client_note)}}</textarea>
                                            </div>
                                        </div>






                                    </div>
                                </div>
                                <!-- END JUSTIFIED TABS -->
                            </div>










                    </div>


                </div>



        </div>





    </div>
    <!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTENT -->
@endsection
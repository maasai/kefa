@extends('admin.layouts.main')

@section('content')
        <!-- PAGE CONTENT -->
<div class="page-content">

    @include('admin.common.navigation_horizontal')


            <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Basic</li>
    </ul>
    <!-- END BREADCRUMB -->

    <!-- PAGE TITLE -->
    <div class="page-title">
        <h2><span class="fa fa-folder-open"></span> New Order </h2>
    </div>
    <!-- END PAGE TITLE -->

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-xs-12">


                <div class="box box-primary">
                    <div class="panel panel-default">
                        <div class="panel-body">

                            @if (session()->has('success'))
                                <div class="alert alert-success" id="success-alert">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                                    {{ session('success') }}
                                </div>
                            @endif
                            @if (session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                            @endif

                            {!! Form::open(['route' => ['admin.orders.store'], 'class' => 'ajax-submit']) !!}
                            <div class="modal-body">

                                <div class="col-sm-4 invoice-col">
                                <div class="form-group">
                                    {!! Form::label('client','Client') !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-user"></div></div>
                                        {!! Form::select('client_id',$clients, null, ['class' => 'form-control select input-md',  'data-live-search'=>"true", 'required']) !!}
                                    </div>
                                </div>
                            </div><!-- /.col -->

                                <div class="col-sm-4 invoice-col">
                                    <div class="form-group">
                                        {!! Form::label('title','Order Title') !!}
                                        <div class="input-group">
                                            <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                            {!! Form::text('title',null,['class' => 'form-control input-sm','required']) !!}
                                        </div>
                                    </div>
                                </div><!-- /.col -->

                                <div class="col-sm-4 invoice-col">


                                    <div class="form-group">
                                        {!! Form::label('paper_type_id','Paper Type') !!}
                                        <div class="input-group">
                                            <div class="input-group-addon"><div class="fa fa-file"></div></div>
                                            {!! Form::select('paper_type_id',$types, null, ['class' => 'form-control select input-md',   'data-live-search'=>"true", 'required']) !!}
                                        </div>
                                    </div>

                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    <div class="form-group">
                                        {!! Form::label('subject_area_id','Subject Area') !!}
                                        <div class="input-group">
                                            <div class="input-group-addon"><div class="fa fa-file"></div></div>
                                            {!! Form::select('subject_area_id',$subjects, null, ['class' => 'form-control select input-md',   'data-live-search'=>"true", 'required']) !!}
                                        </div>
                                    </div>

                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">

                                    <div class="form-group">
                                        {!! Form::label('academic_level_id','Academic Level') !!}
                                        <div class="input-group">
                                            <div class="input-group-addon"><div class="fa fa-file"></div></div>
                                            {!! Form::select('academic_level_id',$levels, null, ['class' => 'form-control select input-md',   'data-live-search'=>"true", 'required']) !!}
                                        </div>
                                    </div>

                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    <div class="form-group">
                                        {!! Form::label('writing_style_id','Writing Style') !!}
                                        <div class="input-group">
                                            <div class="input-group-addon"><div class="fa fa-file"></div></div>
                                            {!! Form::select('writing_style_id',$styles, null, ['class' => 'form-control select input-md',   'data-live-search'=>"true", 'required']) !!}
                                        </div>
                                    </div>

                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    <div class="form-group">
                                        {!! Form::label('spacing_id','Paper Spacing') !!}
                                        <div class="input-group">
                                            <div class="input-group-addon"><div class="fa fa-file"></div></div>
                                            {!! Form::select('spacing_id',$spacings, null, ['class' => 'form-control select input-md',   'data-live-search'=>"true", 'required']) !!}
                                        </div>
                                    </div>

                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">

                                <div class="form-group">
                                    {!! Form::label('sources','Number Of Sources') !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-sort-numeric-asc"></div></div>
                                        {!! Form::number('sources',null,['class' => 'form-control input-sm','required', 'min'=>"0"]) !!}
                                    </div>
                                </div>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    <div class="form-group">
                                        {!! Form::label('pages','Pages') !!}
                                        <div class="input-group">
                                            <div class="input-group-addon"><div class="fa fa-sort-numeric-asc"></div></div>
                                            {!! Form::number('pages',null,['class' => 'form-control input-sm','required', 'min'=>"0"]) !!}
                                        </div>
                                    </div>
                                                                  </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    <div class="form-group">
                                        {!! Form::label('urgency_id','Paper Urgency') !!}
                                        <div class="input-group">
                                            <div class="input-group-addon"><div class="fa fa-file"></div></div>
                                            {!! Form::select('urgency_id',$urgencies, null, ['class' => 'form-control select input-md',   'data-live-search'=>"true"]) !!}
                                        </div>
                                    </div>

                                </div><!-- /.col -->

                                <div class="col-sm-4 invoice-col">
                                    <div class="form-group">
                                        {!! Form::label('deadline_date','DeadLine Date') !!}
                                        <div class="input-group">
                                            <div class="input-group-addon"><div class="fa fa-clock-o"></div></div>
                                            {!! Form::text('deadline_date',null,['class' => 'form-control datepicker input-sm','required']) !!}
                                        </div>
                                    </div>

                                </div><!-- /.col -->

                                <div class="col-sm-4 invoice-col">

                                    <div class="form-group">
                                        {!! Form::label('status_id','Order Status') !!}
                                        <div class="input-group">
                                            <div class="input-group-addon"><div class="fa fa-file"></div></div>
                                            {!! Form::select('status_id',$statuses, null, ['class' => 'form-control select input-md', 'data-live-search'=>"true"]) !!}
                                        </div>
                                    </div>
                                </div><!-- /.col -->

                                <div class="row col-sm-12">
                                <div class="form-group">
                                    {!! Form::label('paper_details','Extra Details') !!}
                                    <div class="input-group">
                                        {!! Form::textarea('paper_details', null, ['class' => 'form-control summernote']) !!}
                                    </div>
                                </div>




                                    <div class="form-group">
                                    {!! Form::label('client_note','Client Note') !!}
                                    <div class="input-group">
                                        {!! Form::textarea('client_note',null,['class' => 'form-control summernote input-sm', 'placeholder'=>'Enter Any Client Details']) !!}

                                    </div>
                                </div>
                                </div>

                            </div>
                            <!-- this row will not appear when printing -->
                            <div class="row no-print">
                                <div class="col-xs-12" style="margin-top: 20px">
                                    <button class="btn btn-success pull-right"><i class="fa fa-save"></i> {{ 'Save' }}</button>
                                    <button type="reset" class="btn btn-danger"><i class="fa fa-times"></i> Clear </button>

                                </div>
                            </div>
                            {!! Form::close() !!}

                        </div>
                    </div>


                </div>



            </div>
        </div>





    </div>
    <!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTENT -->
@endsection
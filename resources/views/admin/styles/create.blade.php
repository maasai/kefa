@extends('layouts.modal')

@section('content')
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5>Add Writing Style</h5>
            </div>
            {!! Form::open(['route' => ['admin.system.styles.store'], 'class' => 'ajax-submit']) !!}
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('writing_style','Writing Style') !!}
                    {!! Form::text('writing_style', null, ['class'=>'form-control required', 'required']) !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>Save</button>
                <button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-times"></i>Close</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
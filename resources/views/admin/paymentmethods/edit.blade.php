@extends('layouts.modal')

@section('content')
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5>Edit Payment Method</h5>
            </div>
            {!! Form::model($paymentMethod, ['route' => ['admin.system.payment_methods.update', $paymentMethod->uuid],'method'=>'PATCH', 'class'=>'ajax-submit']) !!}

            <div class="modal-body">

                <div class="form-group">
                    {!! Form::label('name','Payment Method Name') !!}
                    {!! Form::text('name', null, ['class'=>'form-control required', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('code','Payment Method Code') !!}
                    {!! Form::text('code', null, ['class'=>'form-control required', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('description','Payment Method Description') !!}
                    {!! Form::text('description', null, ['class'=>'form-control required', 'required']) !!}
                </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Update</button>
                <button type="button" data-dismiss="modal" class="btn btn-danger">Close</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
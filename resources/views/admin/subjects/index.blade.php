@extends('admin.layouts.main')

@section('content')
        <!-- PAGE CONTENT -->
<div class="page-content">

    @include('admin.common.navigation_horizontal')
    @include('admin.common.system_data_breadcrumb')
            <!-- START CONTENT FRAME -->
    <div class="content-frame">

        <!-- START CONTENT FRAME TOP -->
        <div class="content-frame-top">

            <div class="pull-right">
                <button class="btn btn-default content-frame-left-toggle"><span class="fa fa-bars"></span></button>
            </div>
        </div>
        <!-- END CONTENT FRAME TOP -->

        <!-- START CONTENT FRAME LEFT -->
        <div class="content-frame-left">
            <div class="panel panel-default">

                @include('admin.common.system_navigation')


            </div>
        </div>
        <!-- END CONTENT FRAME LEFT -->

        <!-- START CONTENT FRAME BODY -->
        <div class="content-frame-body">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="page-title">
                        <h2><span class="fa fa-bars"></span> Paper Subjects </h2>
                    </div>

                <!-- START DATATABLE EXPORT -->
                <div class="panel panel-default">





                    <div class="panel-heading">

                              <div class="btn-toolbar pull-right">
                               <a  class="btn btn-primary" data-toggle="ajax-modal" href="{{ route('admin.system.subjects.create') }}">
                                <i class="fa fa-plus-circle"></i> New Subject</a>

                                  <div class="btn-group pull-right">
                                <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onClick ="$('#customers2').tableExport({type:'xml',escape:'false'});"><img src='{{ asset('assets\admin\img\icons/xml.png')}}' width="24"/> XML</a></li>
                                <li class="divider"></li>
                                <li><a href="#" onClick ="$('#customers2').tableExport({type:'csv',escape:'false'});"><img src='{{ asset('assets\admin\img\icons/csv.png')}}' width="24"/> CSV</a></li>
                                <li><a href="#" onClick ="$('#customers2').tableExport({type:'txt',escape:'false'});"><img src='{{ asset('assets\admin\img\icons/txt.png')}}' width="24"/> TXT</a></li>
                                <li class="divider"></li>
                                <li><a href="#" onClick ="$('#customers2').tableExport({type:'excel',escape:'false'});"><img src='{{ asset('assets\admin\img\icons/xls.png')}}' width="24"/> XLS</a></li>
                                <li><a href="#" onClick ="$('#customers2').tableExport({type:'doc',escape:'false'});"><img src='{{ asset('assets\admin\img\icons/word.png')}}' width="24"/> Word</a></li>
                                <li class="divider"></li>
                            </ul>
                        </div>
                        </div>

                    </div>
                    <div class="panel-body">


                        @if (session()->has('success'))
                            <div class="alert alert-success" id="success-alert">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                                {{ session('success') }}
                            </div>
                        @endif
                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif



                        <table id="customers2" class="table datatable">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Subject</th>

                                <th>Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($subjects as $subject)
                                <tr>
                                    <td>
                                        <div class="radio">
                                            <label for="{{$subject['uuid']}}"> {!! Form::checkbox('selected',$subject['uuid'],null,['class'=>'minimal select_record']) !!} </label> &nbsp;
                                        </div>
                                    </td>

                                    <td>{{ $subject->subject_area }}</td>

                                    <td>

                                        <a  class="btn btn-success btn-sm" data-toggle="ajax-modal" href="{{ route('admin.system.subjects.edit', $subject->uuid) }}">
                                            <i class="fa fa-pencil"></i> Edit </a>


                                        {!! Form::open(array("method"=>"DELETE", "route" => ['admin.system.subjects.destroy', $subject->uuid], 'class' => 'form-inline', 'style'=>'display:inline')) !!}
                                        <a class="btn btn-danger btn-sm btn-delete"><i class="fa fa-trash-o"></i>  Delete</a>
                                        {!! Form::close() !!}

                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {!! Form::open(array("method"=>"DELETE", "route" => ['admin.system.subjects.destroy', ''], 'class' => 'form-inline', 'subject'=>'display:inline')) !!}
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <a class="btn btn-danger btn-sm btn-delete-multiple"><i class="fa fa-trash-o"></i> Delete Multiple </a>
                        {!! Form::close() !!}

                    </div>
                </div>
                <!-- END DATATABLE EXPORT -->
            </div>
        </div>

</div>


    </div>
    <!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTENT -->
@endsection
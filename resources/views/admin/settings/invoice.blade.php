@extends('admin.layouts.main')

@section('content')

        <!-- PAGE CONTENT -->
<div class="page-content">

    @include('admin.common.navigation_horizontal')

            <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Forms Stuff</a></li>
        <li><a href="#">Form Layout</a></li>
        <li class="active">One Column</li>
    </ul>
    <!-- END BREADCRUMB -->


    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">

                    @if (session()->has('success'))
                        <div class="alert alert-success" id="success-alert">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                            {{ session('success') }}
                        </div>
                    @endif
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif

                @if($settings)

                    {!! Form::model($settings, ['route' => ['admin.settings.invoice.update', $settings->uuid],'class'=>'ajax-submit', 'method'=>'PATCH', 'files'=>true]) !!}
                @else
                    {!! Form::open(['route' => ['admin.settings.invoice.store'],'class'=>'ajax-submit', 'files'=>true]) !!}
                @endif


                <div class="panel panel-default">
                    <div class="error">
                        @if($errors->any())
                            dd($errors);
                            @foreach($errors->all() as $error)
                                {{$error}}
                            @endforeach
                        @endif
                    </div>
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Invoice Settings</strong></h3>

                    </div>

                    <div class="panel-body">

                        <div class="row">

                            <div class="col-md-6">

                                <div class="form-group">
                                    {!! Form::label('starting_number','Invoice Starting Number') !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('starting_number',null,['class' => 'form-control input-sm','required', 'placeholder'=>'Enter Invoice Starting Number']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('terms','Invoice Terms') !!}
                                    <div class="input-group">
                                        {!! Form::textarea('terms',null,['class' => 'form-control summernote input-sm', 'placeholder'=>'Enter Invoice Terms']) !!}
                                    </div>
                                </div>


                            </div>



                            <div class="col-md-6">
                               <div class="form-group">
                                    {!! Form::label('logo','Invoice Logo', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        {!! Form::file('logo') !!}
                                    </div>
                                </div>







                            </div>

                        </div>

                    </div>



                    <div class="panel-footer">
                        {!! Form::reset('Clear', ['class="btn btn-default"']) !!}
                        {!! Form::submit('Update', ['class="btn btn-primary pull-right"']) !!}

                    </div>
                </div>
                {!! Form::close() !!}

            </div>
        </div>

    </div>
    <!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTENT -->
@endsection
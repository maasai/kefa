@extends('admin.layouts.main')

@section('content')

        <!-- PAGE CONTENT -->
<div class="page-content">

    @include('admin.common.navigation_horizontal')

            <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Forms Stuff</a></li>
        <li><a href="#">Form Layout</a></li>
        <li class="active">One Column</li>
    </ul>
    <!-- END BREADCRUMB -->


    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                @if (session()->has('success'))
                    <div class="alert alert-success" id="success-alert">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                        {{ session('success') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif

                    @if($settings)

                        {!! Form::model($settings, ['route' => ['admin.settings.company.update', $settings->uuid],'class'=>'ajax-submit', 'method'=>'PATCH', 'files'=>true]) !!}
                    @else
                        {!! Form::open(['route' => ['admin.settings.company.store'],'class'=>'ajax-submit', 'files'=>true]) !!}
                    @endif


                <div class="panel panel-default">
                    <div class="error">
                        @if($errors->any())
                            dd($errors);
                            @foreach($errors->all() as $error)
                                {{$error}}
                            @endforeach
                        @endif
                    </div>
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Company Settings</strong></h3>

                    </div>

                    <div class="panel-body">

                        <div class="row">

                            <div class="col-md-6">

                                <div class="form-group">
                                    {!! Form::label('company_name','Company Name', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('company_name',null,['class' => 'form-control input-sm','required', 'placeholder'=>'Enter Company Name']) !!}
                                    </div>
                                </div>


                                <div class="form-group">
                                    {!! Form::label('contact_person','Contact Person', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('contact_person',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter Company Contact Person']) !!}
                                    </div>
                                </div>


                                <div class="form-group">
                                    {!! Form::label('email','Email', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('email',null,['class' => 'form-control input-sm','required', 'placeholder'=>'Enter Company Email']) !!}
                                    </div>
                                </div>


                                <div class="form-group">
                                    {!! Form::label('phone','Phone', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('phone',null,['class' => 'form-control input-sm','required', 'placeholder'=>'Enter Company Phone']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('address','Address', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('address',null,['class' => 'form-control input-sm','required', 'placeholder'=>'Enter Company Address']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('city','City / Town', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('city',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter City / Town']) !!}
                                    </div>
                                </div>


                            </div>



                            <div class="col-md-6">

                                <div class="form-group">
                                    {!! Form::label('state','State / Province', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('state',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter State / Province']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('postal_code','Postal Code', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('postal_code',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter Postal Code']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('country','Country', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('country',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter Country']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('website','Website', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('website',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter Company Website']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('vat_number','VAT Number', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('vat_number',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter Company VAT Number']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('paypal_email','PAYPAL Email', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('paypal_email',null,['class' => 'form-control input-sm','required', 'placeholder'=>'Enter Company PAYPAL Email']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('logo','Company Logo', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        {!! Form::file('logo') !!}
                                    </div>
                                </div>







                            </div>

                        </div>

                    </div>



                    <div class="panel-footer">
                        {!! Form::reset('Clear', ['class="btn btn-default"']) !!}
                        {!! Form::submit('Update', ['class="btn btn-primary pull-right"']) !!}

                    </div>
                </div>
                {!! Form::close() !!}

            </div>
        </div>

    </div>
    <!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTENT -->
@endsection
@extends('admin.layouts.main')

@section('content')

        <!-- PAGE CONTENT -->
<div class="page-content">

    @include('admin.common.navigation_horizontal')

            <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Forms Stuff</a></li>
        <li><a href="#">Form Layout</a></li>
        <li class="active">One Column</li>
    </ul>
    <!-- END BREADCRUMB -->


    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                @if (session()->has('success'))
                    <div class="alert alert-success" id="success-alert">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                        {{ session('success') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif

                @if($settings)

                    {!! Form::model($settings, ['route' => ['admin.settings.profile.update', $settings->uuid],'class'=>'ajax-submit', 'method'=>'PATCH', 'files'=>true]) !!}
                @else
                    {!! Form::open(['route' => ['admin.settings.profile.store'],'class'=>'ajax-submit', 'files'=>true]) !!}
                @endif


                <div class="panel panel-default">
                    <div class="error">
                        @if($errors->any())
                            dd($errors);
                            @foreach($errors->all() as $error)
                                {{$error}}
                            @endforeach
                        @endif
                    </div>
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong> My Profile </strong></h3>

                    </div>

                    <div class="panel-body">

                        <div class="row">

                            <div class="col-md-6">

                                <div class="form-group">
                                    {!! Form::label('first_name','First Name', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('first_name',null,['class' => 'form-control input-sm','required', 'placeholder'=>'Enter First Name']) !!}
                                    </div>
                                </div>


                                <div class="form-group">
                                    {!! Form::label('last_name','Last Name', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('last_name',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter Last Name']) !!}
                                    </div>
                                </div>


                                <div class="form-group">
                                    {!! Form::label('email','Email', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('email',null,['class' => 'form-control input-sm','required', 'placeholder'=>'Enter Email']) !!}
                                    </div>
                                </div>


                                <div class="form-group">
                                    {!! Form::label('phone_1','Phone #1', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('phone_1',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter Phone #1']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('phone_2','Phone #2', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('phone_2',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter Phone #2']) !!}
                                    </div>
                                </div>




                            </div>



                            <div class="col-md-6">




                                <div class="form-group">
                                    {!! Form::label('country','Country', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('country',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter Country']) !!}
                                    </div>
                                </div>





                                <div class="form-group">
                                    {!! Form::label('password','Password', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::password('password',['class' => 'form-control input-sm']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('password_confirmation','Confirm Password', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::password('password_confirmation',['class' => 'form-control input-sm']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('logo','Profile Picture', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        {!! Form::file('logo') !!}
                                    </div>
                                </div>







                            </div>

                        </div>

                    </div>



                    <div class="panel-footer">
                        {!! Form::reset('Clear', ['class="btn btn-default"']) !!}
                        {!! Form::submit('Update', ['class="btn btn-primary pull-right"']) !!}

                    </div>
                </div>
                {!! Form::close() !!}

            </div>
        </div>

    </div>
    <!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTENT -->
@endsection
@extends('layouts.modal')

@section('content')
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5>Edit Currency</h5>
            </div>
            {!! Form::model($currency, ['route' => ['admin.system.currencies.update', $currency->uuid],'method'=>'PATCH', 'class'=>'ajax-submit']) !!}

            <div class="modal-body">

                <div class="form-group">
                    {!! Form::label('currency_symbol','Currency Symbol') !!}
                    {!! Form::text('currency_symbol', null, ['class'=>'form-control required', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('currency_code','Currency Code') !!}
                    {!! Form::text('currency_code', null, ['class'=>'form-control required', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('default','Default Code') !!}
                    {!! Form::select('default', array('Yes' => 'Yes', 'No' => 'No'), null, ['class'=>'form-control required', 'required'])!!}
                </div>



            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Update</button>
                <button type="button" data-dismiss="modal" class="btn btn-danger">Close</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
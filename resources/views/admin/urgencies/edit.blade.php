@extends('layouts.modal')

@section('content')
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5>Edit Writing Urgency</h5>
            </div>
            {!! Form::model($urgency, ['route' => ['admin.system.urgencies.update', $urgency->uuid],'method'=>'PATCH', 'class'=>'ajax-submit']) !!}

            <div class="modal-body">

                <div class="form-group">
                    {!! Form::label('urgency','Paper Urgency') !!}
                    {!! Form::text('urgency', null, ['class'=>'form-control required', 'required']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('duration','Duration') !!}
                    {!! Form::number('duration',null,['class' => 'form-control input-sm', 'required', 'min'=>"0"]) !!}

                </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Update</button>
                <button type="button" data-dismiss="modal" class="btn btn-danger">Close</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
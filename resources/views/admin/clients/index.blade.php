@extends('admin.layouts.main')

@section('content')
<!-- PAGE CONTENT -->
<div class="page-content">

    @include('admin.common.navigation_horizontal')

    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Basic</li>
    </ul>
    <!-- END BREADCRUMB -->

    <!-- PAGE TITLE -->
    <div class="page-title">
        <h2><span class="fa fa-arrow-circle-o-left"></span> Registered Clients</h2>
    </div>
    <!-- END PAGE TITLE -->

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">

                <!-- START DATATABLE EXPORT -->
                <div class="panel panel-default">

                    <div class="panel-heading">

                              <div class="btn-toolbar pull-right">
                               <a  class="btn btn-primary" data-toggle="ajax-modal" href="{{ route('admin.clients.create') }}">
                                <i class="fa fa-plus-circle"></i> New Client </a>

                                  <div class="btn-group pull-right">
                                <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onClick ="$('#customers2').tableExport({type:'xml',escape:'false'});"><img src='{{ asset('assets\admin\img\icons/xml.png')}}' width="24"/> XML</a></li>
                                <li class="divider"></li>
                                <li><a href="#" onClick ="$('#customers2').tableExport({type:'csv',escape:'false'});"><img src='{{ asset('assets\admin\img\icons/csv.png')}}' width="24"/> CSV</a></li>
                                <li><a href="#" onClick ="$('#customers2').tableExport({type:'txt',escape:'false'});"><img src='{{ asset('assets\admin\img\icons/txt.png')}}' width="24"/> TXT</a></li>
                                <li class="divider"></li>
                                <li><a href="#" onClick ="$('#customers2').tableExport({type:'excel',escape:'false'});"><img src='{{ asset('assets\admin\img\icons/xls.png')}}' width="24"/> XLS</a></li>
                                <li><a href="#" onClick ="$('#customers2').tableExport({type:'doc',escape:'false'});"><img src='{{ asset('assets\admin\img\icons/word.png')}}' width="24"/> Word</a></li>
                                <li class="divider"></li>
                            </ul>
                        </div>
                        </div>

                    </div>
                    <div class="panel-body">


                        @if (session()->has('success'))
                            <div class="alert alert-success" id="success-alert">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                                {{ session('success') }}
                            </div>
                        @endif
                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif

                        <table id="customers2" class="table datatable">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Client Name</th>
                                <th>Country</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($clients as $client)
                                <tr>
                                    <td>
                                        <div class="radio">
                                            <label for="{{$client['uuid']}}"> {!! Form::checkbox('selected',$client['uuid'],null,['class'=>'minimal select_record']) !!} </label> &nbsp;
                                        </div>
                                    </td>

                                    <td>{{ $client->client_name }}</td>
                                    <td>{{ $client->country }}</td>
                                    <td>{{ $client->email }}</td>
                                    <td>{{ $client->status }}</td>

                                    <td>

                                        @if(Auth::user()->uuid == $client->uuid)
                                            <a  class="btn btn-success btn-sm" data-toggle="ajax-modal" href="{{ route('admin.clients.edit', $client->uuid) }}">
                                                <i class="fa fa-pencil"></i> Edit Profile</a>
                                        @endif

                                        @if(Auth::user()->uuid != $client->uuid)
                                        <a  class="btn btn-success btn-sm" data-toggle="ajax-modal" href="{{ route('admin.clients.edit', $client->uuid) }}">
                                            <i class="fa fa-pencil"></i> Edit </a>
                                        {!! Form::open(array("method"=>"DELETE", "route" => ['admin.clients.destroy', $client->uuid], 'class' => 'form-inline', 'style'=>'display:inline')) !!}
                                        <a class="btn btn-danger btn-sm btn-delete"><i class="fa fa-trash-o"></i>  Delete</a>
                                        {!! Form::close() !!}
                                        @endif
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {!! Form::open(array("method"=>"DELETE", "route" => ['admin.clients.destroy', ''], 'class' => 'form-inline', 'style'=>'display:inline')) !!}
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <a class="btn btn-danger btn-sm btn-delete-multiple"><i class="fa fa-trash-o"></i> Delete Multiple </a>
                        {!! Form::close() !!}

                    </div>
                </div>
                <!-- END DATATABLE EXPORT -->
            </div>
        </div>


    </div>
    <!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTENT -->
@endsection
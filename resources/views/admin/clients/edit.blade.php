@extends('layouts.modal')

@section('content')
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5>Edit Client</h5>
            </div>
            {!! Form::model($client, ['route' => ['admin.clients.update', $client->uuid],'method'=>'PATCH', 'class'=>'ajax-submit']) !!}

            <div class="modal-body">

                <div class="form-group">
                    {!! Form::label('client_name','Client Name') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                        {!! Form::text('client_name',null,['class' => 'form-control input-sm','required', 'placeholder'=>'Enter Client Name']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('phone','Phone') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-mobile-phone"></div></div>
                        {!! Form::text('phone',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter Phone']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('email','Email') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-envelope-o"></div></div>
                        {!! Form::text('email',null,['class' => 'form-control input-sm','required', 'placeholder'=>'Enter Email']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('address','Address') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                        {!! Form::text('address',null,['class' => 'form-control', 'placeholder'=>'Enter Address']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('city','City') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                        {!! Form::text('city',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter City']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('state','State / Province') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                        {!! Form::text('state',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter State / Province']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('postal_code','Postal Code') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                        {!! Form::text('postal_code',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter Postal Code']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('country','Country') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                        {!! Form::text('country',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter Country Name']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('website','Website') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                        {!! Form::text('website',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter Client Website']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('notes','Notes') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                        {!! Form::text('notes',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter notes']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('status','Client Status') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                        {!! Form::select('status', array('Active' => 'Active', 'Disabled' => 'Disabled'), null, ['class'=>'form-control required', 'required'])!!}

                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('password','Password') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-unlock-alt"></div></div>
                        {!! Form::password('password',['class' => 'form-control input-sm']) !!}

                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('password_confirmation','Confirm Password') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-unlock-alt"></div></div>
                        {!! Form::password('password_confirmation',['class' => 'form-control input-sm']) !!}

                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Update</button>
                <button type="button" data-dismiss="modal" class="btn btn-danger">Close</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
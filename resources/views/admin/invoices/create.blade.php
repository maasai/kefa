@extends('admin.layouts.main')

@section('content')
        <!-- PAGE CONTENT -->
<div class="page-content">

    @include('admin.common.navigation_horizontal')


            <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Basic</li>
    </ul>
    <!-- END BREADCRUMB -->

    <!-- PAGE TITLE -->
    <div class="page-title">
        <h2><span class="fa fa-folder-open"></span> New Invoice </h2>
    </div>
    <!-- END PAGE TITLE -->

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-xs-12">


                <div class="box box-primary">
                        <div class="panel panel-default">
                            <div class="panel-body">

                    <section class="invoice">
                        {!! Form::open(['route' => ['admin.invoices.store'], 'id' => 'invoice_form', 'data-toggle'=>"validator", 'role' =>"form"]) !!}

                                <!-- info row -->
                        <div class="row invoice-info">
                            <div class="col-sm-4 invoice-col">
                                <div class="form-group">
                                    {!! Form::label('client', 'Client') !!}
                                    {!! Form::select('client',$clients, null, ['class' => "form-control select input-sm", 'data-live-search'=>"true", "required"]) !!}
                                </div>
                            </div><!-- /.col -->

                            <div class="col-sm-4 invoice-col">
                                <div class="form-group">
                                    {!! Form::label('status', 'Status') !!}
                                    {!! Form::select('status',array('Draft','Sent', 'Void'), null, ['class' => "form-control select input-sm", 'data-live-search'=>"true", "required"]) !!}
                                </div>
                            </div><!-- /.col -->

                            <div class="col-sm-4 invoice-col">
                                <div class="form-group">
                                    {!! Form::label('invoice_date', 'Invoice Date') !!}
                                    {!! Form::text('invoice_date',null, ['class' => "form-control input-sm datepicker", "required"]) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('due_date', 'Due Date') !!}
                                    {!! Form::text('due_date',null, ['class' => "form-control input-sm datepicker", "required"]) !!}
                                </div>


                            </div><!-- /.col -->


                        </div><!-- /.row -->






                        <!-- Table row -->
                        <div class="row">
                            <div class="col-xs-12">
                                <table class="table table-striped" id="item_table">
                                    <thead>
                                    <tr class="items_header">
                                        <th width="4%"></th>
                                        <th style="width: 10%">{{ 'Order #' }}</th>
                                        <th>{{ 'Order Title' }}</th>
                                        <th>{{ 'Description' }}</th>
                                        <th class="text-right">{{ 'Amount' }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="item">
                                        <td></td>
                                        <td><div class="form-group"> {!! Form::select('order_id',$orders, null, ['class' => 'form-control select input-md',   'data-live-search'=>"true",'id' => 'order_id', 'onchange' => 'fill_order((this.value))']) !!}</div></td>
                                        <td><div class="form-group">{!! Form::textarea('order_title',null, ['class' => "form-control input-sm item_description", "required", "readonly", "rows"=>"2"]) !!}</div></td>
                                        <td><div class="form-group">{!! Form::textarea('description',null, ['class' => "form-control input-sm item_description", "required","rows"=>"2"]) !!}</div></td>

                                        <td class="text-right"><span class="currencySymbol"></span><span class="itemTotal">0.00</span></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div><!-- /.col -->
                        </div><!-- /.row -->






















                        <!-- Table row -->
                        <div class="row">
                            <div class="col-xs-12 table-responsive">
                                <table class="table table-striped" id="item_table">
                                    <thead>
                                    <tr class="items_header">
                                        <th width="4%"></th>
                                        <th>{{ 'Item' }}</th>
                                        <th>{{ 'Order #' }}</th>
                                        <th>{{ 'Description' }}</th>
                                        <th class="text-right" style="width: 10%">{{ 'Quantity' }}</th>
                                        <th class="text-right" style="width: 10%">{{ 'Price' }}</th>
                                        <th class="text-right">{{ 'Total' }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="item">
                                        <td></td>
                                        <td><div class="form-group">{!! Form::text('item_name',null, ['class' => "form-control input-sm item_name", "required"]) !!}</div></td>
                                        <td><div class="form-group"> {!! Form::select('order_id',$orders, null, ['class' => 'form-control select input-md',   'data-live-search'=>"true"]) !!}</div></td>

                                        <td><div class="form-group">{!! Form::textarea('item_description',null, ['class' => "form-control input-sm item_description", "required","rows"=>"2"]) !!}</div></td>
                                        <td><div class="form-group">{!! Form::text('item_quantity',null, ['class' => "form-control input-sm item_quantity calcEvent", "required"]) !!}</div></td>
                                        <td><div class="form-group">{!! Form::text('item_price',null, ['class' => "form-control input-sm item_price calcEvent", "required"]) !!}</div></td>
                                        <td class="text-right"><span class="currencySymbol"></span><span class="itemTotal">0.00</span></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div><!-- /.col -->
                        </div><!-- /.row -->

                        <div class="row">
                            <div class="col-xs-6">
                                <span id="btn_add_row" class="btn btn-sm btn-info" onclick="addRow('item_table')"><i class="fa fa-plus"></i> {{ 'Add Row' }}</span>
                                <span id="btn_items_list_modal" class="btn btn-sm btn-primary "><i class="fa fa-plus"></i> {{ 'Add Existing Order' }}</span>

                            </div>

                            <div class="col-xs-6">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <th style="width:50%">{{ 'Sub Total' }}</th>
                                            <td class="text-right">
                                                <span class="currencySymbol"></span>
                                                <span id="subTotal">0.00</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>{{ 'Tax' }}</th>
                                            <td class="text-right">
                                                <span class="currencySymbol"></span>
                                                <span id="taxTotal">0.00</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>{{ 'Discount' }}</th>
                                            <td class="text-right">
                                                <span class="currencySymbol"></span>
                                                {!! Form::text('discount', '0.00',['class'=>'form-control text-right input-sm calcEvent']) !!}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>{{ 'Total' }}</th>
                                            <td class="text-right">
                                                <span class="currencySymbol"></span>
                                                <span id="grandTotal"><b>0.00</b></span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div><!-- /.col -->


                        </div><!-- /.row -->
                        <div class="row">
                            <div class="form-group">
                                {!! Form::label('invoice_terms', 'Invoice Terms') !!}
                                {!! Form::textarea('invoice_terms',$invoiceSetting ? $invoiceSetting->default_terms : null, ['class' => "form-control input-sm", "rows" => "2", "required"]) !!}
                            </div>
                        </div>

                        <!-- this row will not appear when printing -->
                        <div class="row no-print">
                            <div class="col-xs-12" style="margin-top: 20px">
                                <button class="btn btn-success pull-right"><i class="fa fa-save"></i> {{ 'Save' }}</button>
                                <button type="reset" class="btn btn-danger"><i class="fa fa-times"></i> Clear </button>

                            </div>
                        </div>
                        {!! Form::close() !!}
                    </section>

                    </div>
                </div>


</div>



            </div>
        </div>





    </div>
    <!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTENT -->
@endsection
@section('scripts')
    @include('admin.invoices.partials.invoices_js')
@endsection
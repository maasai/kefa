<script>
    $(function() {

        $(document).on('click', '.delete_row', function () {
            $(this).parents('tr').remove();
            calcTotals();
        });
        $( document ).on("click change paste keyup", ".calcEvent", function() {
            calcTotals();
        });

        $('#btn_items_list_modal').click(function() {
            $('.invoice').addClass('spinner');
            var $modal = $('#ajax-modal');
            $.get('{{url("backend/items_modal")}}', function(data) {
                $modal.modal();
                $modal.html(data);
                $('.invoice').removeClass('spinner');
            });
        });

        $('#invoice_form').validator().on('submit', function (e) {
            if (!e.isDefaultPrevented()) {
                $('.invoice').addClass('spinner');
                $('.invoice .alert-danger').remove();
                var $form = $('#invoice_form');
                var data = $('#invoice_form select, input, textarea').not('#item_name, #item_description, #item_quantity, #item_price, #itemId').serializeArray();
                var items = [];
                var item_order = 1;
                $('table tr.item').each(function() {
                    var row = {};
                    $(this).find('input,select,textarea').each(function()
                    {
                        if($(this).attr('name')) row[$(this).attr('name')] = $(this).val();
                    });
                    items.push(row);
                });

                data.push({name : 'items', value: JSON.stringify(items)});
                $.post($form.attr('action'), data , 'json').done(function(data){
                    if(data.errors)
                    {
                        return;
                    }
                    if(data.redirectTo){
                        window.location = data.redirectTo;
                    }else {
                        window.location.reload();
                    }
                }).fail(function(jqXhr, json, errorThrown){
                    var errors = jqXhr.responseJSON.errors;
                    var errorStr = '';
                    $.each( errors, function( key, value ) {
                        $('#'+key).parents('.form-group').addClass('has-error');
                        $('.'+key).parents('.form-group').addClass('has-error');
                        errorStr += '- ' + value + '<br/>';
                    });

                    var errorsHtml= '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + errorStr + '</div>';
                    $('.invoice').prepend(errorsHtml);
                }).always(function(){
                    $('.invoice').removeClass('spinner');
                });
                return false;
            }
        });

        /* ----------------------------------------------------------------------------------------------------
         ADDING SELECTED ITEMS TO THE INVOICE
         --------------------------------------------------------------------------------------------------------*/
        $(document).on('click', '#select-items-confirm', function() {
            var items_lookup_ids = [];
            $("input[name='items_lookup_ids[]']:checked").each(function (){
                items_lookup_ids.push($(this).val());
            });
            $.post("{{ url('backend/process_items_selections') }}", {
                items_lookup_ids : items_lookup_ids,_token:'{{ csrf_token() }}'
            }).done(function(data){
                var items = data.items;
                for(var key in items) {
                    //noinspection JSJQueryEfficiency
                    var last_row = $('#item_table tr:last');
                    if (last_row.find('input[name=item_name]').val() !== '') {
                        addRow('item_table');
                        var last_row = $('#item_table tr:last');
                        last_row.find('input[name=item_name]').val(items[key].item_name);
                        last_row.find('textarea[name=item_description]').val(items[key].description);
                        last_row.find('input[name=item_price]').val(items[key].price);
                        last_row.find('input[name=item_quantity]').val('1');
                    }
                    else
                    {
                        last_row.find('input[name=item_name]').val(items[key].item_name);
                        last_row.find('textarea[name=item_description]').val(items[key].description);
                        last_row.find('input[name=item_price]').val(items[key].price);
                        last_row.find('input[name=item_quantity]').val('1');
                    }
                    calcTotals();
                }
            }).always(function(){
                $('#ajax-modal').modal('toggle');
            });
        });
    });
    function calcTotals(){
        var subTotal    = 0;
        var total       = 0;
        var tax_total   = 0;
        var tax         = $("#tax").val() != '' ? $("#tax").val() : 0;
        var discount    = parseFloat($('form#invoice_form').find("[name='discount']").val());

        $('tr.item').each(function(){
            var quantity    = parseFloat($(this).find("[name='item_quantity']").val());
            var price       = parseFloat($(this).find("[name='item_price']").val());
            var itemTotal   = parseFloat(quantity * price) > 0 ? parseFloat(quantity * price) : 0;
            subTotal += parseFloat(price * quantity) > 0 ? parseFloat(price * quantity) : 0;
            $(this).find(".itemTotal").text( itemTotal.toFixed(2) );
        });
        tax_total   += parseFloat(tax/100 * subTotal);
        total       += parseFloat(subTotal + tax_total-discount);

        $('#subTotal').text(subTotal.toFixed(2));
        $('#taxTotal').text(tax_total.toFixed(2));
        $('#grandTotal').text(total.toFixed(2));
    }


    function calcTotalsXx(){
        var subTotal    = 0;
        var total       = 0;
        var tax_total   = 0;
        var tax         = $("#tax").val() != '' ? $("#tax").val() : 0;
        var discount    = parseFloat($('form#invoice_form').find("[name='discount']").val());

        $('tr.item').each(function(){
            var quantity    = parseFloat($(this).find("[name='item_quantity']").val());
            var price       = parseFloat($(this).find("[name='item_price']").val());
            var itemTotal   = parseFloat(quantity * price) > 0 ? parseFloat(quantity * price) : 0;
            subTotal += parseFloat(price * quantity) > 0 ? parseFloat(price * quantity) : 0;
            $(this).find(".itemTotal").text( itemTotal.toFixed(2) );
        });
        tax_total   += parseFloat(tax/100 * subTotal);
        total       += parseFloat(subTotal + tax_total-discount);

        $('#subTotal').text(subTotal.toFixed(2));
        $('#taxTotal').text(tax_total.toFixed(2));
        $('#grandTotal').text(total.toFixed(2));
    }



    var count = "1";
    function addRow(tbl_name){
        var tbody = document.getElementById(tbl_name).getElementsByTagName("tbody")[0];
        // create row
        var row = document.createElement("tr");
        // create table cell 1
        var td1 = document.createElement("td");
        var strHtml1 = '<div class="form-group"><span class="btn btn-danger btn-xs delete_row"><i class="fa fa-minus"></i></span></div>';
        td1.innerHTML = strHtml1.replace(/!count!/g,count);
        // create table cell 2
        var td2 = document.createElement("td");
        var strHtml2 = '<div class="form-group">{!! Form::text("item_name",null, ["class" => "form-control input-sm item_name", "id"=>"item_name" , "required"]) !!}</div>';
        td2.innerHTML = strHtml2.replace(/!count!/g,count);
        // create table cell 3
        var td3 = document.createElement("td");
        var strHtml3 = '<div class="form-group">{!! Form::textarea("item_description",null, ["class" => "form-control item_description input-sm", "id"=>"item_description" ,"required", "rows"=>"2" ]) !!}</div>';
        td3.innerHTML = strHtml3.replace(/!count!/g,count);
        // create table cell 4
        var td4 = document.createElement("td");
        var strHtml4 = '<div class="form-group">{!! Form::input("number","item_quantity",null, ["class" => "form-control input-sm calcEvent quantity", "id"=>"item_quantity" , "required", "step" => "any", "min" => "1"]) !!}</div> ';
        td4.innerHTML = strHtml4.replace(/!count!/g,count);
        // create table cell 5
        var td5 = document.createElement("td");
        var strHtml5 = '<div class="form-group">{!! Form::input("number","item_price",null, ["class" => "form-control input-sm calcEvent price", "id"=>"item_price", "required","step" => "any", "min" => "1"]) !!}</div> ';
        td5.innerHTML = strHtml5.replace(/!count!/g,count);
        // create table cell 6
        var td6 = document.createElement("td");
        var strHtml6 = '<span class="currencySymbol"></span><span class="itemTotal">0.00</span> ';
        td6.innerHTML = strHtml6.replace(/!count!/g,count);
        td6.className = 'text-right';

        // append data to row
        row.appendChild(td1);
        row.appendChild(td2);
        row.appendChild(td3);
        row.appendChild(td4);
        row.appendChild(td5);
        row.appendChild(td6);

        // add to count variable
        count = parseInt(count) + 1;

        // append row to table
        tbody.appendChild(row);
        row.className = 'item';
    }

    function fill_order($order_id)
    {
        alert($orders);

    }
</script>
@extends('layouts.modal')

@section('content')
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5>New Payment</h5>
            </div>
            {!! Form::open(['route' => ['admin.payments.store'], 'class' => 'ajax-submit']) !!}
            <div class="modal-body">
                  <div class="form-group">
                    {!! Form::label('invoice_id','Invoice Number') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-file"></div></div>
                        {!! Form::select('invoice_id',$invoices, null, ['class' => 'form-control select input-md',   'data-live-search'=>"true", 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('received_date','Received On') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-clock-o"></div></div>
                        {!! Form::text('received_date',null,['class' => 'form-control datepicker input-sm','required']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('payment_method_id','Payment Method') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-file"></div></div>
                        {!! Form::select('payment_method_id',$payment_methods, null, ['class' => 'form-control select input-md',   'data-live-search'=>"true", 'required']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('amount','Amount') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><div class="fa fa-sort-numeric-asc"></div></div>
                        {!! Form::number('amount',null,['class' => 'form-control input-sm','required', 'min'=>"0"]) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('payment_note','Payment Note') !!}
                    {!! Form::textarea('payment_note', null, ['class'=>'form-control summernote', 'required']) !!}
                </div>


            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>Save</button>
                <button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-times"></i>Close</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
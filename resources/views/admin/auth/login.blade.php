@extends('admin.layouts.login')
@section('content')
<div class="login-body">
    <div class="login-title">
        <strong>Welcome</strong>, Please login

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                     {{ $error }}<br>
                @endforeach
            </div>
        @endif
    </div>



    {!! Form::open(['url'=>'admin/login', 'method'=> 'post', 'class'=>'form-horizontal']) !!}
    <div class="form-group">
        <div class="col-md-12">
        {!! Form::label('email', 'Email') !!}
        {!! Form::input('email','email', old('email'), ['class' =>"form-control", 'required'=>'required','placeholder'=>"your@email.com"]) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12">
        {!! Form::label('password', 'Password') !!}
        {!! Form::input('password','password', old('password'), ['class' =>"form-control",'required'=>'required','placeholder'=>""]) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-6">
            <a href="#" class="btn btn-link btn-block">Forgot your password?</a>
        </div>
        <div class="col-md-6">
            <input class="btn btn-info btn-block" type="submit" value="Login">

        </div>
    </div>
    {!! Form::close() !!}

</div>
@endsection

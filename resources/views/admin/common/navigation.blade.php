<!-- START PAGE SIDEBAR -->
<div class="page-sidebar">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation x-navigation-custom">
        <li class="xn-logo">
            <a href="#">ATLANT</a>
            <a href="#" class="x-navigation-control"></a>
        </li>
        <li class="xn-profile">
            <a href="#" class="profile-mini">
                <img src="{{ asset('assets/admin/images/users/avatar.jpg')}}" alt="John Doe"/>
            </a>
            <div class="profile">
                <div class="profile-image">
                    <img src="{{ asset('assets/admin/images/users/avatar.jpg')}}" alt="John Doe"/>
                </div>
                <div class="profile-data">
                    <div class="profile-data-name">John Doe</div>
                    <div class="profile-data-title">Writer</div>
                </div>
                <div class="profile-controls">
                    <a href="#" class="profile-control-left"><span class="fa fa-info"></span></a>
                    <a href="#" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                </div>
            </div>
        </li>
        <li class="xn-title">Navigation</li>
        <li class="{{ Html::menu_active('admin/dahsboard') }}">
            <a href="{{ Url('admin') }}"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>
        </li>

       <li class="xn-title">Work</li>
        <li class="xn-openable {{ Html::menu_active('admin/orders') }}">
            <a href="#"><span class="fa fa-briefcase"></span> <span class="xn-text">Orders</span></a>
            <ul>
                <li class="{{ Html::menu_active('admin/orders') }}"><a href="{{ Url('admin/orders') }}"><span class="fa fa-th-list"></span> Manage Orders</a></li>
            @foreach ($orderStatuses as $orderStatus)
                @if(count($orderStatus->orders) > 0)
                <li class="{{ Html::menu_active('admin/orders/?status='.$orderStatus->uuid) }}"><a href="{{ Url('admin/orders/?status='.$orderStatus->uuid) }}"><span class="fa fa-th-list"></span> {{$orderStatus->status}}
                        @if(trim(strtolower($orderStatus->status)) == "new")
                            <span class="informer informer-success">{{count($orderStatus->orders)}}</span></a>
                    @else
                        <span class="informer informer-info">{{count($orderStatus->orders)}}</span></a>
                    @endif


                </li>
                @endif
            @endforeach

            </ul>
        </li>
        <li class="xn-openable {{ Html::menu_active('admin/invoices') }}">
            <a href="#"><span class="fa fa-envelope-o"></span> <span class="xn-text">Invoices</span></a>
            <ul>
                <li class="{{ Html::menu_active('admin/invoices') }}"><a href="{{ Url('admin/invoices') }}"><span class="fa fa-th-list"></span> Manage Invoices</a></li>
                <li class="{{ Html::menu_active('admin/invoices/#') }}"><a href="#"><span class="fa fa-th-list"></span> Pending<span class="informer informer-warning">5</span> </a></li>
                <li class="{{ Html::menu_active('admin/invoices/#') }}"><a href="#"><span class="fa fa-th-list"></span> Overdue <span class="informer informer-danger">0</span> </a></li>
                <li class="{{ Html::menu_active('admin/invoices/#') }}"><a href="#"><span class="fa fa-th-list"></span> Paid<span class="informer informer-success">5</span></a></li>
            </ul>
        </li>

        <li class="{{ Html::menu_active('admin/payments') }}">
            <a href="{{ Url('admin/payments') }}"><span class="fa fa-dollar"></span> <span class="xn-text">Payments</span></a>
        </li>





        <li class="xn-title">App</li>

        <li class="{{ Html::menu_active('admin/system') }}"><a href="{{ Url('admin/system/styles') }}"><span class="fa fa-cog"></span> <span class="xn-text">System Data</span></a></li>
        <li class="xn-openable {{ Html::menu_active('admin/settings') }}">
            <a href="#"><span class="fa fa-cogs"></span> <span class="xn-text">Settings</span></a>
            <ul>
                <li class="{{ Html::menu_active('admin/settings/profile') }}"><a href="{{ Url('admin/settings/profile') }}"><span class="fa fa-user"></span> My Profile </a></li>
                <li class="{{ Html::menu_active('admin/settings/company') }}"><a href="{{ Url('admin/settings/company') }}"><span class="fa fa fa-tasks"></span> Company </a></li>
                <li class="{{ Html::menu_active('admin/settings/invoice') }}"><a href="{{ Url('admin/settings/invoice') }}"><span class="fa fa fa-tasks"></span> Invoice </a></li>
                <li class="{{ Html::menu_active('admin/settings/order') }}"><a href="{{ Url('admin/settings/order') }}"><span class="fa fa fa-tasks"></span> Order </a></li>

            </ul>
        </li>

        <li class="xn-title">People</li>
        <li class="{{ Html::menu_active('admin/clients') }}">
            <a href="{{ Url('admin/clients') }}"><span class="fa fa-users"></span> <span class="xn-text">Clients</span>
                <span class="informer informer-info">
                     @if(isset($totalClients))
                        {{$totalClients}}
                    @endif
                </span></a>
        </li>
        <li class="{{ Html::menu_active('admin/users') }}">
            <a href="{{ Url('admin/users') }}"><span class="fa fa-users"></span> <span class="xn-text">Users</span>
                <span class="informer informer-info">
                    @if(isset($totalUsers))
                        {{$totalUsers}}
                    @endif
                </span></a>
        </li>
        <li class="xn-title">Security</li>
        <li>
            <a href="{{ Url('admin/logout') }}"><span class="fa fa-sign-out"></span> <span class="xn-text">Logout</span></a>
        </li>

    </ul>
    <!-- END X-NAVIGATION -->
</div>
<!-- END PAGE SIDEBAR -->


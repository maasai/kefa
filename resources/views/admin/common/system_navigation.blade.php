<div class="panel-body">
    <div class="page-sidebar">
        <!-- START X-NAVIGATION -->
        <ul class="x-navigation">

        <ul>
            <li class="{{ Html::menu_active('admin/system/levels') }}"><a href="{{ Url('admin/system/levels') }}"><span class="fa fa fa-tasks"></span> Academic Levels</a></li>
            <li class="{{ Html::menu_active('admin/system/assignments') }}"><a href="{{ Url('admin/system/assignments') }}"><span class="fa fa fa-tasks"></span> Assignment Types</a></li>
            <li class="{{ Html::menu_active('admin/system/currencies') }}"><a href="{{ Url('admin/system/currencies') }}"><span class="fa fa fa-tasks"></span> Currencies</a></li>
            <li class="{{ Html::menu_active('admin/system/statuses') }}"><a href="{{ Url('admin/system/statuses') }}"><span class="fa fa fa-tasks"></span> Order Statuses</a></li>
            <li class="{{ Html::menu_active('admin/system/payment_methods') }}"><a href="{{ Url('admin/system/payment_methods') }}"><span class="fa fa fa-tasks"></span> Payment Methods</a></li>
            <li class="{{ Html::menu_active('admin/system/spacings') }}"><a href="{{ Url('admin/system/spacings') }}"><span class="fa fa fa-tasks"></span> Spacings</a></li>
            <li class="{{ Html::menu_active('admin/system/subjects') }}"><a href="{{ Url('admin/system/subjects') }}"><span class="fa fa fa-tasks"></span> Subjects</a></li>
            <li class="{{ Html::menu_active('admin/system/urgencies') }}"><a href="{{ Url('admin/system/urgencies') }}"><span class="fa fa fa-tasks"></span> Urgencies</a></li>
            <li class="{{ Html::menu_active('admin/system/styles') }}"><a href="{{ Url('admin/system/styles') }}"><span class="fa fa fa-tasks"></span> Writing Styles</a></li>

            <li class="xn-title"> More </li>

            <li class="{{ Html::menu_active('admin/system/prices') }}"><a href="{{ Url('admin/system/prices') }}"><span class="fa fa fa-bar-chart-o"></span> Prices </a></li>


        </ul>

    </ul>
    </div>

</div>
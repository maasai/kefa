@extends('client.layouts.main')

@section('content')
<!-- PAGE CONTENT -->
<div class="page-content">

    @include('admin.common.navigation_horizontal')


    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Basic</li>
    </ul>
    <!-- END BREADCRUMB -->

    <!-- PAGE TITLE -->
    <div class="page-title">
        <h2>
            <span class="fa fa-bars"></span> My Orders
            @if($title)
                <h2>  : {{$orders['0']->status['status']}}</h2>

            @endif

        </h2>
    </div>
    <!-- END PAGE TITLE -->

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">


        <div class="row">
            <div class="col-md-12">

                <!-- START DATATABLE EXPORT -->
                <div class="panel panel-default">





                    <div class="panel-heading">
                        <div class="btn-toolbar pull-left">

                            </div>
                              <div class="btn-toolbar pull-right">
                               <a  class="btn btn-primary" href="{{ route('client.orders.create') }}">
                                <i class="fa fa-plus-circle"></i> New Order </a>

                                  <div class="btn-group pull-right">
                                <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onClick ="$('#customers2').tableExport({type:'xml',escape:'false'});"><img src='{{ asset('assets\client\img\icons/xml.png')}}' width="24"/> XML</a></li>
                                <li class="divider"></li>
                                <li><a href="#" onClick ="$('#customers2').tableExport({type:'csv',escape:'false'});"><img src='{{ asset('assets\client\img\icons/csv.png')}}' width="24"/> CSV</a></li>
                                <li><a href="#" onClick ="$('#customers2').tableExport({type:'txt',escape:'false'});"><img src='{{ asset('assets\client\img\icons/txt.png')}}' width="24"/> TXT</a></li>
                                <li class="divider"></li>
                                <li><a href="#" onClick ="$('#customers2').tableExport({type:'excel',escape:'false'});"><img src='{{ asset('assets\client\img\icons/xls.png')}}' width="24"/> XLS</a></li>
                                <li><a href="#" onClick ="$('#customers2').tableExport({type:'doc',escape:'false'});"><img src='{{ asset('assets\client\img\icons/word.png')}}' width="24"/> Word</a></li>
                                <li class="divider"></li>
                            </ul>
                        </div>
                        </div>

                    </div>
                    <div class="panel-body">


                        @if (session()->has('success'))
                            <div class="alert alert-success" id="success-alert">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                                {{ session('success') }}
                            </div>
                        @endif
                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif



                        <table id="customers2" class="table datatable">
                            <thead>
                            <tr>
                                <th>Order #</th>
                                <th>Title</th>
                                <th> Pages </th>
                                <th>Cost</th>
                                <th>Due</th>
                                <th> Status </th>

                                <th></th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($orders as $order)
                                <tr>
                                    <td><a href="{{ route('client.orders.show', $order->uuid) }}">{{ $order->order_number }}</a></td>
                                    <td>{{ $order->title }}</td>
                                    <td>{{ $order->pages }}</td>
                                    <td>{{ $order->total_cost }}</td>
                                    <td>{{ date('F d, Y', strtotime($order->deadline_date)) }}</td>
                                    <td>{{ $order->status['status'] }}</td>

                                    <td>
                                        <a  class="btn btn-info btn-sm" href="{{ route('client.orders.show', $order->uuid) }}">
                                            <i class="fa fa-eye"></i> View </a>

                                        <a  class="btn btn-success btn-sm" href="{{ route('client.orders.edit', $order->uuid) }}">
                                            <i class="fa fa-pencil"></i> Edit </a>


                                        {!! Form::open(array("method"=>"DELETE", "route" => ['client.orders.destroy', $order->uuid], 'class' => 'form-inline', 'style'=>'display:inline')) !!}
                                        <a class="btn btn-danger btn-sm btn-delete"><i class="fa fa-trash-o"></i>  Delete</a>
                                        {!! Form::close() !!}

                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
                <!-- END DATATABLE EXPORT -->
            </div>
        </div>




    </div>
    <!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTENT -->
@endsection
<!-- START PAGE SIDEBAR -->
<div class="page-sidebar">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation">
        <li class="xn-logo">
            <a href="#">ATLANT</a>
            <a href="#" class="x-navigation-control"></a>
        </li>
        <li class="xn-profile">
            <a href="#" class="profile-mini">
                <img src="{{ asset('assets/admin/images/users/avatar.jpg')}}" alt="Jane Doe"/>
            </a>
            <div class="profile">
                <div class="profile-image">
                    <img src="{{ asset('assets/admin/images/users/avatar.jpg')}}" alt="Jane Doe"/>
                </div>
                <div class="profile-data">
                    <div class="profile-data-name">Jane Doe</div>
                    <div class="profile-data-title">Writer</div>
                </div>
                <div class="profile-controls">
                    <a href="#" class="profile-control-left"><span class="fa fa-info"></span></a>
                    <a href="#" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                </div>
            </div>
        </li>
        <li class="xn-title">Navigation</li>
        <li class="{{ Html::menu_active('client/dahsboard') }}">
            <a href="#"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>
        </li>

        <li class="{{ Html::menu_active('client/orders') }}">
        <li class="{{ Html::menu_active('client/orders') }}"><a href="{{ Url('client/orders') }}"><span class="fa fa-th-list"></span> My Orders</a></li>
        </li>


        <li class="xn-title">Payments</li>
        <li class="xn-openable">
            <a href="#"><span class="fa fa-cogs"></span> <span class="xn-text">Invoices</span></a>
            <ul>
                <li><a href="#"><span class="fa fa-heart"></span> Manage Invoices</a></li>
                <li><a href="#"><span class="fa fa-heart"></span> Pending Invoices</a></li>
            </ul>
        </li>
        <li class="xn-title">System</li>

        <li class="">
        <li class="{{ Html::menu_active('client/settings/profile') }}"><a href="{{ Url('client/settings/profile') }}"><span class="fa fa-user"></span> My Profile</a></li>


        </li>
        <li>
            <a href="{{ Url('client/logout') }}"><span class="fa fa-sign-out"></span> <span class="xn-text">Logout</span></a>
        </li>

    </ul>
    <!-- END X-NAVIGATION -->
</div>
<!-- END PAGE SIDEBAR -->


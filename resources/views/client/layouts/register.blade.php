<!DOCTYPE html>
<html lang="en" class="body-full-height">
<head>
    <!-- META SECTION -->
    <title>Kefa - Client Login</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link type="text/css" href="{{ asset('assets/admin/css/theme-default.css')}}" rel="stylesheet" />

    <!-- EOF CSS INCLUDE -->
</head>
<body>

<div class="login-container">

    <div class="login-box animated fadeInDown">
        <div class="login-logo"></div>
        @yield('content')
        <div class="login-footer">
            <div>
                &copy; <?php echo date("Y"); ?>  <a href="{{ Url('/') }}">Kefa Writers</a>
            </div>

        </div>
    </div>

</div>

</body>
</html>







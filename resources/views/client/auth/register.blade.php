@extends('client.layouts.register')
@section('content')
    <div class="login-body">
        <div class="login-title">
           <span>One final step, register to complete order.</span>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br>
                    @endforeach
                </div>
            @endif
        </div>

        @if(session('temp_order'))
            <?php $temp_order = session('temp_order');

            session()->keep(['temp_order', 'email']);
            // dd($temp_order);
            ?>
        @else
            <?php  $temp_order = ""; ?>
        @endif

        @if(session('email'))
            <?php $email = session('email'); ?>
        @else
            <?php $email=""; ?>
        @endif


        {!! Form::open(['url'=>'client/register', 'method'=> 'post', 'class'=>'form-horizontal']) !!}

        {!! Form::hidden('temp_order', serialize($temp_order), ['class' =>"form-control"]) !!}


        <div class="form-group">
            <div class="col-md-12">
                {!! Form::label('client_name', 'Client Name') !!}
                {!! Form::input('client_name','client_name', null, ['class' =>"form-control", 'required'=>'required','placeholder'=>"Name"]) !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                {!! Form::label('email', 'Client Email') !!}
                {!! Form::input('email','email', $email, ['class' =>"form-control", 'required'=>'required','placeholder'=>"Email"]) !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                {!! Form::label('password', 'Password') !!}
                {!! Form::password('password',['class' => 'form-control input-sm']) !!}

            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                {!! Form::label('password_confirmation', 'Confirm Password') !!}
                {!! Form::password('password_confirmation',['class' => 'form-control input-sm']) !!}

            </div>
        </div>
        <div class="form-group">

            <button class="btn btn-finish btn-fill btn-info btn-wd btn-md pull-right"> {{ 'Complete Order' }} <i class="glyphicon glyphicon-chevron-right"></i></button>


        </div>
        {!! Form::close() !!}

    </div>
@endsection

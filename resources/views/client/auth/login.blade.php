@extends('client.layouts.login')
@section('content')
<div class="login-body">
    <div class="login-title">
        <strong>Welcome</strong> back, please login.

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                     {{ $error }}<br>
                @endforeach
            </div>
        @endif
    </div>



    @if(session('temp_order'))
        <?php $temp_order = session('temp_order');
        session()->keep(['temp_order', 'email']);
        ?>
    @else
        <?php  $temp_order = ""; ?>
    @endif

    @if(session('email'))
        <?php $email = session('email'); ?>
        @else
        <?php  $email=""; ?>
    @endif


    {!! Form::open(['url'=>'client/login', 'method'=> 'post', 'class'=>'form-horizontal']) !!}

    {!! Form::hidden('temp_order', serialize($temp_order), ['class' =>"form-control"]) !!}


    <div class="form-group">
        <div class="col-md-12">
        {!! Form::label('email', 'Email') !!}
        {!! Form::input('email','email', $email, ['class' =>"form-control", 'required'=>'required','placeholder'=>"your@email.com"]) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12">
        {!! Form::label('password', 'Password') !!}
        {!! Form::input('password','password', old('password'), ['class' =>"form-control",'required'=>'required','placeholder'=>""]) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-6">
            <a href="#" class="btn btn-link btn-block">Forgot your password?</a>
        </div>
        <div class="col-md-6">
            <input class="btn btn-info btn-block" type="submit" value="Login">

        </div>
    </div>
    {!! Form::close() !!}

</div>
@endsection

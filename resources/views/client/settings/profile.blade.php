@extends('client.layouts.main')

@section('content')

        <!-- PAGE CONTENT -->
<div class="page-content">

    @include('admin.common.navigation_horizontal')

            <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Forms Stuff</a></li>
        <li><a href="#">Form Layout</a></li>
        <li class="active">One Column</li>
    </ul>
    <!-- END BREADCRUMB -->


    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                @if (session()->has('success'))
                    <div class="alert alert-success" id="success-alert">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                        {{ session('success') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif

                @if($settings)
                    {!! Form::model($settings, ['route' => ['client.settings.profile.update', $settings->uuid],'class'=>'ajax-submit', 'method'=>'PATCH', 'files'=>true]) !!}
                @else
                    {!! Form::open(['route' => ['client.settings.profile.store'],'class'=>'ajax-submit', 'files'=>true]) !!}
                @endif


                <div class="panel panel-default">
                    <div class="error">
                        @if($errors->any())
                            dd($errors);
                            @foreach($errors->all() as $error)
                                {{$error}}
                            @endforeach
                        @endif
                    </div>
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong> My Client Profile </strong></h3>

                    </div>

                    <div class="panel-body">

                        <div class="row">

                            <div class="col-md-6">

                                <div class="form-group">
                                    {!! Form::label('client_name','Client Name', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('client_name',null,['class' => 'form-control input-sm','required', 'placeholder'=>'Enter Client Name']) !!}
                                    </div>
                                </div>


                                <div class="form-group">
                                    {!! Form::label('phone','Phone', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('phone',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter Phone']) !!}
                                    </div>
                                </div>


                                <div class="form-group">
                                    {!! Form::label('email','Email', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('email',null,['class' => 'form-control input-sm','required', 'placeholder'=>'Enter Email']) !!}
                                    </div>
                                </div>


                                <div class="form-group">
                                    {!! Form::label('address','Address', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('address',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter Address']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('city','City', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('city',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter City']) !!}
                                    </div>
                                </div>




                            </div>



                            <div class="col-md-6">

                                <div class="form-group">
                                    {!! Form::label('state','State', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('state',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter State']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('postal_code','Postal Code', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('postal_code',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter Postal Code']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('country','Country', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('country',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter Country']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('website','Website', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('website',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter Website']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('notes','Notes', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::text('notes',null,['class' => 'form-control input-sm', 'placeholder'=>'Enter Personal Note']) !!}
                                    </div>
                                </div>








                                <div class="form-group">
                                    {!! Form::label('password','Password', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::password('password',['class' => 'form-control input-sm']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('password_confirmation','Confirm Password', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        <div class="input-group-addon"><div class="fa fa-pencil"></div></div>
                                        {!! Form::password('password_confirmation',['class' => 'form-control input-sm']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('logo','Profile Picture', ['class="col-md-3 control-label"']) !!}
                                    <div class="input-group">
                                        {!! Form::file('logo') !!}
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>



                    <div class="panel-footer">
                        {!! Form::reset('Clear', ['class="btn btn-default"']) !!}
                        {!! Form::submit('Update', ['class="btn btn-primary pull-right"']) !!}

                    </div>
                </div>
                {!! Form::close() !!}

            </div>
        </div>

    </div>
    <!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTENT -->
@endsection
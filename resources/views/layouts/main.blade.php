<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    @include('layouts.title')

    <!-- CSS INCLUDE
    <link type="text/css" href="{{ asset('assets/admin/css/bootstrap/bootstrap.min.css')}}" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">-->

    <link type="text/css" href="{{ asset('assets/admin/css/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" />
    <link type="text/css" href="{{ asset('assets/admin/css/theme-default.css')}}" rel="stylesheet" />
    <link type="text/css" href="{{ asset('assets/css/custom.css')}}" rel="stylesheet" />

@yield('main')

<!-- START SCRIPTS -->
<!-- START PLUGINS-->
<script type="text/javascript" src="{{ asset('assets/js/plugins/jquery/jquery-2.2.0.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/plugins/jquery/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/plugins/bootstrap/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/plugins/bootstrap/bootstrap-dialog.js') }}"></script>
<script src="{{ asset('assets/js/plugins/validator/validator.min.js') }}"></script>


<!-- END PLUGINS-->


<!-- START THIS PAGE PLUGINS-->
<script type='text/javascript' src="{{ asset('assets/admin/js/plugins/icheck/icheck.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/plugins/scrolltotop/scrolltopcontrol.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/plugins/morris/raphael-min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/plugins/morris/morris.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/admin/js/plugins/rickshaw/d3.v3.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/plugins/rickshaw/rickshaw.min.js') }}"></script>

<script type='text/javascript' src="{{ asset('assets/admin/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('assets/admin/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<script type='text/javascript' src="{{ asset('assets/admin/js/plugins/bootstrap/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/plugins/owl/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/plugins/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- END THIS PAGE PLUGINS-->

<script type='text/javascript' src="{{ asset('assets/admin/js/plugins/summernote/summernote.js') }}"></script>

<!-- START TEMPLATE
<script type="text/javascript" src="{{ asset('assets/admin/js/demo_dashboard.js') }}"></script>-->


<script type="text/javascript" src="{{ asset('assets/admin/js/plugins.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/actions.js') }}"></script>


<!-- THIS PAGE PLUGINS -->

<script type="text/javascript" src="{{ asset('assets/admin/js/plugins/bootstrap/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/plugins/bootstrap/bootstrap-file-input.js') }}"></script>


<script type="text/javascript" src="{{ asset('assets/admin/js/plugins/bootstrap/bootstrap-select.js') }}"></script>
<!--
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>-->

<script type="text/javascript" src="{{ asset('assets/admin/js/plugins/tagsinput/jquery.tagsinput.min.js') }}"></script>
<!-- END THIS PAGE PLUGINS -->

<!-- START THIS PAGE PLUGINS-->

<script type="text/javascript" src="{{ asset('assets/admin/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/plugins/tableexport/tableExport.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/plugins/tableexport/jquery.base64.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/plugins/tableexport/html2canvas.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/plugins/tableexport/jspdf/libs/sprintf.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/plugins/tableexport/jspdf/jspdf.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/plugins/tableexport/jspdf/libs/base64.js') }}"></script>


<!-- END TEMPLATE -->
<!-- END SCRIPTS-->
<script type="text/javascript" src="{{ asset('assets/js/custom.js') }}"></script>

</html>
@yield('scripts')






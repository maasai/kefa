<?php
Html::macro('menu_active', function($route)
{
    if(Request::is($route.'/*') OR Request::is($route) )
    {
        $active = "active";
    }else{
        $active = '';
    }
    return $active;
});
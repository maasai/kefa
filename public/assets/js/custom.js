﻿$(function(){
    /*---------------------------------------------------------------
     Modal displays
     --------------------------------------------------------------*/
    var $modal = $('#ajax-modal');

    $(document).on('click', '[data-toggle="ajax-modal"]', function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        $.get(url, function(data) {
            $modal.modal();
            $modal.html(data);
          //  $('form').validator();
           // $('.datepicker').datepicker({format:'yyyy-mm-dd', autoclose:true });
        });
    });
    /*----------------------------------------------------------------
     CUSTOM SCRIPT TO SUBMIT A FORM VIA AJAX
     -----------------------------------------------------------------*/
    $(document).on('submit', '.ajax-submit', function(e){
        e.preventDefault();

        var $form = $(this);
        var formData = new FormData(this);
        $form.find("span").remove();
        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend : function(){
                $form.addClass('spinner');
            },
            success : function(){
                window.location.reload();
            },
            error : function(jqXhr, json, errorThrown){
                var errors = jqXhr.responseJSON.errors;
                $.each( errors, function( key, value ) {
                    $('input[name="'+key+'"]').parents('.form-group').addClass("has-error");
                    $('select[name="'+key+'"]').parents('.form-group').addClass("has-error");

                    $('input[name="'+key+'"]').after('<span class="help-block">'+value+'</span>');


                });
            },
            complete : function(){
                $form.removeClass('spinner');
            }
        });
    });
    /*-----------------------------------------------------------
     Delete Button clicked
     --------------------------------------------------------------*/
    $(document).on('click', '.btn-delete', function (e){e.preventDefault(); confirm_dialog($(this).parent('form')); });
    function confirm_dialog(form){
        BootstrapDialog.show({
            title: 'Confirm Delete',
            message: 'You are about to delete a record. This action cannot be undone. Do you want to proceed?',
            buttons: [ {
                icon: 'fa fa-check',
                label: ' Yes',
                cssClass: 'btn-success btn-sm',
                action: function(){
                    form.submit();
                }
            }, {
                icon: 'fa fa-remove',
                label: 'No',
                cssClass: 'btn-danger btn-sm',
                action: function(dialogItself){
                    dialogItself.close();
                }
            }]
        });
        return false;
    }

    /*-----------------------------------------------------------
     Fade success message alert after a moment
     --------------------------------------------------------------*/
    $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#success-alert").alert('close');
    });



    /*-----------------------------------------------------------
     Delete multiple Button clicked
     --------------------------------------------------------------*/
    $(document).on('click', '.btn-delete-multiple', function (e){e.preventDefault(); confirm_multiple_dialog($(this).parent('form')); });
    function confirm_multiple_dialog(form){
        var selected = $('.select_record').filter(':checked').length;
        if(selected  > 0){
            BootstrapDialog.show({
                title: 'Deleting a Record',
                message: 'You are about to delete records. This action cannot be undone. Do you want to proceed?',
                buttons: [ {
                    icon: 'fa fa-check',
                    label: ' Yes',
                    cssClass: 'btn-success btn-sm',
                    action: function(dialogItself){
                        var url = form.attr('action');
                        var token = form.find('input[name="_token"]').val();
                        var _method = 'DELETE';
                        var selected_rows = [];
                        $('input[name="selected"]:checked').each(function() {
                            selected_rows.push(this.value);
                        });

                        $.post(url+'/'+'delete_multiple', {
                                items: JSON.stringify(selected_rows), _method: _method,  token:token
                            },
                            function(data) {
                                var response = data.responseJSON;
                                if (data.success)
                                {
                                    window.location.reload();
                                }
                            });
                        dialogItself.close();
                    }
                }, {
                    icon: 'fa fa-remove',
                    label: 'No',
                    cssClass: 'btn-danger btn-sm',
                    action: function(dialogItself){
                        dialogItself.close();
                    }
                }]
            });
        }
        else{
            BootstrapDialog.show({
                title: 'Select Record to delete',
                message: 'Please select at least one record to delete',
                buttons: [ {
                    icon: 'fa fa-check',
                    label: ' OK',
                    cssClass: 'btn-success btn-sm',
                    action: function(dialogItself){
                        dialogItself.close();
                    }
                }]
            });
        }
        return false;
    }





});




